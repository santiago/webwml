#use wml::debian::template title="어떻게 연락하나" NOCOMMENTS="yes" MAINPAGE="true"
#use wml::debian::translation-check translation="fb050641b19d0940223934350c51061fcb2439a9" maintainer="Sebul"
#
<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#generalinfo">일반 정보</a>
  <li><a href="#installuse">데비안 설치 및 사용</a>
  <li><a href="#press">홍보 &amp; 언론</a>
  <li><a href="#events">이벤트 &amp; 컨퍼런스</a>
  <li><a href="#helping">데비안 돕기</a>
  <li><a href="#packageproblems">데비안 패키지 문제 보고</a>
  <li><a href="#development">데비안 개발</a>
  <li><a href="#infrastructure">데비안 인프라 문제</a>
  <li><a href="#harassment">괴롭힘 이슈</a>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 초기 요청은 <strong>영어</strong>로 보내기를 정중히 바라는데, 
왜냐면 영어를 가장 많이 쓰기 때문입니다. 
영어가 불가능하면 <a href="https://lists.debian.org/users.html#debian-user">사용자 메일링 리스트</a> 중 하나에 
도움 요청 바랍니다. 여러 다른 언어가 가능합니다.</p>
</aside>

<p>
데비안은 대규모 조직이며 프로젝트 구성원 및 다른 데비안 사용자와 연락할 수 있는 여러 가지 방법이 있습니다. 
이 페이지는 자주 요청되는 연락 방법을 요약합니다. 그것은 결코 완전하지 않습니다. 
다른 연락 방법은 나머지 웹 페이지를 참조하십시오.
</p>

<p>
아래의 대부분의 이메일 주소는 공개 아카이브가 있는 공개 메일링 리스트를 나타냅니다.
메일 보내기 전에 <a href="$(HOME)/MailingLists/disclaimer">고지 사항</a>을 읽으세요.
</p>

<h2 id="generalinfo">일반 정보</h2>

<p>
데비안 관련 대부분 정보는 우리 웹사이트 <a href="$(HOME)">https://www.debian.org/</a>에 수집되니, 
연락 전에 훑어보고 <a href="$(SEARCH)">검색</a>하세요.
</p>

<p>
데비안 프로젝트에 관한 질문은 <email debian-project@lists.debian.org> 메일링 리스트로 보낼 수 있습니다.
리눅스 관련 일반 질문을 여기 보내지 마세요. 다른 메일링 리스트에 대한 정보를 읽으세요.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="doc/user-manuals#faq">FAQ</a></button></p>

<aside class="light">
  <span class="fa fa-envelope fa-5x"></span>
</aside>
<h2 id="installuse">데비안 설치 및 사용</h2>

<p>
설치 미디어와 우리 웹사이트에 있는 문서에 문제에 대한 해결책이 없다고 확신한다면, 
데비안 사용자와 개발자가 질문에 답할 수 있는 매우 활동적인 사용자 메일링 리스트가 있습니다.
여기에서 아래 주제에 관한 모든 종류의 질문을 할 수 있습니다.
</p>

<ul>
  <li>설치
  <li>설정
  <li>지원하는 하드웨어
  <li>기계 관리
  <li>데비안 사용
</ul>

<p>
리스트를 <a href="https://lists.debian.org/debian-user/">구독</a>해서
<email debian-user@lists.debian.org>에 질문을 보내세요.
</p>

<p>
또한, 다양한 언어 사용자를 위한 사용자 메일링 리스트가 있습니다.
<a href="https://lists.debian.org/users.html#debian-user">국제 메일링 리스트</a> 구독 정보를 보세요.
</p>

<p>
구독 필요 없이 <a href="https://lists.debian.org/">메일링 리스트 아카이브</a>를 훑어보거나 아카이브를 
<a href="https://lists.debian.org/search.html">검색</a>할 수 있습니다.
</p>

<p>
무엇보다, 메일링 리스트를 뉴스그룹처럼 훑어볼 수 있습니다.
</p>

<p>
설치 시스템에서 버그를 찾았다고 생각하면, <email debian-boot@lists.debian.org> 메일링 리스트에 쓰세요. 
<a href="$(HOME)/releases/stable/i386/ch05s04#submit-bug">버그 보고</a>를 
<a href="https://bugs.debian.org/debian-installer">debian-installer</a> pseudo-package를 통해 할 수 있습니다.
</p>

<aside class="light">
  <span class="far fa-newspaper fa-5x"></span>
</aside>
<h2 id="press">홍보 &amp; 언론</h2>

<p>
<a href="https://wiki.debian.org/Teams/Publicity">데비안 홍보 팀</a>은
메일링 리스트, 블로그, 마이크로뉴스 웹사이트, 공식 소셜 미디어 채널 같은 모든 데비안 공식 리소스에서 뉴스와 발표를 조정합니다.
</p>

<p>
데비안 관련 글을 쓰고 도움 또는 정보가 필요하다면,
<a href="mailto:press@debian.org">홍보 팀</a>에 연락하세요.
자체 뉴스 페이지에 뉴스를 제출한다면 이 주소가 올바른 주소입니다.
</p>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>
<h2 id="events">이벤트 &amp; 컨퍼런스</h2>

<p>
<a href="$(HOME)/events/">컨퍼런스</a>, 전시 또는 다른 이벤트 초대장을 <email events@debian.org>에 보내세요.
</p>

<p>
전단, 포스터, 유럽 참가 요청은 <email debian-events-eu@lists.debian.org> 메일링 리스트에 보내세요.
</p>

<aside class="light">
  <span class="fas fa-hands-helping fa-5x"></span>
</aside>
<h2 id="helping">데비안 돕기</h2>

<p>
데비안 프로젝트를 돕고 배포판에 기여하는 가능성이 충분합니다.
도움을 주고 싶다면, 먼저
<a href="devel/join/">How to join Debian</a> 페이지를 방문하세요:
</p>

<p>
데비안 미러를 유지하고 싶다면, 
<a href="mirror/">mirroring Debian</a> 관련 페이지를 보세요. 
새 미러 제출은
<a href="mirror/submit">이 폼</a>으로 하세요. 
존재하는 미러 문제 보고 <email mirrors@debian.org>.
</p>

<p>데비안 CD/DVD를 팔고 싶으면, <a href="CD/vendors/info">CD 공급자 정보</a> 보세요. 
CD 공급자 목록을 얻으려면, 
<a href="CD/vendors/adding-form">이 폼</a>을 쓰세요.
</p>

<aside class="light">
  <span class="fas fa-bug fa-5x"></span>
</aside>
<h2 id="packageproblems">데비안 패키지 문제 보고</h2>

<p>
데비안 패키지 버그를 알리려 한다면,
우리는 버그추적시스템을 갖고 있으며 여기에 여러분의 문제를 쉽게 보고할 수 있습니다.
<a href="Bugs/Reporting">instructions for filing bug reports</a> 읽으세요.

<p>
데비안 패키지 메인테이너와 소통하고 싶으면,
각 패키지에 설정된 특별한 메일 별칭을 쓸 수 있습니다.
&lt;<var>패키지 이름</var>&gt;@packages.debian.org 에 보낸 메일은
그 패키지의 메인테이너에게 전달될 겁니다.</p>

<p> 
신중한 방식으로 개발자에게 데비안 보안 문제를 알리고 싶다면 
<email security@debian.org>로 이메일을 보내주십시오.
</p>

<aside class="light">
  <span class="fas fa-code-branch fa-5x"></span>
</aside>
<h2 id="development">데비안 개발</h2>

<p>
데비안 개발에 관한 질문이 있으면, 
여러 <a href="https://lists.debian.org/devel.html">메일링 리스트</a>가 있습니다.
여기에서 개발자와 연락할 수 있습니다.
</p>

<p>
개발자를 위한 일반 메일링 리스트: <email debian-devel@lists.debian.org>.
구독하려면 <a href="https://lists.debian.org/debian-devel/">링크</a>를 따르세요.
</p>

<aside class="light">
  <span class="fas fa-network-wired fa-5x"></span>
</aside>
<h2 id="infrastructure">데비안 인프라 문제</h2>

<p>데비안 서비스 관련 문제를 보고하려면, 대개
<a href="Bugs/Reporting">버그 리포트</a>를 적절한 
<a href="Bugs/pseudo-packages">pseudo-package</a>에 할 수 있습니다.

<p>
대신, 아래 주소 중 하나로 보낼 수 있습니다:
</p>

<define-tag btsurl>package: <a href="https://bugs.debian.org/%0">%0</a></define-tag>

<dl>
<dt>웹 페이지 편집자</dt>
  <dd><btsurl www.debian.org><br />
      <email debian-www@lists.debian.org></dd>
#include "$(ENGLISHDIR)/devel/website/tc.data"
<ifneq "$(CUR_LANG)" "English" "
<dt>웹 페이지 번역자</dt>
  <dd><: &list_translators($CUR_LANG); :></dd>
">
<dt>메일링 리스트 관리자 및 아카이브 메인테이너</dt>
  <dd><btsurl lists.debian.org><br />
      <email listmaster@lists.debian.org></dd>
<dt>버그 추적 시스템 관리자</dt>
  <dd><btsurl bugs.debian.org><br /> 
      <email owner@bugs.debian.org></dd>
</dl>

<aside class="light">
  <span class="fas fa-umbrella fa-5x"></span>
</aside>
<h2 id="harassment">괴롭힘 이슈</h2>

<p>데비안은 존중을 중요시 하는 사람들의 커뮤니티 입니다. 
부적절한 행동의 피해자거나
프로젝트의 누구가 여러분을 다치게 하거나
괴롭힘을 느낀다면
연락할 커뮤니티 팀: <email community@debian.org>
</p>
