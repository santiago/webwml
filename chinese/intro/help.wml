#use wml::debian::template title="开始贡献：您如何協助 Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="3a5c68f115956c48aa60c6aa793eb4d802665b23" maintainer="Boyuan Yang"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#coding">编程和维护软件包</a></li>
    <li><a href="#testing">测试和修复缺陷</a></li>
    <li><a href="#documenting">撰写文档和标记软件包</a></li>
    <li><a href="#translating">翻译和本地化</a></li>
    <li><a href="#usersupport">帮助其他用户</a></li>
    <li><a href="#events">组织活动</a></li>
    <li><a href="#donations">捐赠金钱、硬件和带宽</a></li>
    <li><a href="#usedebian">使用 Debian</a></li>
    <li><a href="#organizations">您的组织如何支持 Debian</a></li>
  </ul>
</div>

<p>Debian 不仅仅是一个操作系统，它也是一个社区。
许多拥有不同技能的人对这个项目作出贡献：
我们的软件、美工、维基，以及其他文档正是许多人齐心协力的
成果。不是所有人都是开发者，所以，您想要参与进来的话，当然
不需要先学习如何编程。有很多不同的方式可以使 Debian 变得更好。
如果您想加入进来的话，以下建议对有经验和没有经验的用户
都很合适：</p>

<h2><a id="coding">编程和维护软件包</a></h2>

<aside class="light">
  <span class="fa fa-code-branch fa-5x"></span>
</aside>

<p>也许您想要从头编写一个新的应用程序，
也许您想在一个现有的程序中实现一个新功能。如果您是一名
开发者，且想要对 Debian 作出贡献，您也可以帮助我们
为 Debian 准备软件以简化安装过程，我们称之为<q>打包</q>。
看看以下的列表以了解如何上手：</p>

<ul>
  <li>您可以挑选您熟悉且您认为对 Debian 有价值的应用程序进行打包。如需了解如何成为软件包的维护者，请阅读 <a href="$(HOME)/devel/">Debian 开发者天地</a>。</li>
  <li>您可以帮助维护已有的应用程序，比如在<a href="https://bugs.debian.org/">缺陷跟踪系统</a>中提交补丁或补充额外信息。另外，您还可以在 <a href="https://salsa.debian.org/">Salsa</a>（我们自己的 GitLab 实例）上加入一个软件包维护团队或软件项目。</li>
  <li>您可以帮助我们<a href="https://security-tracker.debian.org/tracker/data/report">跟踪</a>和<a href="$(DOC)/manuals/developers-reference/pkgs.html#bug-security">修复</a> Debian 软件包中的<a href="$(HOME)/security/">安全问题</a>。</li>
  <li>您也可以帮助加固<a href="https://wiki.debian.org/Hardening">软件包</a>、<a href="https://wiki.debian.org/Hardening/RepoAndImages">仓库、镜像</a>和<a href="https://wiki.debian.org/Hardening/Goals">其它组件</a>。</li>
  <li>您是否对将 Debian <a href="$(HOME)/ports/">移植</a>到一个您所熟悉的架构感兴趣？您可以创建一个新的移植，或者对一个现有的移植作出贡献。</li>
  <li>您可以帮助改进<a href="https://wiki.debian.org/Services">现有的</a>与 Debian 有关的服务，或者建立社区<a href="https://wiki.debian.org/Services#wishlist">需要的</a>新服务。</li>
</ul>

<h2><a id="testing">测试和修复缺陷</a></h2>

<aside class="light">
  <span class="fa fa-bug fa-5x"></span>
</aside>

<p>
和其他软件项目一样，Debian 需要用户测试操作系统及其上的应用程序。
一种作出贡献的方式是安装最新版本的系统，如果有程序没有以预期的方式
工作，就将问题反馈给开发者。我们也需要人手来在不同的硬件上
测试我们的安装媒介、安全启动和 U-Boot 引导程序。
</p>

<ul>
  <li>您可以使用我们的<a href="https://bugs.debian.org/">缺陷跟踪系统</a>来报告您在 Debian 中发现的问题。在这么做之前，请确认该缺陷还没有被报告。</li>
  <li>打开缺陷跟踪系统，并尝试浏览您所使用的软件包的缺陷报告。试试您是否可以提供进一步的信息，或是重现所描述的问题。</li>
  <li>您也可以帮助测试 <a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting">Debian 安装程序和 Live 镜像</a>、<a href="https://wiki.debian.org/SecureBoot/Testing">对安全启动的支持</a>、<a href="https://wiki.debian.org/LTS/TestSuites">LTS 的更新</a>以及 <a href="https://wiki.debian.org/U-boot/Status">U-Boot</a> 引导程序。</li>
</ul>

<h2><a id="documenting">撰写文档和标记软件包</a></h2>

<aside class="light">
  <span class="fa fa-file-alt fa-5x"></span>
</aside>

<p>如果您在 Debian 中遇到了问题，并且不能编写代码来解决问题，
您也许可以试试记一些笔记，并将您的解决方案写下来。
您可以通过这种方式帮助其他遇到类似问题的用户。所有 Debian 文档
都是由社区成员撰写的，您也有几种可以帮助我们的方式。</p>

<ul>
  <li>加入 <a href="$(HOME)/doc/ddp">Debian 文档计划</a> 帮助撰写官方文档。</li>
  <li>对 <a href="https://wiki.debian.org/">Debian 维基</a>作出贡献。</li>
  <li>在 <a href="https://debtags.debian.org/">Debtags</a> 网站上对软件包进行标记和分类。这样，我们的用户可以更容易地找到他们需要的软件。</li>
</ul>


<h2><a id="translating">翻译和本地化</a></h2>

<aside class="light">
  <span class="fa fa-language fa-5x"></span>
</aside>

<p>
如果您的母语不是英语，但您的英语水平足够好，能够理解和翻译软件
或者与 Debian 有关的信息，例如网页、文档等，为什么不试着加入
一个翻译团队，并将 Debian 应用程序翻译成您的母语呢？我们也需要
人手来检查现有的翻译，并在有必要的情况下提交缺陷报告。
</p>

# Translators, link directly to your group's pages
<ul>
  <li>参与中文翻译与本地化工作，请使用 <a href="https://lists.debian.org/debian-l10n-chinese/">debian-l10n-chinese</a> 邮件列表。</li>
  <li>更多信息请参阅 <a href="$(HOME)/international/chinese/">Debian 中文计划</a>页面。</li>
</ul>

<h2><a id="usersupport">帮助其他用户</a></h2>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>

<p>
您也可以通过帮助其他 Debian 用户的方式对本项目作出贡献。
本项目有多个支持平台，例如不同语言的邮件列表和 IRC 频道。欲了解
更多信息，请访问我们的<a href="$(HOME)/support">支持页面</a>。
</p>

# Translators, link directly to the translated mailing lists and provide
# a link to the IRC channel in the user's language

<ul>
  <li>Debian 项目使用许多不同的<a href="https://lists.debian.org/">邮件列表</a>；有些是针对开发者的，有些是针对用户的。有经验的中文用户可以在 <a href="https://lists.debian.org/debian-chinese-gb/"><tt>debian-chinese-gb</tt> 邮件列表</a>或者 <a href="https://lists.debian.org/debian-chinese-big5/"><tt>debian-chinese-big5</tt> 邮件列表</a>上帮助其他有需要的用户。</li>
  <li>来自世界各地的人们都可以通过 IRC（Internet Relay Chat）进行实时聊天。加入 <a href="https://www.oftc.net/">OFTC</a> 服务器的 <tt>#debian-zh</tt> 频道，您可以与其他 Debian 中文用户聊天。</li>
</ul>

<h2><a id="events">组织活动</a></h2>

<aside class="light">
  <span class="fas fa-calendar-check fa-5x"></span>
</aside>

<p>除了年度的 Debian 会议（DebConf）以外，每年在不同国家
都有其他一些小的会议和聚会。帮助组织<a
href="$(HOME)/events/">活动</a>是一个认识其他 Debian 用户
和开发者的好机会。</p>

<ul>
  <li>您可以帮助组织参与每年的 <a href="https://debconf.org/">Debian 会议</a>，例如录制演讲和演示的<a href="https://video.debconf.org/">视频</a>、欢迎参加者和帮助演讲者、在 DebConf 议程中组织特殊活动（例如芝士和红酒派对）、帮助搭建会场、拆除设备，等等。</li>
  <li>另外，还有一些 <a href="https://wiki.debian.org/MiniDebConf">MiniDebConf</a> 活动，就是 Debian 项目成员组织的本地会议。</li>
  <li>您可以创建或加入一个<a href="https://wiki.debian.org/LocalGroups">本地 Debian 用户组</a>，并组织定期见面会或其他活动。</li>
  <li>您还可以看看其他活动，例如 <a href="https://wiki.debian.org/DebianDay">Debian 之日派对</a>、<a href="https://wiki.debian.org/ReleaseParty">发布派对</a>、<a href="https://wiki.debian.org/BSP">缺陷修补聚会</a>、<a href="https://wiki.debian.org/Sprints">开发活动</a>以及<a href="https://wiki.debian.org/DebianEvents">其它世界各地的活动</a>。</li>
</ul>

<h2><a id="donations">捐赠金钱、硬件和带宽</a></h2>

<aside class="light">
  <span class="fas fa-gift fa-5x"></span>
</aside>

<p>所有对 Debian 项目的捐赠都由 Debian 项目领导者（DPL）进行管理。
有了您的支持，我们可以购买硬件、域名、加密证书等等。
我们的资金还可以用于支持 DebConf 和 MiniDebConf 会议、开发活动、
出席其他活动，以及其他事项。</p>

<ul>
  <li>您可以向 Debian 计划<a href="$(HOME)/donations">捐赠</a>金钱、设备和服务。</li>
  <li>我们一直在寻求在世界各地建立<a href="$(HOME)/mirror/">镜像站点</a>。</li>
  <li>Debian 移植依赖于我们的<a href="$(HOME)/devel/buildd/">自动构建系统</a>。</li>
</ul>

<h2><a id="usedebian">使用 Debian 并谈论它</a></h2>

<aside class="light">
  <span class="fas fa-bullhorn fa-5x"></span>
</aside>

<p>请将 Debian 和 Debian 社区告诉更多的人。
将操作系统推荐给其他人，并向他们演示如何安装。
您只需使用并享受它——这可能是回馈 Debian 项目的最简单的方式。</p>

<ul>
  <li>您可以通过进行演讲并向其他用户演示 Debian 的方式来帮助宣传 Debian。</li>
  <li>您可以对我们的<a href="https://www.debian.org/devel/website/">网站</a>作出贡献，以改善 Debian 的公众形象。</li>
  <li>您可以制作软件包的<a href="https://wiki.debian.org/ScreenShots">屏幕截图</a>并将它们<a href="https://screenshots.debian.net/upload">上传</a>到 <a href="https://screenshots.debian.net/">screenshots.debian.net</a>，以使我们的用户在使用软件之前即可看到软件在 Debian 中是什么样子。</li>
  <li>您可以启用<a href="https://packages.debian.org/popularity-contest">软件包流行度调查提交</a>功能，以使我们了解哪些软件包更流行、对更多的人有价值。</li>
</ul>

<h2><a id="organizations">您的组织如何支持 Debian</a></h2>

<aside class="light">
  <span class="fa fa-check-circle fa-5x"></span>
</aside>

<p>
不论您是在教育、商业、非营利性组织，还是政府组织工作，您都有很多方式可以使用您的资源来支持 Debian。
</p>

<ul>
  <li>例如，您的组织可以简单地<a href="$(HOME)/donations">捐赠</a>金钱或者硬件。</li>
  <li>也许您愿意<a href="https://www.debconf.org/sponsors/">赞助</a>我们的会议。</li>
  <li>您的组织可以<a href="https://wiki.debian.org/MemberBenefits">为 Debian 贡献者提供产品或服务</a>。</li>
  <li>我们在寻求为 Debian 服务提供<a href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">免费托管</a>。</li>
  <li>当然，我们也会很感谢您为我们的<a href="https://www.debian.org/mirror/ftpmirror">软件</a>、<a
href="https://www.debian.org/CD/mirroring/">安装媒介</a>，或<a href="https://wiki.debconf.org/wiki/Videoteam/Archive">会议视频</a>设立镜像。</li>
  <li>您也可以考虑售卖 Debian <a href="https://www.debian.org/events/merchandise">周边商品</a>、<a href="https://www.debian.org/CD/vendors/">安装媒介</a>，或者<a href="https://www.debian.org/distrib/pre-installed">预装 Debian 的系统</a>。</li>
  <li>如果您的组织提供 Debian <a href="https://www.debian.org/consultants/">顾问服务</a>或者<a href="https://wiki.debian.org/DebianHosting">托管服务</a>，请联系我们。</li>
</ul>

<p>
我们也希望建立<a href="https://www.debian.org/partners/">合作伙伴</a>关系。
如果您可以通过<a href="https://www.debian.org/users/">提供推荐信</a>、
在您的组织的服务器或桌面计算机中运行 Debian、或者鼓励您的雇员参与我们的项目
的方式来推广 Debian，那就更好了。您也可以考虑开设 Debian 操作系统和社区
的课程、允许您的团队在工作时间进行贡献，或者让他们来参加我们
的<a href="$(HOME)/events/">活动</a>。</p>

