<define-tag pagetitle>Updated Debian 10: 10.6 released</define-tag>
<define-tag release_date>2020-09-26</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>The Debian project is pleased to announce the sixth update of its
stable distribution Debian <release> (codename <q><codename></q>). 
This point release mainly adds corrections for security issues,
along with a few adjustments for serious problems.  Security advisories
have already been published separately and are referenced where available.</p>

<p>Please note that the point release does not constitute a new version of Debian
<release> but only updates some of the packages included.  There is
no need to throw away old <q><codename></q> media. After installation,
packages can be upgraded to the current versions using an up-to-date Debian
mirror.</p>

<p>Those who frequently install updates from security.debian.org won't have
to update many packages, and most such updates are
included in the point release.</p>

<p>New installation images will be available soon at the regular locations.</p>

<p>Upgrading an existing installation to this revision can be achieved by
pointing the package management system at one of Debian's many HTTP mirrors.
A comprehensive list of mirrors is available at:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Miscellaneous Bugfixes</h2>

<p>This stable update adds a few important corrections to the following packages.</p>

<p>Note that, due to build issues, the updates for the cargo, rustc and rustc-bindgen packages are currently not available for the <q>armel</q> architecture.
They may be added at a later date if the issues are resolved.</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction arch-test "Fix detection of s390x sometimes failing">
<correction asterisk "Fix crash when negotiating for T.38 with a declined stream [CVE-2019-15297], <q>SIP request can change address of a SIP peer</q> [CVE-2019-18790], <q>AMI user could execute system commands</q> [CVE-2019-18610], segfault in pjsip show history with IPv6 peers">
<correction bacula "Fix <q>oversized digest strings allow a malicious client to cause a heap overflow in the director's memory</q> [CVE-2020-11061]">
<correction base-files "Update /etc/debian_version for the point release">
<correction calamares-settings-debian "Disable displaymanager module">
<correction cargo "New upstream release, to support upcoming Firefox ESR versions">
<correction chocolate-doom "Fix missing validation [CVE-2020-14983]">
<correction chrony "Prevent symlink race when writing to the PID file [CVE-2020-14367]; fix temperature reading">
<correction debian-installer "Update Linux ABI to 4.19.0-11">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction diaspora-installer "Use --frozen option to bundle install to use upstream Gemfile.lock; don't exclude Gemfile.lock during upgrades; don't overwrite config/oidc_key.pem during upgrades; make config/schedule.yml writeable">
<correction dojo "Fix prototype pollution in deepCopy method [CVE-2020-5258] and in jqMix method [CVE-2020-5259]">
<correction dovecot "Fix dsync sieve filter sync regression; fix handling of getpwent result in userdb-passwd">
<correction facter "Change Google GCE Metadata endpoint from <q>v1beta1</q> to <q>v1</q>">
<correction gnome-maps "Fix an issue with misaligned shape layer rendering">
<correction gnome-shell "LoginDialog: Reset auth prompt on VT switch before fade in [CVE-2020-17489]">
<correction gnome-weather "Prevent a crash when the configured set of locations are invalid">
<correction grunt "Use safeLoad when loading YAML files [CVE-2020-7729]">
<correction gssdp "New upstream stable release">
<correction gupnp "New upstream stable release; prevent the <q>CallStranger</q> attack [CVE-2020-12695]; require GSSDP 1.0.5">
<correction haproxy "logrotate.conf: use rsyslog helper instead of SysV init script; reject messages where <q>chunked</q> is missing from Transfer-Encoding [CVE-2019-18277]">
<correction icinga2 "Fix symlink attack [CVE-2020-14004]">
<correction incron "Fix cleanup of zombie processes">
<correction inetutils "Fix remote code execution issue [CVE-2020-10188]">
<correction libcommons-compress-java "Fix denial of service issue [CVE-2019-12402]">
<correction libdbi-perl "Fix memory corruption in XS functions when Perl stack is reallocated [CVE-2020-14392]; fix a buffer overflow on an overlong DBD class name [CVE-2020-14393]; fix a NULL profile dereference in dbi_profile() [CVE-2019-20919]">
<correction libvncserver "libvncclient: bail out if UNIX socket name would overflow [CVE-2019-20839]; fix pointer aliasing/alignment issue [CVE-2020-14399]; limit max textchat size [CVE-2020-14405]; libvncserver: add missing NULL pointer checks [CVE-2020-14397]; fix pointer aliasing/alignment issue [CVE-2020-14400]; scale: cast to 64 bit before shifting [CVE-2020-14401]; prevent OOB accesses [CVE-2020-14402 CVE-2020-14403 CVE-2020-14404]">
<correction libx11 "Fix integer overflows [CVE-2020-14344 CVE-2020-14363]">
<correction lighttpd "Backport several usability and security fixes">
<correction linux "New upstream stable release; increase ABI to 11">
<correction linux-latest "Update for -11 Linux kernel ABI">
<correction linux-signed-amd64 "New upstream stable release">
<correction linux-signed-arm64 "New upstream stable release">
<correction linux-signed-i386 "New upstream stable release">
<correction llvm-toolchain-7 "New upstream release, to support upcoming Firefox ESR versions; fix bugs affecting rustc build">
<correction lucene-solr "Fix security issue in DataImportHandler configuration handling [CVE-2019-0193]">
<correction milkytracker "Fix heap overflow [CVE-2019-14464], stack overflow [CVE-2019-14496], heap overflow [CVE-2019-14497], use after free [CVE-2020-15569]">
<correction node-bl "Fix over-read vulnerability [CVE-2020-8244]">
<correction node-elliptic "Prevent malleability and overflows [CVE-2020-13822]">
<correction node-mysql "Add localInfile option to control LOAD DATA LOCAL INFILE [CVE-2019-14939]">
<correction node-url-parse "Fix insufficient validation and sanitization of user input [CVE-2020-8124]">
<correction npm "Don't show password in logs [CVE-2020-15095]">
<correction orocos-kdl "Remove explicit inclusion of default include path, fixing issues with cmake &lt; 3.16">
<correction postgresql-11 "New upstream stable release; set a secure search_path in logical replication walsenders and apply workers [CVE-2020-14349]; make contrib modules' installation scripts more secure [CVE-2020-14350]">
<correction postgresql-common "Don't drop plpgsql before testing extensions">
<correction pyzmq "Asyncio: wait for POLLOUT on sender in can_connect">
<correction qt4-x11 "Fix buffer overflow in XBM parser [CVE-2020-17507]">
<correction qtbase-opensource-src "Fix buffer overflow in XBM parser [CVE-2020-17507]; fix clipboard breaking when timer wraps after 50 days">
<correction ros-actionlib "Load YAML safely [CVE-2020-10289]">
<correction rustc "New upstream release, to support upcoming Firefox ESR versions">
<correction rust-cbindgen "New upstream release, to support upcoming Firefox ESR versions">
<correction ruby-ronn "Fix handling of UTF-8 content in manpages">
<correction s390-tools "Hardcode perl dependency instead of using ${perl:Depends}, fixing installation under debootstrap">
</table>


<h2>Security Updates</h2>


<p>This revision adds the following security updates to the stable release.
The Security Team has already released an advisory for each of these
updates:</p>

<table border=0>
<tr><th>Advisory ID</th>  <th>Package</th></tr>
<dsa 2020 4662 openjdk-11>
<dsa 2020 4734 openjdk-11>
<dsa 2020 4736 firefox-esr>
<dsa 2020 4737 xrdp>
<dsa 2020 4738 ark>
<dsa 2020 4739 webkit2gtk>
<dsa 2020 4740 thunderbird>
<dsa 2020 4741 json-c>
<dsa 2020 4742 firejail>
<dsa 2020 4743 ruby-kramdown>
<dsa 2020 4744 roundcube>
<dsa 2020 4745 dovecot>
<dsa 2020 4746 net-snmp>
<dsa 2020 4747 icingaweb2>
<dsa 2020 4748 ghostscript>
<dsa 2020 4749 firefox-esr>
<dsa 2020 4750 nginx>
<dsa 2020 4751 squid>
<dsa 2020 4752 bind9>
<dsa 2020 4753 mupdf>
<dsa 2020 4754 thunderbird>
<dsa 2020 4755 openexr>
<dsa 2020 4756 lilypond>
<dsa 2020 4757 apache2>
<dsa 2020 4758 xorg-server>
<dsa 2020 4759 ark>
<dsa 2020 4760 qemu>
<dsa 2020 4761 zeromq3>
<dsa 2020 4762 lemonldap-ng>
<dsa 2020 4763 teeworlds>
<dsa 2020 4764 inspircd>
<dsa 2020 4765 modsecurity>
</table>



<h2>Debian Installer</h2>
<p>The installer has been updated to include the fixes incorporated
into stable by the point release.</p>

<h2>URLs</h2>

<p>The complete lists of packages that have changed with this revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>The current stable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Proposed updates to the stable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>stable distribution information (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Security announcements and information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>
