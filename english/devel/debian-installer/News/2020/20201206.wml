<define-tag pagetitle>Debian Installer Bullseye Alpha 3 release</define-tag>
<define-tag release_date>2020-12-06</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the third alpha release of the installer for Debian 11
<q>Bullseye</q>.
</p>


<h2>Improvements in this release</h2>

<ul>
  <li>apt-setup:
    <ul>
      <li>Remove mention of volatile repo from generated sources.list
        file (<a href="https://bugs.debian.org/954460">#954460</a>).</li>
    </ul>
  </li>
  <li>base-installer:
    <ul>
      <li>Improve test architecture, adding support for Linux 5.x
        versions.</li>
    </ul>
  </li>
  <li>brltty:
    <ul>
      <li>Improve hardware detection and driver support.</li>
    </ul>
  </li>
  <li>cdebconf:
    <ul>
      <li>Make text interface report progress more accurately: from
        the very beginning, and also as soon as an answer to a
        question has been given.</li>
    </ul>
  </li>
  <li>choose-mirror:
    <ul>
      <li>Update Mirrors.masterlist.</li>
    </ul>
  </li>
  <li>console-setup:
    <ul>
      <li>Improve support for box-drawing characters (<a href="https://bugs.debian.org/965029">#965029</a>).</li>
      <li>Sync Terminus font with the xfonts-terminus package.</li>
      <li>Fix Lithuanian layout (<a href="https://bugs.debian.org/951387">#951387</a>).</li>
    </ul>
  </li>
  <li>debian-cd:
    <ul>
      <li>Only include Linux udebs for the latest ABI, making small
        installation images more useful.</li>
    </ul>
  </li>
  <li>debian-installer:
    <ul>
      <li>Bump Linux kernel ABI to 5.9.0-4</li>
      <li>Drop fontconfig tweaks introduced in the Debian Installer
        Buster Alpha 1 release (See: <a href="https://bugs.debian.org/873462">#873462</a>).</li>
      <li>Install kmod-udeb instead of libkmod2-udeb.</li>
      <li>Mimick libgcc1 handling, for libgcc-s1.</li>
      <li>Clean up the list of fake packages.</li>
      <li>Replace the mklibs library reduction pass with a hack,
        copying libgcc_s.so.[124] from the host filesystem for the
        time being.</li>
      <li>Add explicit build-depends on fdisk on arm64, amd64 and i386
        now that util-linux doesn't depend on it anymore.</li>
      <li>Add grub2 to built-using (<a href="https://bugs.debian.org/968998">#968998</a>).</li>
      <li>Fix FTBFS with fakeroot by adjusting the /dev/console check
        (see <a href="https://bugs.debian.org/940056">#940056</a>).</li>
    </ul>
  </li>
  <li>debian-installer-utils:
    <ul>
      <li>Adjust fetch-url's use of file descriptors for recent udev
        versions (<a href="https://bugs.debian.org/967546">#967546</a>).</li>
    </ul>
  </li>
  <li>debootstrap:
    <ul>
      <li>Only install apt-transport-https on stretch and earlier,
        HTTPS support was merged into the core apt package for buster
        (<a href="https://bugs.debian.org/920255">#920255</a>, <a href="https://bugs.debian.org/879755">#879755</a>).</li>
    </ul>
  </li>
  <li>finish-install:
    <ul>
      <li>Drop upstart support entirely (<a href="https://bugs.debian.org/923845">#923845</a>).</li>
    </ul>
  </li>
  <li>fonts-noto:
    <ul>
      <li>Fix Sinhala support in the installer (<a href="https://bugs.debian.org/954948">#954948</a>).</li>
    </ul>
  </li>
  <li>grub-installer:
    <ul>
      <li>Update templates, to make them fit for UEFI systems and new
        kind of system storage media (<a href="https://bugs.debian.org/954718">#954718</a>).</li>
    </ul>
  </li>
  <li>kmod:
    <ul>
      <li>Split kmod-udeb off of libkmod2-udeb and actually ship the
        libraries in libkmod2-udeb (<a href="https://bugs.debian.org/953952">#953952</a>).</li>
    </ul>
  </li>
  <li>locale-chooser:
    <ul>
      <li>Activate new languages: Kabyle, Occitan.</li>
    </ul>
  </li>
  <li>partman-auto:
    <ul>
      <li>Bump /boot sizes in most recipes from between 128 and 256M
        to between 512 and 768M (<a href="https://bugs.debian.org/893886">#893886</a>, <a href="https://bugs.debian.org/951709">#951709</a>).</li>
      <li>Import partman-auto/cap-ram support from Ubuntu, to allow
        capping RAM size as used for swap partition calculations
        (<a href="https://bugs.debian.org/949651">#949651</a>, <a href="https://bugs.debian.org/950344">#950344</a>). This allows us to cap the minimum size of
        swap partitions size to 1*CAP, and their maximum size to a
        maximum of 2 or 3*CAP depending on architecture. Default is
        set to 1024, thus capping swap partitions to between 1 and
        3GB.</li>
    </ul>
  </li>
  <li>partman-efi:
    <ul>
      <li>Remount /cdrom read-write if it also happens to be used as
        /boot/efi (<a href="https://bugs.debian.org/967918">#967918</a>).</li>
      <li>Remove usage of the efivars module, and stop looking for
        /proc/efi. efivarfs is the current interface, and /proc/efi
        went away a long time ago.</li>
    </ul>
  </li>
  <li>partman-partitioning:
    <ul>
      <li>Include ntfs-3g-udeb on arm64.</li>
    </ul>
  </li>
  <li>partman-target:
    <ul>
      <li>Add a hint to the new fstab about using <code>systemctl daemon-reload</code>
        after changing /etc/fstab (<a href="https://bugs.debian.org/963573">#963573</a>).</li>
    </ul>
  </li>
  <li>systemd:
    <ul>
      <li>Install 60-block.rules in udev-udeb (<a href="https://bugs.debian.org/958397">#958397</a>). The block
        device rules were split out from 60-persistent-storage.rules
        in v220. This fixes a longstanding bug where UUIDs would not
        be used for filesystems on initial installation.</li>
    </ul>
  </li>
  <li>util-linux:
    <ul>
      <li>Take over eject-udeb (<a href="https://bugs.debian.org/737658">#737658</a>).</li>
    </ul>
  </li>
  <li>win32-loader:
    <ul>
      <li>Introduce UEFI boot manager and Secure Boot support
        (<a href="https://bugs.debian.org/918863">#918863</a>).</li>
    </ul>
  </li>
</ul>


<h2>Hardware support changes</h2>

<ul>
  <li>debian-cd:
    <ul>
      <li>Enable graphical installer for arm64.</li>
      <li>Exclude lilo-installer and elilo-installer udebs for all
        archs.</li>
      <li>Stop making XFCE single CD images.</li>
      <li>Stop making DVD ISO images 2 and 3 for amd64/i386 (they are
        still available via jigdo).</li>
    </ul>
  </li>
  <li>debian-installer:
    <ul>
      <li>Update Firefly-RK3288 image for new u-boot version.</li>
      <li>[arm64] Add support for firefly-rk3399, pinebook-pro-rk3399,
        rockpro64-rk3399, rock64-rk3328 and rock-pi-4-rk3399 to u-boot
        images and netboot SD card images.</li>
      <li>[arm64] Make all netboot sdcard images start at offset
        32768, for compatibility with rockchip platforms.</li>
      <li>Add OLPC XO-1.75 laptop support (<a href="https://bugs.debian.org/949306">#949306</a>).</li>
      <li>Enable GTK build for arm64.</li>
      <li>Add support for NanoPi NEO Air (<a href="https://bugs.debian.org/928863">#928863</a>).</li>
      <li>Add wireless-regdb-udeb to Linux builds that include
        nic-wireless-modules.</li>
      <li>efi-image: Improve sizing calculation to reduce wasted
        space.</li>
      <li>efi-image: Include DTB files in the ESP for armhf and arm64
        systems. This should make U-Boot based systems work better
        when booting via UEFI.</li>
    </ul>
  </li>
  <li>flash-kernel:
    <ul>
      <li>Add FriendlyARM NanoPi NEO Plus2 (<a href="https://bugs.debian.org/955374">#955374</a>).</li>
      <li>Add Pinebook (<a href="https://bugs.debian.org/930098">#930098</a>).</li>
      <li>Add Pinebook Pro.</li>
      <li>Add Olimex A64-Olinuxino and A64-Olinuxino-eMMC
        (<a href="https://bugs.debian.org/931195">#931195</a>).</li>
      <li>Add SolidRun LX2160A Honeycomb and Clearfog CX
        (<a href="https://bugs.debian.org/958023">#958023</a>).</li>
      <li>Add SolidRun Cubox-i Solo/DualLite variants (<a href="https://bugs.debian.org/939261">#939261</a>).</li>
      <li>Add Turris MOX (<a href="https://bugs.debian.org/961303">#961303</a>).</li>
    </ul>
  </li>
  <li>linux:
    <ul>
      <li>Move any compression modules to kernel-image udeb; drop
        compress-modules udeb.</li>
      <li>Make input-modules udeb depend on crc-modules.</li>
      <li>[arm64] Add i2c_mv64xxx to i2c-modules udeb.</li>
      <li>[arm64] Add drivers/pinctrl to kernel-image udeb.</li>
      <li>[arm64] Add analogix-anx6345, pwm-sun4i, sun4i-drm and
        sun8i-mixer to fb-modules udeb.</li>
      <li>[arm64] Add pwm-sun4i to fb-modules udeb.</li>
      <li>[arm64] Add armada_37xx_wdt to kernel-image udeb
        (<a href="https://bugs.debian.org/961086">#961086</a>).</li>
      <li>[mips*] Drop hfs-modules udeb.</li>
      <li>[x86] Add crc32_pclmul to crc-modules udeb.</li>
      <li>Add crc32_generic to crc-modules udeb.</li>
      <li>Reverse order of cdrom-core and isofs/udf udebs: the latter
        ones now require the former.</li>
      <li>Drop zlib-modules udeb (zlib_deflate is now always built-in).</li>
      <li>Add f2fs-modules udeb.</li>
    </ul>
  </li>
</ul>


<h2>Localization status</h2>

<ul>
  <li>78 languages are supported in this release.</li>
  <li>New languages: Kabyle, Occitan.</li>
  <li>Full translation for 16 of them.</li>
</ul>


<h2>Known issues in this release</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>Feedback for this release</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>Thanks</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>
