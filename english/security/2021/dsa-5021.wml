<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in MediaWiki, a website engine
for collaborative work: Vulnerabilities in the mcrundo and rollback
actions may allow an attacker to leak page content from private wikis
or to bypass edit restrictions.</p>

<p>For additional information please refer to
<a href="https://www.mediawiki.org/wiki/2021-12_security_release/FAQ">\
https://www.mediawiki.org/wiki/2021-12_security_release/FAQ</a></p>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 1:1.31.16-1+deb10u2.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 1:1.35.4-1+deb11u2.</p>

<p>We recommend that you upgrade your mediawiki packages.</p>

<p>For the detailed security status of mediawiki please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mediawiki">\
https://security-tracker.debian.org/tracker/mediawiki</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5021.data"
# $Id: $
