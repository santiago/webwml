<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the Key Distribution Center (KDC) in krb5, the
MIT implementation of Kerberos, is prone to a NULL pointer dereference
flaw. An unauthenticated attacker can take advantage of this flaw to
cause a denial of service (KDC crash) by sending a request containing a
PA-ENCRYPTED-CHALLENGE padata element without using FAST.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1.17-3+deb10u2.</p>

<p>We recommend that you upgrade your krb5 packages.</p>

<p>For the detailed security status of krb5 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/krb5">\
https://security-tracker.debian.org/tracker/krb5</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4944.data"
# $Id: $
