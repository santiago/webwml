<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in HAProxy, a fast and reliable
load balancing reverse proxy, which can result in HTTP request
smuggling. By carefully crafting HTTP/2 requests, it is possible to
smuggle another HTTP request to the backend selected by the HTTP/2
request. With certain configurations, it allows an attacker to send an
HTTP request to a backend, circumventing the backend selection logic.</p>

<p>Known workarounds are to disable HTTP/2 and set
"tune.h2.max-concurrent-streams" to 0 in the <q>global</q> section.</p>

    <p>global
        tune.h2.max-concurrent-streams 0</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.2.9-2+deb11u1.</p>

<p>We recommend that you upgrade your haproxy packages.</p>

<p>For the detailed security status of haproxy please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/haproxy">https://security-tracker.debian.org/tracker/haproxy</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4960.data"
# $Id: $
