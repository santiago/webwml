<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in libxml2, a library providing
support to read, modify and write XML and HTML files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40303">CVE-2022-40303</a>

    <p>Maddie Stone discovered that missing safety checks in several
    functions can result in integer overflows when parsing a XML
    document with the XML_PARSE_HUGE option enabled.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40304">CVE-2022-40304</a>

    <p>Ned Williamson and Nathan Wachholz discovered a vulnerability when
    handling detection of entity reference cycles, which may result in
    corrupted dictionary entries. This flaw may lead to logic errors,
    including memory errors like double free flaws.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.9.10+dfsg-6.7+deb11u3.</p>

<p>We recommend that you upgrade your libxml2 packages.</p>

<p>For the detailed security status of libxml2 please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxml2">https://security-tracker.debian.org/tracker/libxml2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5271.data"
# $Id: $
