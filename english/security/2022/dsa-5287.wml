<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Heimdal, an implementation of
Kerberos 5 that aims to be compatible with MIT Kerberos.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3671">CVE-2021-3671</a>

    <p>Joseph Sutton discovered that the Heimdal KDC does not validate that
    the server name in the TGS-REQ is present before dereferencing,
    which may result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44758">CVE-2021-44758</a>

    <p>It was discovered that Heimdal is prone to a NULL dereference in
    acceptors where an initial SPNEGO token that has no acceptable
    mechanisms, which may result in denial of service for a server
    application that uses SPNEGO.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3437">CVE-2022-3437</a>

    <p>Several buffer overflow flaws and non-constant time leaks were
    discovered when using 1DES, 3DES or RC4 (arcfour).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41916">CVE-2022-41916</a>

    <p>An out-of-bounds memory access was discovered when Heimdal
    normalizes Unicode, which may result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42898">CVE-2022-42898</a>

    <p>It was discovered that integer overflows in PAC parsing may result
    in denial of service for Heimdal KDCs or possibly Heimdal servers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44640">CVE-2022-44640</a>

    <p>It was discovered that the Heimdal's ASN.1 compiler generates code
    that allows specially crafted DER encodings to invoke an invalid
    free on the decoded structure upon decode error, which may result in
    remote code execution in the Heimdal KDC.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 7.7.0+dfsg-2+deb11u2.</p>

<p>We recommend that you upgrade your heimdal packages.</p>

<p>For the detailed security status of heimdal please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/heimdal">https://security-tracker.debian.org/tracker/heimdal</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5287.data"
# $Id: $
