<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Julian Gilbey discovered that schroot, a tool allowing users to execute
commands in a chroot environment, had too permissive rules on chroot or
session names, allowing a denial of service on the schroot service for
all users that may start a schroot session.</p>

<p>Note that existing chroots and sessions are checked during upgrade, and
an upgrade is aborted if any future invalid name is detected.</p>

<p>Problematic session and chroots can be checked before upgrading with the
following command:</p>

<code>schroot --list --all | LC_ALL=C grep -vE '^[a-z]+:[a-zA-Z0-9][a-zA-Z0-9_.-]*$'</code>

<p>See 
<url "https://codeberg.org/shelter/reschroot/src/tag/release/reschroot-1.6.13/NEWS#L10-L41">
for instructions on how to resolve such a situation.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 1.6.10-12+deb11u1.</p>

<p>We recommend that you upgrade your schroot packages.</p>

<p>For the detailed security status of schroot please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/schroot">\
https://security-tracker.debian.org/tracker/schroot</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5213.data"
# $Id: $
