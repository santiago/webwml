<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Robin Peraglie and Johannes Moritz discovered an argument injection bug in the
xfce4-mime-helper component of xfce4-settings, which can be exploited using the
xdg-open common tool. Since xdg-open is used by multiple standard applications
for opening links, this bug could be exploited by an attacker to run arbitrary
code on an user machine by providing a malicious PDF file with specifically
crafted links.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 4.16.0-1+deb11u1.</p>

<p>We recommend that you upgrade your xfce4-settings packages.</p>

<p>For the detailed security status of xfce4-settings please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xfce4-settings">\
https://security-tracker.debian.org/tracker/xfce4-settings</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5296.data"
# $Id: $
