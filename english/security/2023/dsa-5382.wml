<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was reported that cairosvg, a SVG converter based on Cairo, can send
requests to external hosts when processing specially crafted SVG files
with external file resource loading. An attacker can take advantage of
this flaw to perform a server-side request forgery or denial of service.
Fetching of external files is disabled by default with this update.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 2.5.0-1.1+deb11u1.</p>

<p>We recommend that you upgrade your cairosvg packages.</p>

<p>For the detailed security status of cairosvg please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cairosvg">\
https://security-tracker.debian.org/tracker/cairosvg</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5382.data"
# $Id: $
