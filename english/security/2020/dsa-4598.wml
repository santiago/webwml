<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Simon Charette reported that the password reset functionality in Django,
a high-level Python web development framework, uses a Unicode
case-insensitive query to retrieve accounts matching the email address
requesting the password reset. An attacker can take advantage of this
flaw to potentially retrieve password reset tokens and hijack accounts.</p>

<p>For details please refer to
<a href="https://www.djangoproject.com/weblog/2019/dec/18/security-releases/">https://www.djangoproject.com/weblog/2019/dec/18/security-releases/</a></p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 1:1.10.7-2+deb9u7.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1:1.11.27-1~deb10u1.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>For the detailed security status of python-django please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-django">https://security-tracker.debian.org/tracker/python-django</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4598.data"
# $Id: $
