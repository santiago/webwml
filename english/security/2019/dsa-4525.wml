<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Simon McVittie reported a flaw in ibus, the Intelligent Input Bus. Due
to a misconfiguration during the setup of the DBus, any unprivileged
user could monitor and send method calls to the ibus bus of another
user, if able to discover the UNIX socket used by another user connected
on a graphical environment. The attacker can take advantage of this flaw
to intercept keystrokes of the victim user or modify input related
configurations through DBus method calls.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 1.5.14-3+deb9u2.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1.5.19-4+deb10u1.</p>

<p>We recommend that you upgrade your ibus packages.</p>

<p>For the detailed security status of ibus please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/ibus">https://security-tracker.debian.org/tracker/ibus</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4525.data"
# $Id: $
