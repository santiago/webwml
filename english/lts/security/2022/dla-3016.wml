<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in rsyslog, a system and
kernel logging daemon. When a log server is configured to accept logs
from remote clients through specific modules such as <q>imptcp</q>, an
attacker can cause a denial of service (DoS) and possibly execute code
on the server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16881">CVE-2018-16881</a>

    <p>A denial of service vulnerability was found in rsyslog
    in the imptcp module. An attacker could send a specially crafted
    message to the imptcp socket, which would cause rsyslog to crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24903">CVE-2022-24903</a>

    <p>Modules for TCP syslog reception have a potential heap buffer
    overflow when octet-counted framing is used. This can result in a
    segfault or some other malfunction.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
8.24.0-1+deb9u2.</p>

<p>We recommend that you upgrade your rsyslog packages.</p>

<p>For the detailed security status of rsyslog please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/rsyslog">https://security-tracker.debian.org/tracker/rsyslog</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3016.data"
# $Id: $
