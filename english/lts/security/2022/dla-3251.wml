<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>ZeddYu Lu discovered that the FTP client of Apache Commons Net, a Java
client API for basic Internet protocols, trusts the host from PASV response
by default. A malicious server can redirect the Commons Net code to use a
different host, but the user has to connect to the malicious server in the
first place. This may lead to leakage of information about services running
on the private network of the client.</p>

<p>For Debian 10 buster, this problem has been fixed in version
3.6-1+deb10u1.</p>

<p>We recommend that you upgrade your libcommons-net-java packages.</p>

<p>For the detailed security status of libcommons-net-java please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libcommons-net-java">https://security-tracker.debian.org/tracker/libcommons-net-java</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3251.data"
# $Id: $
