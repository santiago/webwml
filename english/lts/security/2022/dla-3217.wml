<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>g810-led, a LED configuration tool for Logitech Gx10 keyboards,
contained a udev rule to make supported device nodes world-readable
and writable, allowing any process on the system to read traffic
from keyboards, including sensitive data.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.3.3-2+deb10u1.</p>

<p>We recommend that you upgrade your g810-led packages.</p>

<p>For the detailed security status of g810-led please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/g810-led">https://security-tracker.debian.org/tracker/g810-led</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3217.data"
# $Id: $
