<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The security update announced as DLA 2955-1 caused a regression in named due to
an incomplete fix for <a href="https://security-tracker.debian.org/tracker/CVE-2021-25220">CVE-2021-25220</a> when the Forwarders option was configured.
Updated bind9 packages are now available to correct this issue.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1:9.10.3.dfsg.P4-12.3+deb9u12.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>For the detailed security status of bind9 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/bind9">https://security-tracker.debian.org/tracker/bind9</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2955-2.data"
# $Id: $
