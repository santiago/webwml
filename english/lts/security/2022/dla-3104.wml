<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential race condition in Paramiko, a
pure-Python implementation of the SSH algorithm. In particular, unauthorised
information disclosure could have occurred during the creation of SSH private
keys.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24302">CVE-2022-24302</a>

    <p>In Paramiko before 2.10.1, a race condition (between creation and chmod)
    in the write_private_key_file function could allow unauthorized information
    disclosure.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
2.4.2-0.1+deb10u1.</p>

<p>We recommend that you upgrade your paramiko packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3104.data"
# $Id: $
