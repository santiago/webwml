<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two issues were found in GnuTLS, a library implementing the TLS and SSL
protocols. A remote attacker could take advantage of these flaws to
cause an application using the GnuTLS library to crash (denial of
service), or potentially, to execute arbitrary code.</p>

<p>For Debian 10 buster, these problems have been fixed in version
3.6.7-4+deb10u9.</p>

<p>We recommend that you upgrade your gnutls28 packages.</p>

<p>For the detailed security status of gnutls28 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/gnutls28">https://security-tracker.debian.org/tracker/gnutls28</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3070.data"
# $Id: $
