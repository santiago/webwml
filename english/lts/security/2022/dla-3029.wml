<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Joshua Mason discovered that a logic error in the validation of the
secret key used in the <q>local</q> authorisation mode of the CUPS printing
system may result in privilege escalation.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
2.2.1-8+deb9u8.</p>

<p>We recommend that you upgrade your cups packages.</p>

<p>For the detailed security status of cups please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cups">https://security-tracker.debian.org/tracker/cups</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3029.data"
# $Id: $
