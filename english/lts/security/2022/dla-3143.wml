<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential denial of service vulnerability
in strongswan, an IPsec VPN solution.</p>

<p>Strongswan could have queried URLs with untrusted certificates, and this
could potentially lead to a DoS attack by blocking the fetcher thread.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40617">CVE-2022-40617</a>
</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
5.7.2-1+deb10u3.</p>

<p>We recommend that you upgrade your strongswan packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3143.data"
# $Id: $
