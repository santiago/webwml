<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security vulnerabilities were discovered in openjpeg2, a JPEG 2000
image library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9112">CVE-2016-9112</a>

     <p>A floating point exception or divide by zero in the function
     opj_pi_next_cprl may lead to a denial-of-service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20847">CVE-2018-20847</a>

     <p>An improper computation of values in the function
     opj_get_encoding_parameters can lead to an integer overflow.
     This issue was partly fixed by the patch for <a href="https://security-tracker.debian.org/tracker/CVE-2015-1239">CVE-2015-1239</a>.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.1.0-2+deb8u7.</p>

<p>We recommend that you upgrade your openjpeg2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1851.data"
# $Id: $
