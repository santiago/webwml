<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were discovered in openldap, a server
and tools to provide a standalone directory service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13057">CVE-2019-13057</a>

    <p>When the server administrator delegates rootDN (database admin)
    privileges for certain databases but wants to maintain isolation
    (e.g., for multi-tenant deployments), slapd does not properly stop a
    rootDN from requesting authorization as an identity from another
    database during a SASL bind or with a proxyAuthz (RFC 4370) control.
    (It is not a common configuration to deploy a system where the
    server administrator and a DB administrator enjoy different levels
    of trust.)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13565">CVE-2019-13565</a>

    <p>When using SASL authentication and session encryption, and relying
    on the SASL security layers in slapd access controls, it is possible
    to obtain access that would otherwise be denied via a simple bind
    for any identity covered in those ACLs. After the first SASL bind is
    completed, the sasl_ssf value is retained for all new non-SASL
    connections. Depending on the ACL configuration, this can affect
    different types of operations (searches, modifications, etc.). In
    other words, a successful authorization step completed by one user
    affects the authorization requirement for a different user.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.4.40+dfsg-1+deb8u5.</p>

<p>We recommend that you upgrade your openldap packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1891.data"
# $Id: $
