<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in libcaca, a graphics library
that outputs text: integer overflows, floating point exceptions or
invalid memory reads may lead to a denial-of-service (application
crash) if a malformed image file is processed.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.99.beta19-2+deb8u1.</p>

<p>We recommend that you upgrade your libcaca packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1631.data"
# $Id: $
