<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An ACL bypass flaw was found in pacemaker, a cluster resource manager.
An attacker having a local account on the cluster and in the haclient group
could use IPC communication with various daemons directly to perform certain tasks that they would be prevented by ACLs from doing if they went through the configuration.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.1.16-1+deb9u1.</p>

<p>We recommend that you upgrade your pacemaker packages.</p>

<p>For the detailed security status of pacemaker please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pacemaker">https://security-tracker.debian.org/tracker/pacemaker</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2447.data"
# $Id: $
