<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was discovered in the telnetd component of inetutils, a
collection of network utilities.  Execution of arbitrary remote code was
possible through short writes or urgent data.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2:1.9.2.39.3a460-3+deb8u1.</p>

<p>We recommend that you upgrade your inetutils packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2176.data"
# $Id: $
