<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Various vulnerabilities were fixed in nss,
the Network Security Service libraries.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12404">CVE-2018-12404</a>

    <p>Cache side-channel variant of the Bleichenbacher attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18508">CVE-2018-18508</a>

    <p>NULL pointer dereference in several CMS functions
     resulting in a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11719">CVE-2019-11719</a>

    <p>Out-of-bounds read when importing curve25519 private key.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11729">CVE-2019-11729</a>

    <p>Empty or malformed p256-ECDH public keys may
     trigger a segmentation fault.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11745">CVE-2019-11745</a>

    <p>Out-of-bounds write when encrypting with a block cipher.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17006">CVE-2019-17006</a>

    <p>Some cryptographic primitives did not check the
     length of the input text, potentially resulting in overflows.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17007">CVE-2019-17007</a>

    <p>Handling of Netscape Certificate Sequences may crash with a NULL
    dereference leading to a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12399">CVE-2020-12399</a>

    <p>Force a fixed length for DSA exponentiation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6829">CVE-2020-6829</a>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12400">CVE-2020-12400</a>

    <p>Side channel attack on ECDSA signature generation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12401">CVE-2020-12401</a>

    <p>ECDSA timing attack mitigation bypass.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12402">CVE-2020-12402</a>

    <p>Side channel vulnerabilities during RSA key generation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12403">CVE-2020-12403</a>

    <p>CHACHA20-POLY1305 decryption with undersized tag leads to
    out-of-bounds read.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2:3.26.2-1.1+deb9u2.</p>

<p>We recommend that you upgrade your nss packages.</p>

<p>For the detailed security status of nss please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/nss">https://security-tracker.debian.org/tracker/nss</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2388.data"
# $Id: $
