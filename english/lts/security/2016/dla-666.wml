<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in GNU Guile, an
implementation of the Scheme programming language. The Common
Vulnerabilities and Exposures project identifies the following issues.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8605">CVE-2016-8605</a>

    <p>The mkdir procedure of GNU Guile temporarily changed the process'
    umask to zero. During that time window, in a multithreaded
    application, other threads could end up creating files with
    insecure permissions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8606">CVE-2016-8606</a>

    <p>GNU Guile provides a <q>REPL server</q> which is a command prompt that
    developers can connect to for live coding and debugging purposes.
    The REPL server is started by the '--listen' command-line option
    or equivalent API.</p>

    <p>It was reported that the REPL server is vulnerable to the HTTP
    inter-protocol attack.</p>

    <p>This constitutes a remote code execution vulnerability for
    developers running a REPL server that listens on a loopback device
    or private network. Applications that do not run a REPL server, as
    is usually the case, are unaffected.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.0.5+1-3+deb7u1.</p>

<p>We recommend that you upgrade your guile-2.0 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-666.data"
# $Id: $
