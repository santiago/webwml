<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>As previously announced [1][2], the default Java implementation has
been switched from OpenJDK 6 to OpenJDK 7. We strongly recommend to
remove the unsupported OpenJDK 6 packages which will receive no
further security updates.</p>

<p>[1] <a href="https://lists.debian.org/debian-lts-announce/2016/05/msg00007.html">https://lists.debian.org/debian-lts-announce/2016/05/msg00007.html</a>
[2] <a href="https://www.debian.org/News/2016/20160425">https://www.debian.org/News/2016/20160425</a></p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.47+deb7u2.</p>

<p>We recommend that you upgrade your java-common packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-530.data"
# $Id: $
