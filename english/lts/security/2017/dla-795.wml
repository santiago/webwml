<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Numerous security vulnerabilities have been found through fuzzing on
various tiff-related binaries. Crafted TIFF images allows remote
attacks to cause denial of service or, in certain cases arbitrary code
execution through divide-by-zero, out of bunds write, integer and heap
overflow.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3622">CVE-2016-3622</a>

    <p>The fpAcc function in tif_predict.c in the tiff2rgba tool in
    LibTIFF 4.0.6 and earlier allows remote attackers to cause a
    denial of service (divide-by-zero error) via a crafted TIFF image.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3623">CVE-2016-3623</a>

    <p>The rgb2ycbcr tool in LibTIFF 4.0.6 and earlier allows remote
    attackers to cause a denial of service (divide-by-zero) by setting
    the (1) v or (2) h parameter to 0. (Fixed along with
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-3624">CVE-2016-3624</a>.)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3624">CVE-2016-3624</a>

    <p>The cvtClump function in the rgb2ycbcr tool in LibTIFF 4.0.6 and
    earlier allows remote attackers to cause a denial of service
    (out-of-bounds write) by setting the "-v" option to -1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3945">CVE-2016-3945</a>

    <p>Multiple integer overflows in the (1) cvt_by_strip and (2)
    cvt_by_tile functions in the tiff2rgba tool in LibTIFF 4.0.6 and
    earlier, when -b mode is enabled, allow remote attackers to cause
    a denial of service (crash) or execute arbitrary code via a
    crafted TIFF image, which triggers an out-of-bounds write.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3990">CVE-2016-3990</a>

    <p>Heap-based buffer overflow in the horizontalDifference8 function
    in tif_pixarlog.c in LibTIFF 4.0.6 and earlier allows remote
    attackers to cause a denial of service (crash) or execute
    arbitrary code via a crafted TIFF image to tiffcp.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9533">CVE-2016-9533</a>

    <p>tif_pixarlog.c in libtiff 4.0.6 has out-of-bounds write
    vulnerabilities in heap allocated buffers. Reported as MSVR 35094,
    aka "PixarLog horizontalDifference heap-buffer-overflow."</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9534">CVE-2016-9534</a>

    <p>tif_write.c in libtiff 4.0.6 has an issue in the error code path
    of TIFFFlushData1() that didn't reset the tif_rawcc and tif_rawcp
    members. Reported as MSVR 35095, aka "TIFFFlushData1
    heap-buffer-overflow."</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9535">CVE-2016-9535</a>

    <p>tif_predict.h and tif_predict.c in libtiff 4.0.6 have assertions
    that can lead to assertion failures in debug mode, or buffer
    overflows in release mode, when dealing with unusual tile size
    like YCbCr with subsampling. Reported as MSVR 35105, aka
    "Predictor heap-buffer-overflow."</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9536">CVE-2016-9536</a>

    <p>tools/tiff2pdf.c in libtiff 4.0.6 has out-of-bounds write
    vulnerabilities in heap allocated buffers in
    t2p_process_jpeg_strip(). Reported as MSVR 35098, aka
    "t2p_process_jpeg_strip heap-buffer-overflow."</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9537">CVE-2016-9537</a>

    <p>tools/tiffcrop.c in libtiff 4.0.6 has out-of-bounds write
    vulnerabilities in buffers. Reported as MSVR 35093, MSVR 35096,
    and MSVR 35097.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9538">CVE-2016-9538</a>

    <p>tools/tiffcrop.c in libtiff 4.0.6 reads an undefined buffer in
    readContigStripsIntoBuffer() because of a uint16 integer
    overflow. Reported as MSVR 35100.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9540">CVE-2016-9540</a>

    <p>tools/tiffcp.c in libtiff 4.0.6 has an out-of-bounds write on
    tiled images with odd tile width versus image width.  Reported as
    MSVR 35103, aka cpStripToTile heap-buffer-overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10092">CVE-2016-10092</a>

    <p>heap-buffer-overflow in tiffcrop</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10093">CVE-2016-10093</a>

    <p>uint32 underflow/overflow that can cause heap-based buffer
    overflow in tiffcp</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5225">CVE-2017-5225</a>

    <p>LibTIFF version 4.0.7 is vulnerable to a heap buffer overflow in
    the tools/tiffcp resulting in DoS or code execution via a crafted
    BitsPerSample value.</p></li>

<li>Bug #846837

    <p>heap-based buffer verflow in TIFFFillStrip (tif_read.c)</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.0.2-6+deb7u9.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-795.data"
# $Id: $
