<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An information disclosure vulnerability was found in kedpm, a password
manager compatible with the figaro password manager file format. The
history file can reveal the master password if it is provided on the
commandline. The name of entries created or read in the password
manager are also exposed in the history file.</p>

<p>For Debian 7 <q>Wheezy</q>, the master password disclosure issue has been
fixed in version 0.5.0-4+deb7u1. The entries issues has not been fixed
as it requires a rearchitecture of the software.</p>

<p>We recommend that you upgrade your kedpm packages. Note that kedpm has
been removed from the upcoming Debian release ("stretch") and you
should migrate to another password manager as kedpm was abandoned.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-925.data"
# $Id: $
