<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The update for icedove/thunderbird issued as DLA-1087-1 did not build on
i386. This update corrects this.  For reference, the original advisory
text follows.</p>

<p>Multiple security issues have been found in the Mozilla Thunderbird mail
client: Multiple memory safety errors, buffer overflows and other
implementation errors may lead to the execution of arbitrary code or
spoofing.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:52.3.0-4~deb7u2.</p>

<p>We recommend that you upgrade your icedove packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1087-2.data"
# $Id: $
