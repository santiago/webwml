<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that libpam4j, a Java binding for libpam.so, does
not call pam_acct_mgmt(). As a consequence, the PAM account is not
properly
verified. Any user with a valid password but with deactivated or
disabled account was able to log in.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.4-2+deb7u1.</p>

<p>We recommend that you upgrade your libpam4j packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1165.data"
# $Id: $
