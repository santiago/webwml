<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14608">CVE-2017-14608</a>

      <p>An out of bounds read flaw related to kodak_65000_load_raw has been
      reported in dcraw/dcraw.c and internal/dcraw_common.cpp. An attacker
      could possibly exploit this flaw to disclose potentially sensitive
      memory or cause an application crash.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.14.6-2+deb7u3.</p>

<p>We recommend that you upgrade your libraw packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1109.data"
# $Id: $
