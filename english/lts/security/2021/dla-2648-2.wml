<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The patch from latest upstream release to address <a href="https://security-tracker.debian.org/tracker/CVE-2021-30152">CVE-2021-30152</a> was
not portable to stretch-security version causing MediaWiki APIs to
fail. This update includes a patch from upstream REL_31 release which
fix the issue.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1:1.27.7-1~deb9u9.</p>

<p>We recommend that you upgrade your mediawiki packages.</p>

<p>For the detailed security status of mediawiki please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mediawiki">https://security-tracker.debian.org/tracker/mediawiki</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2648-2.data"
# $Id: $
