<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Tenable discovered that in Babel, a set of tools for
internationalizing Python applications, Babel.Locale allows attackers
to load arbitrary locale .dat files (containing serialized Python
objects) via directory traversal, leading to code execution.  This
vulnerability was also previously addressed under <a href="https://security-tracker.debian.org/tracker/CVE-2021-20095">CVE-2021-20095</a> in
other distributions and suites.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.3.4+dfsg.1-2+deb9u1.</p>

<p>We recommend that you upgrade your python-babel packages.</p>

<p>For the detailed security status of python-babel please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-babel">https://security-tracker.debian.org/tracker/python-babel</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2790.data"
# $Id: $
