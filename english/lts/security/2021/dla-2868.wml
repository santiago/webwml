<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been fixed in the AdvanceCOMP recompression utilities.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1056">CVE-2018-1056</a>

    <p>Out-of-bounds heap buffer read in advzip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8379">CVE-2019-8379</a>

    <p>NULL pointer dereference in be_uint32_read().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8383">CVE-2019-8383</a>

    <p>Invalid memory access in adv_png_unfilter_8().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9210">CVE-2019-9210</a>

    <p>Integer overflow in advpng with invalid PNG size.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.20-1+deb9u1.</p>

<p>We recommend that you upgrade your advancecomp packages.</p>

<p>For the detailed security status of advancecomp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/advancecomp">https://security-tracker.debian.org/tracker/advancecomp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2868.data"
# $Id: $
