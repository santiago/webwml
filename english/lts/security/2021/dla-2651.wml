<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was potential directory-traversal vulnerability
in Django, a popular Python-based web development framework.</p>

<p>The MultiPartParser, UploadedFile and FieldFile classes allowed
directory-traversal via uploaded files with suitably crafted file names. In
order to mitigate this risk, stricter basename and path sanitation is now
applied. Specifically, empty file names and paths with dot segments are
rejected.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31542">CVE-2021-31542</a></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1:1.10.7-2+deb9u13.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2651.data"
# $Id: $
