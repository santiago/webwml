<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a series of denial of service
vulnerabilities in Pygments, a popular syntax highlighting library for Python.</p>

<p>A number of regular expressions had exponential or cubic worst-case
complexity which could cause a remote denial of service (DoS) when provided
with malicious input.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27291">CVE-2021-27291</a>

    <p>In pygments 1.1+, fixed in 2.7.4, the lexers used to parse programming
    languages rely heavily on regular expressions. Some of the regular
    expressions have exponential or cubic worst-case complexity and are
    vulnerable to ReDoS. By crafting malicious input, an attacker can cause a
    denial of service.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
2.2.0+dfsg-1+deb9u2.</p>

<p>We recommend that you upgrade your pygments packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2600.data"
# $Id: $
