<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in imagemagick that may lead
to a privilege escalation, denial of service or information leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19667">CVE-2020-19667</a>

    <p>A stack-based buffer overflow and unconditional jump was found in
    ReadXPMImage in coders/xpm.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25665">CVE-2020-25665</a>

    <p>An out-of-bounds read in the PALM image coder was found in
    WritePALMImage in coders/palm.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25666">CVE-2020-25666</a>

    <p>An integer overflow was possible during simple math
    calculations in HistogramCompare() in MagickCore/histogram.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25674">CVE-2020-25674</a>

    <p>A for loop with an improper exit condition was found that can
    allow an out-of-bounds READ via heap-buffer-overflow in
    WriteOnePNGImage from coders/png.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25675">CVE-2020-25675</a>

    <p>A undefined behavior was found in the form of integer overflow
    and out-of-range values as a result of rounding calculations
    performed on unconstrained pixel offsets in the CropImage()
    and CropImageToTiles() routines of MagickCore/transform.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25676">CVE-2020-25676</a>

    <p>A undefined behavior was found in the form of integer overflow
    and out-of-range values as a result of rounding calculations
    performed on unconstrained pixel offsets in CatromWeights(),
    MeshInterpolate(), InterpolatePixelChannel(),
    InterpolatePixelChannels(), and InterpolatePixelInfo(),
    which are all functions in /MagickCore/pixel.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27560">CVE-2020-27560</a>

    <p>A division by Zero was found in OptimizeLayerFrames in
    MagickCore/layer.c, which may cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27750">CVE-2020-27750</a>

    <p>A division by Zero was found in MagickCore/colorspace-private.h
    and MagickCore/quantum.h, which may cause a denial of service</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27751">CVE-2020-27751</a>

    <p>A undefined behavior was found in the form of values outside the
    range of type `unsigned long long` as well as a shift exponent
    that is too large for 64-bit type in MagickCore/quantum-export.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27754">CVE-2020-27754</a>

    <p>A integer overflow was found in IntensityCompare() of
    /magick/quantize.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27756">CVE-2020-27756</a>

    <p>A division by zero was found in ParseMetaGeometry() of
    MagickCore/geometry.c.
    Image height and width calculations can lead to
    divide-by-zero conditions which also lead to undefined behavior.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27757">CVE-2020-27757</a>

    <p>A undefined behavior was found in MagickCore/quantum-private.h
    A floating point math calculation in
    ScaleAnyToQuantum() of /MagickCore/quantum-private.h could lead to
    undefined behavior in the form of a value outside the range of type
    unsigned long long.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27758">CVE-2020-27758</a>

    <p>Undefined behavior was found in the form of values outside the
    range of type `unsigned long long` in coders/txt.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27759">CVE-2020-27759</a>

    <p>In IntensityCompare() of /MagickCore/quantize.c, a
    double value was being casted to int and returned, which in some cases
    caused a value outside the range of type `int` to be returned.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27760">CVE-2020-27760</a>

    <p>In `GammaImage()` of /MagickCore/enhance.c, depending
    on the `gamma` value, it's possible to trigger a
    divide-by-zero condition when a crafted input file
    is processed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27761">CVE-2020-27761</a>

    <p>WritePALMImage() in /coders/palm.c used size_t casts
    in several areas of a calculation which could lead to values outside the
    range of representable type `unsigned long` undefined behavior when a
    crafted input file was processed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27762">CVE-2020-27762</a>

    <p>Undefined behavior was found in the form of values outside the
    range of type `unsigned char` in coders/hdr.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27763">CVE-2020-27763</a>

    <p>Undefined behavior was found in the form of math division by
    zero in MagickCore/resize.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27764">CVE-2020-27764</a>

    <p>Out-of-range values was found under some
    circumstances when a crafted input file is processed in
    /MagickCore/statistic.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27765">CVE-2020-27765</a>

    <p>Undefined behavior was found in the form of math division by
    zero in MagickCore/segment.c when a crafted file is processed</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27766">CVE-2020-27766</a>

    <p>A crafted file that is processed by ImageMagick could trigger
    undefined behavior in the form of values outside the range of
    type `unsigned long`</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27767">CVE-2020-27767</a>

    <p>Undefined behavior was found in the form of values outside the
    range of types `float` and `unsigned char` in MagickCore/quantum.h</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27768">CVE-2020-27768</a>

    <p>An outside the range of representable values of type
    `unsigned int` was found in MagickCore/quantum-private.h</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27769">CVE-2020-27769</a>

    <p>An outside the range of representable values of type
    `float` was found in MagickCore/quantize.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27770">CVE-2020-27770</a>

    <p>Due to a missing check for 0 value of
    `replace_extent`, it is possible for offset `p` to overflow in
    SubstituteString()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27771">CVE-2020-27771</a>

    <p>In RestoreMSCWarning() of /coders/pdf.c there are
    several areas where calls to GetPixelIndex() could result in values
    outside the range of representable for the `unsigned char` type</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27772">CVE-2020-27772</a>

    <p>Undefined behavior was found in the form of values outside the
    range of type `unsigned int` in coders/bmp.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27773">CVE-2020-27773</a>

    <p>Undefined behavior was found in the form of values outside the
    range of type `unsigned char` or division by zero</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27774">CVE-2020-27774</a>

    <p>A crafted file that is processed by ImageMagick could trigger
    undefined behavior in the form of a too large shift for
    64-bit type `ssize_t`.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27775">CVE-2020-27775</a>

    <p>Undefined behavior was found in the form of values outside the
    range of type `unsigned char` in MagickCore/quantum.h</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27776">CVE-2020-27776</a>

    <p>A crafted file that is processed by ImageMagick could trigger
    undefined behavior in the form of values outside the range of
    type unsigned long.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29599">CVE-2020-29599</a>

    <p>ImageMagick mishandles the -authenticate option, which
    allows setting a password for password-protected PDF files. The
    user-controlled password was not properly escaped/sanitized and it was
    therefore possible to inject additional shell commands via
    coders/pdf.c. On debian system, by default the imagemagick policy
    mitigate this CVE.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3574">CVE-2021-3574</a>

    <p>A memory leak was found converting a crafted TIFF file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3596">CVE-2021-3596</a>

    <p>A NULL pointer dereference was found in ReadSVGImage() in
    coders/svg.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20224">CVE-2021-20224</a>

    <p>An integer overflow issue was discovered in ImageMagick's
    ExportIndexQuantum() function in MagickCore/quantum-export.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44267">CVE-2022-44267</a>

    <p>A Denial of Service was found. When it parses a PNG image,
    the convert process could be left waiting for stdin input.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44268">CVE-2022-44268</a>

    <p>An Information Disclosure was found. When it parses a PNG image,
    (e.g., for resize), the resulting image could have embedded
    the content of an arbitrary. file.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
8:6.9.10.23+dfsg-2.1+deb10u2.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>For the detailed security status of imagemagick please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/imagemagick">https://security-tracker.debian.org/tracker/imagemagick</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3357.data"
# $Id: $
