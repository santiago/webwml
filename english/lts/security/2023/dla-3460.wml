<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Erik Krogh Kristensen and Rasmus Petersen from the GitHub Security Lab
discovered a ReDoS (Regular Expression Denial of Service) vulnerability
in python-mechanize, a library to automate interaction with websites
modeled after the Perl module <code>WWW::Mechanize</code>, which could lead to
Denial of Service when parsing a malformed authentication header.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1:0.2.5-3+deb10u1.</p>

<p>We recommend that you upgrade your python-mechanize packages.</p>

<p>For the detailed security status of python-mechanize please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-mechanize">https://security-tracker.debian.org/tracker/python-mechanize</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3460.data"
# $Id: $
