<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security issues have been discovered in libssh, a tiny C SSH library, which
may allows an remote authenticated user to cause a denial of service or inject
arbitrary commands.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14889">CVE-2019-14889</a>

    <p>A flaw was found with the libssh API function ssh_scp_new() in
    versions before 0.9.3 and before 0.8.8. When the libssh SCP client
    connects to a server, the scp command, which includes a
    user-provided path, is executed on the server-side. In case the
    library is used in a way where users can influence the third
    parameter of the function, it would become possible for an attacker
    to inject arbitrary commands, leading to a compromise of the remote
    target.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1667">CVE-2023-1667</a>

    <p>A NULL pointer dereference was found In libssh during re-keying with
    algorithm guessing. This issue may allow an authenticated client to
    cause a denial of service.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.8.7-1+deb10u2.</p>

<p>We recommend that you upgrade your libssh packages.</p>

<p>For the detailed security status of libssh please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libssh">https://security-tracker.debian.org/tracker/libssh</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3437.data"
# $Id: $
