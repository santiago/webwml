<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in Thunderbird: Multiple memory
safety errors and use-after-frees may lead to the execution of arbitrary
code or denial of service.</p>

<p>Debian follows the Thunderbird upstream releases. Support for the 52.x
series has ended, so starting with this update we're now following the
60.x releases.</p>

<p>Between 52.x and 60.x, Thunderbird has undergone significant internal
updates, which makes it incompatible with a number of extensions. For
more information please refer to
<a href="https://support.mozilla.org/en-US/kb/new-thunderbird-60">https://support.mozilla.org/en-US/kb/new-thunderbird-60</a></p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:60.3.0-1~deb8u1.</p>

<p>We recommend that you upgrade your thunderbird packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1575.data"
# $Id: $
