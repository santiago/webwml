<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been discovered in Quagga, a routing
daemon. The Common Vulnerabilities and Exposures project identifies the
following issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5378">CVE-2018-5378</a>

     <p>It was discovered that the Quagga BGP daemon, bgpd, does not
     properly bounds check data sent with a NOTIFY to a peer, if an
     attribute length is invalid. A configured BGP peer can take
     advantage of this bug to read memory from the bgpd process or cause
     a denial of service (daemon crash).</p>

     <p><url "https://www.quagga.net/security/Quagga-2018-0543.txt"></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5379">CVE-2018-5379</a>

     <p>It was discovered that the Quagga BGP daemon, bgpd, can double-free
     memory when processing certain forms of UPDATE message, containing
     cluster-list and/or unknown attributes, resulting in a denial of
     service (bgpd daemon crash).</p>

     <p><url "https://www.quagga.net/security/Quagga-2018-1114.txt"></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5380">CVE-2018-5380</a>

     <p>It was discovered that the Quagga BGP daemon, bgpd, does not
     properly handle internal BGP code-to-string conversion tables.</p>

     <p><url "https://www.quagga.net/security/Quagga-2018-1550.txt"></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5381">CVE-2018-5381</a>

     <p>It was discovered that the Quagga BGP daemon, bgpd, can enter an
     infinite loop if sent an invalid OPEN message by a configured peer.
     A configured peer can take advantage of this flaw to cause a denial
     of service (bgpd daemon not responding to any other events; BGP
     sessions will drop and not be reestablished; unresponsive CLI
     interface).</p>

     <p><url "https://www.quagga.net/security/Quagga-2018-1975.txt"></p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.99.22.4-1+wheezy3+deb7u3.</p>

<p>We recommend that you upgrade your quagga packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1286.data"
# $Id: $
