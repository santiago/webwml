<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Qualys Research Labs discovered multiple vulnerabilities in procps,
a set of command line and full screen utilities for browsing procfs. The
Common Vulnerabilities and Exposures project identifies the following
problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1122">CVE-2018-1122</a>

    <p>top read its configuration from the current working directory if no
    $HOME was configured. If top were started from a directory writable
    by the attacker (such as /tmp) this could result in local privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1123">CVE-2018-1123</a>

    <p>Denial of service against the ps invocation of another user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1124">CVE-2018-1124</a>

    <p>An integer overflow in the file2strvec() function of libprocps could
    result in local privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1125">CVE-2018-1125</a>

    <p>A stack-based buffer overflow in pgrep could result in denial
    of service for a user using pgrep for inspecting a specially
    crafted process.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1126">CVE-2018-1126</a>

    <p>Incorrect integer size parameters used in wrappers for standard C
    allocators could cause integer truncation and lead to integer
    overflow issues.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:3.3.3.3+deb7u1.</p>

<p>We recommend that you upgrade your procps packages.</p>

<p>The Debian LTS team would like to thank Abhijith PA for preparing this update.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1390.data"
# $Id: $
