#use wml::debian::translation-check translation="e0e83a446207444c8d7cbfe76be73fc5338ccab7"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto las siguientes vulnerabilidades en el motor web
webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9862">CVE-2020-9862</a>

    <p>Ophir Lojkine descubrió que copiar una URL desde Web Inspector
    podía dar lugar a inyección de órdenes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9893">CVE-2020-9893</a>

    <p>0011 descubrió que un atacante remoto podía provocar
    terminación inesperada de aplicaciones o ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9894">CVE-2020-9894</a>

    <p>0011 descubrió que un atacante remoto podía provocar
    terminación inesperada de aplicaciones o ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9895">CVE-2020-9895</a>

    <p>Wen Xu descubrió que un atacante remoto podía provocar
    terminación inesperada de aplicaciones o ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9915">CVE-2020-9915</a>

    <p>Ayoub Ait Elmokhtar descubrió que el procesado de contenido web preparado
    maliciosamente podía impedir la imposición de la política de seguridad del
    contenido.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9925">CVE-2020-9925</a>

    <p>Un investigador anónimo descubrió que el procesado de contenido web
    preparado maliciosamente podía dar lugar a ejecución de scripts entre sitios universal («universal cross site scripting»).</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 2.28.4-1~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de webkit2gtk.</p>

<p>Para información detallada sobre el estado de seguridad de webkit2gtk, consulte su
página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4739.data"
