#use wml::debian::translation-check translation="222fe2a8c3db7e4bec94b5d9a4a285666247753c"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el navegador web Chromium:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21201">CVE-2021-21201</a>

    <p>Gengming Liu y Jianyu Chen descubrieron un problema de «uso tras liberar».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21202">CVE-2021-21202</a>

    <p>David Erceg descubrió un problema de «uso tras liberar» en extensiones.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21203">CVE-2021-21203</a>

    <p>asnine descubrió un problema de «uso tras liberar» en Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21204">CVE-2021-21204</a>

    <p>Tsai-Simek, Jeanette Ulloa y Emily Voigtlander descubrieron un
    problema de «uso tras liberar» en Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21205">CVE-2021-21205</a>

    <p>Alison Huffman descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21207">CVE-2021-21207</a>

    <p>koocola y Nan Wang descubrieron un «uso tras liberar» en la base de datos indexada.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21208">CVE-2021-21208</a>

    <p>Ahmed Elsobky descubrió un error de validación de datos en el escáner de códigos QR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21209">CVE-2021-21209</a>

    <p>Tom Van Goethem descubrió un error de implementación en la API Storage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21210">CVE-2021-21210</a>

    <p>@bananabr descubrió un error en la implementación de redes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21211">CVE-2021-21211</a>

    <p>Akash Labade descubrió un error en la implementación de la navegación.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21212">CVE-2021-21212</a>

    <p>Hugo Hue y Sze Yui Chau descubrieron un error en la interfaz de usuario de
    configuración de redes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21213">CVE-2021-21213</a>

    <p>raven descubrió un problema de «uso tras liberar» en la implementación de WebMIDI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21214">CVE-2021-21214</a>

    <p>Se descubrió un problema de «uso tras liberar» en la implementación de redes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21215">CVE-2021-21215</a>

    <p>Abdulrahman Alqabandi descubrió un error en la funcionalidad de llenado automático («Autofill»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21216">CVE-2021-21216</a>

    <p>Abdulrahman Alqabandi descubrió un error en la funcionalidad de llenado automático («Autofill»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21217">CVE-2021-21217</a>

    <p>Zhou Aiting descubrió que se usaba memoria no inicializada en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21218">CVE-2021-21218</a>

    <p>Zhou Aiting descubrió que se usaba memoria no inicializada en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21219">CVE-2021-21219</a>

    <p>Zhou Aiting descubrió que se usaba memoria no inicializada en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21221">CVE-2021-21221</a>

    <p>Guang Gong descubrió una validación insuficiente de entradas no confiables.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21222">CVE-2021-21222</a>

    <p>Guang Gong descubrió un problema de desbordamiento de memoria en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21223">CVE-2021-21223</a>

    <p>Guang Gong descubrió un problema de desbordamiento de entero.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21224">CVE-2021-21224</a>

    <p>Jose Martinez descubrió un error de tipo en la biblioteca javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21225">CVE-2021-21225</a>

    <p>Brendon Tiszka descubrió un problema de acceso a memoria fuera de límites en la
    biblioteca javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21226">CVE-2021-21226</a>

    <p>Brendon Tiszka descubrió un problema de «uso tras liberar» en la implementación
    de redes.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 90.0.4430.85-1~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de chromium.</p>

<p>Para información detallada sobre el estado de seguridad de chromium, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4906.data"
