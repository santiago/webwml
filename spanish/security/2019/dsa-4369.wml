#use wml::debian::translation-check translation="8a1c0e346cc4b60809eb2067ebcb114fe8cc027d"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto múltiples vulnerabilidades en el hipervisor Xen:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19961">CVE-2018-19961</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-19962">CVE-2018-19962</a>

    <p>Paul Durrant descubrió que una gestión incorrecta del TLB podía dar lugar a
    denegación de servicio, a elevación de privilegios o a fugas de información.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19965">CVE-2018-19965</a>

    <p>Matthew Daley descubrió que una gestión incorrecta de la instrucción
    INVPCID podía dar lugar a denegación de servicio por parte de huéspedes PV.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19966">CVE-2018-19966</a>

    <p>Se descubrió que una regresión en la corrección de
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-15595">CVE-2017-15595</a>
    podía dar lugar a denegación de servicio, a elevación
    de privilegios o a fugas de información por parte de un huésped PV.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19967">CVE-2018-19967</a>

    <p>Se descubrió que un error en algunas CPU Intel podía dar lugar a
    denegación de servicio por parte de una instancia huésped.</p></li>

</ul>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 4.8.5+shim4.10.2+xsa282-1+deb9u11.</p>

<p>Le recomendamos que actualice los paquetes de xen.</p>

<p>Para información detallada sobre el estado de seguridad de xen, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/xen">\
https://security-tracker.debian.org/tracker/xen</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4369.data"
