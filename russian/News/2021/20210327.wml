#use wml::debian::translation-check translation="2a3fa2ac1b49f491737f3c89828986a43c00a61e" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 10: выпуск 10.9</define-tag>
<define-tag release_date>2021-03-27</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о девятом обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction avahi "Удаление механизма avahi-daemon-check-dns, который более не требуется">
<correction base-files "Обновление /etc/debian_version для редакции 10.9">
<correction cloud-init "Прекращение записи создаваемых паролей в доступны всем пользователям файлы журналов [CVE-2021-3429]">
<correction debian-archive-keyring "Добавление ключей для bullseye; удаление ключей для jessie">
<correction debian-installer "Использование ABI ядра Linux версии 4.19.0-16">
<correction debian-installer-netboot-images "Повторная сборка с учётом proposed-updates">
<correction exim4 "Исправление использования многопоточных TLS-соединений с GnuTLS; исправление проверки TLS-сертификатов с использованием CNAME; README.Debian: документирование ограничений/пределов проверки серверного сертификата в настройках по умолчанию">
<correction fetchmail "Более не выводить сообщение <q>System error during SSL_connect(): Success</q>; удаление проверки версии OpenSSL">
<correction fwupd "Добавление поддержки SBAT">
<correction fwupd-amd64-signed "Добавление поддержки SBAT">
<correction fwupd-arm64-signed "Добавление поддержки SBAT">
<correction fwupd-armhf-signed "Добавление поддержки SBAT">
<correction fwupd-i386-signed "Добавление поддержки SBAT">
<correction fwupdate "Добавление поддержки SBAT">
<correction fwupdate-amd64-signed "Добавление поддержки SBAT">
<correction fwupdate-arm64-signed "Добавление поддержки SBAT">
<correction fwupdate-armhf-signed "Добавление поддержки SBAT">
<correction fwupdate-i386-signed "Добавление поддержки SBAT">
<correction gdnsd "Исправление переполнения стека из-за слишком больших адресов IPv6 [CVE-2019-13952]">
<correction groff "Повторная сборка с учётом ghostscript 9.27">
<correction hwloc-contrib "Включение поддержки для архитектуры ppc64el">
<correction intel-microcode "Обновление различного микрокода">
<correction iputils "Исправление ошибок округления в ping; исправление повреждения цели в tracepath">
<correction jquery "Исправление выполнения недоверенного кода [CVE-2020-11022 CVE-2020-11023]">
<correction libbsd "Исправление чтения за пределами выделенного буфера памяти [CVE-2019-20367]">
<correction libpano13 "Исправление уязвимости форматной строки">
<correction libreoffice "Не загружать encodings.py из текущего каталога">
<correction linux "Новый стабильный выпуск основной ветки разработки; обновление ABI до версии -16; замена ключей для подписи модулей при безопасной загрузке; rt: обновление до версии 4.19.173-rt72">
<correction linux-latest "Обновление ABI ядра до версии -15; обновление ABI ядра до версии -16">
<correction linux-signed-amd64 "Новый стабильный выпуск основной ветки разработки; обновление ABI до версии -16; замена ключей для подписи модулей при безопасной загрузке; rt: обновление до версии 4.19.173-rt72">
<correction linux-signed-arm64 "Новый стабильный выпуск основной ветки разработки; обновление ABI до версии -16; замена ключей для подписи модулей при безопасной загрузке; rt: обновление до версии 4.19.173-rt72">
<correction linux-signed-i386 "Новый стабильный выпуск основной ветки разработки; обновление ABI до версии -16; замена ключей для подписи модулей при безопасной загрузке; rt: обновление до версии 4.19.173-rt72">
<correction lirc "Нормализация встроенного в код значения ${DEB_HOST_MULTIARCH} в /etc/lirc/lirc_options.conf для поиска неизменённых файлов настройки на всех архитектурах; рекомендуется пакет gir1.2-vte-2.91 вместо несуществующего gir1.2-vte">
<correction m2crypto "Исправление ошибок тестирования, возникающих с новыми версиями OpenSSL">
<correction openafs "Исправление исходящих соединений после времени эпохи Unix 0x60000000 (14 января 2021 года)">
<correction portaudio19 "Обработка EPIPE из alsa_snd_pcm_poll_descriptors, что исправляет аварийную остановку">
<correction postgresql-11 "Новый стабильный выпуск основной ветки разработки; исправление утечки информации в сообщениях об ошибках из-за нарушения ограничений [CVE-2021-3393]; исправление CREATE INDEX CONCURRENTLY для ожидания многопоточно подготовленных транзакций">
<correction privoxy "Ошибки безопасности [CVE-2020-35502 CVE-2021-20209 CVE-2021-20210 CVE-2021-20211 CVE-2021-20212 CVE-2021-20213 CVE-2021-20214 CVE-2021-20215 CVE-2021-20216 CVE-2021-20217 CVE-2021-20272 CVE-2021-20273 CVE-2021-20275 CVE-2021-20276]">
<correction python3.7 "Исправление CRLF-инъекции в http.client [CVE-2020-26116]; исправление переполнения буфера в PyCArg_repr в _ctypes/callproc.c [CVE-2021-3177]">
<correction redis "Исправление ряда проблем с переполнением целых чисел на 32-битных системах [CVE-2021-21309]">
<correction ruby-mechanize "Исправление введения команд [CVE-2021-21289]">
<correction systemd "core: проверка того, чтобы идентификатор команды управления тоже восстанавливался, что исправляет ошибку сегментирования; seccomp: разрешение отключения фильтрации seccomp с помощью переменной окружения">
<correction uim "libuim-data: выполнение преобразования symlink_to_dir для /usr/share/doc/libuim-data в восстановленном пакете для выполнения чистых обновлений с выпуска stretch">
<correction xcftools "Исправление переполнения целых чисел [CVE-2019-5086 CVE-2019-5087]">
<correction xterm "Исправление верхнего предела для буфера выделения с учётом комбинированных символов [CVE-2021-27135]">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2021 4826 nodejs>
<dsa 2021 4844 dnsmasq>
<dsa 2021 4845 openldap>
<dsa 2021 4846 chromium>
<dsa 2021 4847 connman>
<dsa 2021 4849 firejail>
<dsa 2021 4850 libzstd>
<dsa 2021 4851 subversion>
<dsa 2021 4853 spip>
<dsa 2021 4854 webkit2gtk>
<dsa 2021 4855 openssl>
<dsa 2021 4856 php7.3>
<dsa 2021 4857 bind9>
<dsa 2021 4858 chromium>
<dsa 2021 4859 libzstd>
<dsa 2021 4860 openldap>
<dsa 2021 4861 screen>
<dsa 2021 4862 firefox-esr>
<dsa 2021 4863 nodejs>
<dsa 2021 4864 python-aiohttp>
<dsa 2021 4865 docker.io>
<dsa 2021 4867 grub-efi-amd64-signed>
<dsa 2021 4867 grub-efi-arm64-signed>
<dsa 2021 4867 grub-efi-ia32-signed>
<dsa 2021 4867 grub2>
<dsa 2021 4868 flatpak>
<dsa 2021 4869 tiff>
<dsa 2021 4870 pygments>
<dsa 2021 4871 tor>
<dsa 2021 4872 shibboleth-sp>
</table>



<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
