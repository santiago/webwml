#use wml::debian::translation-check translation="4c0e547d89fc6a0f2307d782bbce0a6ac7d74bc2" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 11: выпуск 11.4</define-tag>
<define-tag release_date>2022-07-09</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о четвёртом обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction apache2 "Новый стабильный выпуск основной ветки разработки; исправление подделки HTTP-запросов [CVE-2022-26377], чтения за пределами выделенного буфера памяти [CVE-2022-28330 CVE-2022-28614 CVE-2022-28615], отказа в обслуживании [CVE-2022-29404 CVE-2022-30522], возможных чтений за пределами выделенного буфера памяти [CVE-2022-30556], возможного обхода аутентификации на основе IP [CVE-2022-31813]">
<correction base-files "Обновление /etc/debian_version для редакции 11.4">
<correction bash "Исправление переполнения буфера при чтении на 1 байт больше, приводящего к повреждению многобайтовых символов в подстановке команд">
<correction clamav "Новый стабильный выпуск основной ветки разработки; исправления безопасности [CVE-2022-20770 CVE-2022-20771 CVE-2022-20785 CVE-2022-20792 CVE-2022-20796]">
<correction clementine "Добавление отсутствующей зависимости от libqt5sql5-sqlite">
<correction composer "Исправление введения кода [CVE-2022-24828]; обновление шаблона токена GitHub">
<correction cyrus-imapd "Гарантирование того, чтобы почтовые ящики имеют поле <q>uniqueid</q>, исправляющее обновление до версии 3.6">
<correction dbus-broker "Исправление переполнения буфера [CVE-2022-31212]">
<correction debian-edu-config "Принятие почты из локальной сети, отправленной на адрес root@&lt;моё-имя-сети&gt;; создание узла Kerberos и администраторов службы только в том случае, если они ещё не существуют; проверка того, что пакет libsss-sudo установлен на рабочих станциях с автоматической настройкой на местную сеть связи; исправление именования и отображения очередей печати; поддержка krb5i на бездисковых рабочих станциях; squid: предпочитать поиск DNSv4 поиску DNSv6">
<correction debian-installer "Повторная сборка с учётом proposed-updates; увеличение версии ABI ядра Linux до 16; восстановление некоторых целей сетевой загрузки на armel (openrd)">
<correction debian-installer-netboot-images "Повторная сборка с учётом proposed-updates; увеличение версии ABI ядра Linux до 16; восстановление некоторых целей сетевой загрузки на armel (openrd)">
<correction distro-info-data "Добавление Ubuntu 22.10, Kinetic Kudu">
<correction docker.io "Запуск docker.service после containerd.service, чтобы исправить отключение контейнеров; явная передача пути сокета containerd в dockerd, чтобы последний не запускал containerd самостоятельно">
<correction dpkg "dpkg-deb: исправление условий неожиданного окончания файла при распаковке .deb; libdpkg: не ограничивать виртуальные поля source:* установленными пакетами; Dpkg::Source::Package::V2: всегда исправлять права доступа к tar-архивам основной ветки разработки (регрессия из DSA-5147-1]">
<correction freetype "Исправление переполнения буфера [CVE-2022-27404]; исправление аварийных остановок [CVE-2022-27405 CVE-2022-27406]">
<correction fribidi "Исправление переполнения буфера [CVE-2022-25308 CVE-2022-25309]; исправление аварийной остановки [CVE-2022-25310]">
<correction ganeti "Новый выпуск основной ветки разработки; исправление нескольких проблем с обновлением; исправление живой миграции с QEMU 4 и <q>security_model</q>, имеющей значение <q>user</q> или <q>pool</q>">
<correction geeqie "Исправление клика мышью с зажатой клавишей Ctrl внутри блока выбора">
<correction gnutls28 "Исправление ошибочного вычисления SSSE3 SHA384; исправление разыменования null-указателя [CVE-2021-4209]">
<correction golang-github-russellhaering-goxmldsig "Исправление разыменования null-указателя, вызванного специально сформированными подписями XML [CVE-2020-7711]">
<correction grunt "Исправление обхода каталога [CVE-2022-0436]">
<correction hdmi2usb-mode-switch "udev: добавление суффикса к нодам устройств /dev/video, чтобы их не путать; перенесение правил udev в приоритет 70, чтобы они загружались после 60-persistent-v4l.rules">
<correction hexchat "Добавление отсутствующей зависимости от python3-cffi-backend">
<correction htmldoc "Исправление бесконечного цикла [CVE-2022-24191], переполнений целых чисел [CVE-2022-27114] и переполнения динамической памяти [CVE-2022-28085]">
<correction knot-resolver "Исправление возможный ошибки утверждения в пограничном случае NSEC3 [CVE-2021-40083]">
<correction libapache2-mod-auth-openidc "Новый стабильный выпуск основной ветки разработки; исправление открытого перенаправления [CVE-2021-39191]; исправление аварийной остановки при выполнении действий reload / restart">
<correction libintl-perl "Установка gettext_xs.pm">
<correction libsdl2 "Избегание чтения за пределами выделенного буфера памяти при загрузке специально сформированных файлов в формате BMP [CVE-2021-33657], а также во время преобразования YUV в RGB">
<correction libtgowt "Новый стабильный выпуск основной ветки разработки, поддерживающий новые версии telegram-desktop">
<correction linux "Новый стабильный выпуск основной ветки разработки; увеличение ABI до 16">
<correction linux-signed-amd64 "Новый стабильный выпуск основной ветки разработки; увеличение ABI до 16">
<correction linux-signed-arm64 "Новый стабильный выпуск основной ветки разработки; увеличение ABI до 16">
<correction linux-signed-i386 "Новый стабильный выпуск основной ветки разработки; увеличение ABI до 16">
<correction logrotate "Пропуск блокиировки в случае, если файл состояния доступен для чтения всем пользователям [CVE-2022-1348]; более строгий грамматический разбор настроек для того, чтобы избежать разбора посторонних файлов (таких как дампы ядра)">
<correction lxc "Обновление сервера ключей GPG по умолчанию, исправляющее создание контейнеров, использующих шаблон <q>download</q>">
<correction minidlna "Проверка HTTP-запросов с целью защиты против атак по переназначению DNS [CVE-2022-26505]">
<correction mutt "Исправление переполнения буфера uudecode [CVE-2022-1328]">
<correction nano "Несколько исправлений ошибок, включая исправления аварийных остановок">
<correction needrestart "Добавление поддержки cgroup v2 к функционалу обнаружения cgroup для служб и пользовательских сессий">
<correction network-manager "Новый стабильный выпуск основной ветки разработки">
<correction nginx "Исправление аварийной остановки при загрузке libnginx-mod-http-lua и использовании init_worker_by_lua*; уменьшение риска атаки по смешению содержимого протокола уровня приложения в модуле Mail [CVE-2021-3618]">
<correction node-ejs "Исправление введения шаблона на стороне сервера [CVE-2022-29078]">
<correction node-eventsource "Удаление чувствительных заголовков при перенаправлении на другой источник [CVE-2022-1650]">
<correction node-got "Не разрешать перенаправление на Unix-сокет [CVE-2022-33987]">
<correction node-mermaid "Исправление межсайтового скриптинга [CVE-2021-23648 CVE-2021-43861]">
<correction node-minimist "Исправление загрязнения прототипа [CVE-2021-44906]">
<correction node-moment "Исправление обхода каталога [CVE-2022-24785]">
<correction node-node-forge "Исправление проверки подписи [CVE-2022-24771 CVE-2022-24772 CVE-2022-24773]">
<correction node-raw-body "Исправление потенциального отказа в обслуживании в node-express путём использования node-iconv-lite вместо node-iconv">
<correction node-sqlite3 "Исправление отказа в обслуживании [CVE-2022-21227]">
<correction node-url-parse "Исправление обхода аутентификации [CVE-2022-0686 CVE-2022-0691]">
<correction nvidia-cuda-toolkit "Использование срезов OpenJDK8 для amd64 и ppc64el; проверка применяемости двоичных компонентов java; nsight-compute: перемещение папки 'sections' в место, поддерживающее несколько архитектур; исправление порядка версий nvidia-openjdk-8-jre">
<correction nvidia-graphics-drivers "Новый выпуск основной ветки разработки; переход на ветку 470; исправление отказа в обслуживании [CVE-2022-21813 CVE-2022-21814]; исправление записи за пределы выделенного буфера памяти [CVE-2022-28181], чтения за пределами выделенного буфера памяти [CVE-2022-28183], отказа в обслуживании [CVE-2022-28184 CVE-2022-28191 CVE-2022-28192]">
<correction nvidia-graphics-drivers-legacy-390xx "Новый выпуск основной ветки разработки; исправление записи за пределами выделенного буфера памяти [CVE-2022-28181 CVE-2022-28185]">
<correction nvidia-graphics-drivers-tesla-418 "Новый стабильный выпуск основной ветки разработки">
<correction nvidia-graphics-drivers-tesla-450 "Новый стабильный выпуск основной ветки разработки; исправление записи за пределами выделенного буфера памяти [CVE-2022-28181 CVE-2022-28185], отказа в обслуживании [CVE-2022-28192]">
<correction nvidia-graphics-drivers-tesla-460 "Новый стабильный выпуск основной ветки разработки">
<correction nvidia-graphics-drivers-tesla-470 "Новый пакет, перенос поддержки Tesla на ветку 470; исправление чтения за пределами выделенного буфера памяти [CVE-2022-28181], чтения за пределами выделенного буфера памяти [CVE-2022-28183], отказа в обслуживании [CVE-2022-28184 CVE-2022-28191 CVE-2022-28192]">
<correction nvidia-persistenced "Новый выпуск основной ветки разработки; переход на ветку 470">
<correction nvidia-settings "Новый выпуск основной ветки разработки; переход на ветку 470">
<correction nvidia-settings-tesla-470 "Новый пакет, перенос поддержки Tesla на ветку 470">
<correction nvidia-xconfig "Новый выпуск основной ветки разработки">
<correction openssh "seccomp: добавление системного вызова pselect6_time64 на 32-битных архитектурах">
<correction orca "Исправление использования с webkitgtk 2.36">
<correction php-guzzlehttp-psr7 "Исправление грамматического разбора неправильного заголовка [CVE-2022-24775]">
<correction phpmyadmin "Исправление нескольких SQL-запросов, приводящих к ошибкам на стороне сервера">
<correction postfix "Новый стабильный выпуск основной ветки разработки; не изменять установленный пользователем параметр default_transport в сценарии postinst; if-up.d: не выводить ошибку, если postfix пока не может отправить почту">
<correction procmail "Исправление разыменования null-указателя">
<correction python-scrapy "Не отправлять данные аутентификации со всеми запросами [CVE-2021-41125]; не раскрывать междоменные куки при перенаправлении [CVE-2022-0577]">
<correction ruby-net-ssh "Исправление аутентификации в системах, использующих OpenSSH 8.8">
<correction runc "Выполнять seccomp defaultErrnoRet; не устанавливать наследуемые возможности [CVE-2022-29162]">
<correction samba "Исправление ошибки запуска winbind, если используется <q>allow trusted domains = no</q>; исправление аутентфикации MIT Kerberos; исправление выхода из общего каталога через состояние гонки в mkdir [CVE-2021-43566]; исправление возможного серьёзного повреждения данных из-за отравления кеша Windows-клиента; исправление установки на системах без systemd">
<correction tcpdump "Обновление профиля AppArmor с целью разрешения доступа к файлам *.cap, а также обработки цифрового суффикса в именах файлов с -W">
<correction telegram-desktop "Новый стабильный выпуск основной ветки разработки, восстанавливающий функционирование">
<correction tigervnc "Исправление запуска рабочего стола GNOME при использовании tigervncserver@.service; исправление цветного дисплея, когда vncviewer и X11-сервер используют разный порядок байтов">
<correction twisted "Исправление раскрытия информации при кроссдоменном перенаправлении [CVE-2022-21712], отказа в обслуживании во время рукопожатий SSH [CVE-2022-21716], подделки HTTP-запросов [CVE-2022-24801]">
<correction tzdata "Обновление данных часового пояса для Палестины; обновление списка корректировочных секунд">
<correction ublock-origin "Новый стабильный выпуск основной ветки разработки">
<correction unrar-nonfree "Исправление обхода каталога [CVE-2022-30333]">
<correction usb.ids "Новый выпуск основной ветки разработки; обновление поставляемых данных">
<correction wireless-regdb "Новый выпуск основной ветки разработки; удаление отклонения, добавленного программой установки, чтобы гарантировать, что используются файлы из пакета">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2021 4999 asterisk>
<dsa 2021 5026 firefox-esr>
<dsa 2022 5034 thunderbird>
<dsa 2022 5044 firefox-esr>
<dsa 2022 5045 thunderbird>
<dsa 2022 5069 firefox-esr>
<dsa 2022 5074 thunderbird>
<dsa 2022 5086 thunderbird>
<dsa 2022 5090 firefox-esr>
<dsa 2022 5094 thunderbird>
<dsa 2022 5097 firefox-esr>
<dsa 2022 5106 thunderbird>
<dsa 2022 5107 php-twig>
<dsa 2022 5108 tiff>
<dsa 2022 5110 chromium>
<dsa 2022 5111 zlib>
<dsa 2022 5112 chromium>
<dsa 2022 5113 firefox-esr>
<dsa 2022 5114 chromium>
<dsa 2022 5115 webkit2gtk>
<dsa 2022 5116 wpewebkit>
<dsa 2022 5117 xen>
<dsa 2022 5118 thunderbird>
<dsa 2022 5119 subversion>
<dsa 2022 5120 chromium>
<dsa 2022 5121 chromium>
<dsa 2022 5122 gzip>
<dsa 2022 5123 xz-utils>
<dsa 2022 5124 ffmpeg>
<dsa 2022 5125 chromium>
<dsa 2022 5127 linux-signed-amd64>
<dsa 2022 5127 linux-signed-arm64>
<dsa 2022 5127 linux-signed-i386>
<dsa 2022 5127 linux>
<dsa 2022 5128 openjdk-17>
<dsa 2022 5129 firefox-esr>
<dsa 2022 5130 dpdk>
<dsa 2022 5131 openjdk-11>
<dsa 2022 5132 ecdsautils>
<dsa 2022 5133 qemu>
<dsa 2022 5134 chromium>
<dsa 2022 5136 postgresql-13>
<dsa 2022 5137 needrestart>
<dsa 2022 5138 waitress>
<dsa 2022 5139 openssl>
<dsa 2022 5140 openldap>
<dsa 2022 5141 thunderbird>
<dsa 2022 5142 libxml2>
<dsa 2022 5143 firefox-esr>
<dsa 2022 5145 lrzip>
<dsa 2022 5147 dpkg>
<dsa 2022 5148 chromium>
<dsa 2022 5149 cups>
<dsa 2022 5150 rsyslog>
<dsa 2022 5151 smarty3>
<dsa 2022 5152 spip>
<dsa 2022 5153 trafficserver>
<dsa 2022 5154 webkit2gtk>
<dsa 2022 5155 wpewebkit>
<dsa 2022 5156 firefox-esr>
<dsa 2022 5157 cifs-utils>
<dsa 2022 5158 thunderbird>
<dsa 2022 5159 python-bottle>
<dsa 2022 5160 ntfs-3g>
<dsa 2022 5161 linux-signed-amd64>
<dsa 2022 5161 linux-signed-arm64>
<dsa 2022 5161 linux-signed-i386>
<dsa 2022 5161 linux>
<dsa 2022 5162 containerd>
<dsa 2022 5163 chromium>
<dsa 2022 5164 exo>
<dsa 2022 5165 vlc>
<dsa 2022 5166 slurm-wlm>
<dsa 2022 5167 firejail>
<dsa 2022 5168 chromium>
<dsa 2022 5169 openssl>
<dsa 2022 5171 squid>
<dsa 2022 5172 firefox-esr>
<dsa 2022 5174 gnupg2>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за причин, на которые мы не можем повлиять:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction elog "Не сопровождается; проблемы безопасности">
<correction python-hbmqtt "Не сопровождается и сломан">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию предыдущего стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
