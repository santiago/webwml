#use wml::debian::translation-check translation="6547bb7720bfba1c2481b95c44642a4a6d3df030" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 10: выпуск 10.4</define-tag>
<define-tag release_date>2020-05-09</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о четвёртом обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction apt-cacher-ng "Выполнение изолированного вызова к серверу при включении задачи обслуживания [CVE-2020-5202]; разрешение сжатия .zst для tar-архивов; увеличение размера буфера сжатия строк для чтения файла настроек">
<correction backuppc "Передача имени пользователя службе запуска и остановки при перезагрузке, предотвращение ошибок перезагрузки">
<correction base-files "Обновление для текущей редакции">
<correction brltty "Понижение важности сообщения журнала для того, чтобы избежать выведения слишком многого числа сообщений при использовании новых версий Orca">
<correction checkstyle "Исправление введения внешней XML-сущности [CVE-2019-9658 CVE-2019-10782]">
<correction choose-mirror "Обновление списка зеркал">
<correction clamav "Новый выпуск основной ветки разработки [CVE-2020-3123]">
<correction corosync "totemsrp: уменьшение MTU, чтобы избежать создания пакетов слишком большого размера">
<correction corosync-qdevice "Исправление запуска службы">
<correction csync2 "Ошибка команды HELLO, если используется SSL">
<correction cups "Исправление переполнения динамической памяти [CVE-2020-3898] и ошибки <q>функция `ippReadIO` может считывать поле расширения за пределами буфера</q> [CVE-2019-8842]">
<correction dav4tbsync "Новый выпуск основной ветки разработки, восстанавливающий совместимость с новыми версиями Thunderbird">
<correction debian-edu-config "Добавление файлов правил для Firefox ESR и Thunderbird, чтобы исправить настройки TLS/SSL">
<correction debian-installer "Обновление ABI ядра до 4.19.0-9">
<correction debian-installer-netboot-images "Повторная сборка с учётом proposed-updates">
<correction debian-security-support "Новый стабильный выпуск основной ветки разработки; обновление статуса некоторых пакетов; использование <q>runuser</q> вместо <q>su</q>">
<correction distro-info-data "Добавление Ubuntu 20.10 и вероятной даты окончания поддержки для stretch">
<correction dojo "Исправление неправильного использования регулярного выражения [CVE-2019-10785]">
<correction dpdk "Новый стабильный выпуск основной ветки разработки">
<correction dtv-scan-tables "Новый срез основной ветки разработки; добавление всех текущих мультиплексоров спутников German DVB-T2 и Eutelsat-5-West-A">
<correction eas4tbsync "Новый выпуск основной ветки разработки, восстанавливающий совместимость в новыми версиями Thunderbird">
<correction edk2 "Исправления безопасности [CVE-2019-14558 CVE-2019-14559 CVE-2019-14563 CVE-2019-14575 CVE-2019-14586 CVE-2019-14587]">
<correction el-api "Исправление обновлений с выпуска stretch до выпуска buster при использовании Tomcat 8">
<correction fex "Исправление потенциальной проблемы безопасности в fexsrv">
<correction filezilla "Исправление недоверенного пути поиска [CVE-2019-5429]">
<correction frr "Исправление возможности расширенного следующего сетевого сегмента">
<correction fuse "Удаление устаревших команд udevadm из постустановочных сценариев; не удалять fuse.conf при вычищении">
<correction fuse3 "Удаление устаревших команд udevadm из постустановочных сценариев; не удалять fuse.conf при вычищении; исправление утечки памяти в fuse_session_new()">
<correction golang-github-prometheus-common "Продление периода действия тестовых сертификатов">
<correction gosa "Замена (де)сериализации на json_encode/json_decode, чтобы снизить риск от введения PHP-объектов [CVE-2019-14466]">
<correction hbci4java "Поддержка директивы ЕС касательно платёжных служб (PSD2)">
<correction hibiscus "Поддержка директивы ЕС касательно платёжных служб (PSD2)">
<correction iputils "Исправление проблемы, при которой ping завершается неправильно с кодом ошибки, если имеются непривязанные адреса, которые ещё доступны в возвращаемом значении библиотечного вызова getaddrinfo()">
<correction ircd-hybrid "Использование dhparam.pem для избегания аварийной остановки при запуске">
<correction jekyll "Разрешение использовать ruby-i18n 0.x и 1.x">
<correction jsp-api "Исправление обновлений с выпуска stretch до выпуска buster при использовании Tomcat 8">
<correction lemonldap-ng "Предотвращение нежелательного доступа к административным рабочим местам [CVE-2019-19791]; исправление дополнения GrantSession, которое может не запрещать вход при использовании двухфакторной авторизации; исправление произвольных перенаправлений с OIDC, если не используется redirect_uri">
<correction libdatetime-timezone-perl "Обновление данных">
<correction libreoffice "Исправление смены кадров OpenGL">
<correction libssh "Исправление отказа в обслуживании при обработке ключей AES-CTR с помощью OpenSSL [CVE-2020-1730]">
<correction libvncserver "Исправление переполнения динамической памяти [CVE-2019-15690]">
<correction linux "Новый стабильный выпуск основной ветки разработки">
<correction linux-latest "Обновление ABI ядра до 4.19.0-9">
<correction linux-signed-amd64 "Новый стабильный выпуск основной ветки разработки">
<correction linux-signed-arm64 "Новый стабильный выпуск основной ветки разработки">
<correction linux-signed-i386 "Новый стабильный выпуск основной ветки разработки">
<correction lwip "Исправление переполнения буфера [CVE-2020-8597]">
<correction lxc-templates "Новый стабильный выпуск основной ветки разработки; обработка языков, которые кодируются только с помощью UTF-8">
<correction manila "Исправление отсутствующей проверки прав доступа [CVE-2020-9543]">
<correction megatools "Добавление поддержки для нового формата ссылок mega.nz">
<correction mew "Исправление проверки корректности серверного SSL-сертификата">
<correction mew-beta "Исправление проверки корректности серверного SSL-сертификата">
<correction mkvtoolnix "Повторная сборка с целью усилить зависимость от libmatroska6v5">
<correction ncbi-blast+ "Отключение поддержки SSE4.2">
<correction node-anymatch "Удаление ненужных зависимостей">
<correction node-dot "Запрет исполнения кода после загрязнения прототипа [CVE-2020-8141]">
<correction node-dot-prop "Исправление загрязнения прототипа [CVE-2020-8116]">
<correction node-knockout "Исправление экранирования со старыми версиями Internet Explorer [CVE-2019-14862]">
<correction node-mongodb "Отклонение неверных _bsontypes [CVE-2019-2391 CVE-2020-7610]">
<correction node-yargs-parser "Исправление загрязнения прототипа [CVE-2020-7608]">
<correction npm "Исправление доступа по произвольному пути [CVE-2019-16775 CVE-2019-16776 CVE-2019-16777]">
<correction nvidia-graphics-drivers "Новый стабильный выпуск основной ветки разработки">
<correction nvidia-graphics-drivers-legacy-390xx "Новый стабильный выпуск основной ветки разработки">
<correction nvidia-settings-legacy-340xx "Новый выпуск основной ветки разработки">
<correction oar "Возврат к поведению как в выпуске stretch для Perl-функции Storable::dclone, что исправляет проблему с глубиной рекурсии">
<correction opam "Предпочитать mccs вместо aspcud">
<correction openvswitch "Исправление отмены выполнения vswitchd при добавлении порта и отключения контроллера">
<correction orocos-kdl "Исправление преобразования строк с помощью Python 3">
<correction owfs "Удаление сломанных пакетов Python 3">
<correction pango1.0 "Исправление аварийной остановки в pango_fc_font_key_get_variations(), если ключ пуст">
<correction pgcli "Добавление отсутствующей зависимости от python3-pkg-resources">
<correction php-horde-data "Исправление удалённого выполнения кода аутентифицированным пользователем [CVE-2020-8518]">
<correction php-horde-form "Исправление удалённого выполнения кода аутентифицированным пользователем [CVE-2020-8866]">
<correction php-horde-trean "Исправление удалённого выполнения кода аутентифицированным пользователем [CVE-2020-8865]">
<correction postfix "Новый стабильный выпуск основной ветки разработки; исправление паники Postfix при настройке нескольких фильтров Milter во время MAIL FROM; исправление запуска d/init.d, чтобы сценарий снова начал работать с несколькими виртуальными узлами">
<correction proftpd-dfsg "Исправление проблемы доступа к памяти в коде интерактивной клавиатуры в mod_sftp; корректная обработка сообщений DEBUG, IGNORE, DISCONNECT и UNIMPLEMENTED в режиме интерактивной клавиатуры">
<correction puma "Исправление отказа в обслуживании [CVE-2019-16770]">
<correction purple-discord "Исправление аварийных остановок в ssl_nss_read">
<correction python-oslo.utils "Исправление утечки чувствительной информации через журалы mistral [CVE-2019-3866]">
<correction rails "Исправление межсайтового скриптинга через вспомогательный код для экранирования Javascript [CVE-2020-5267]">
<correction rake "Исправление введения команды [CVE-2020-8130]">
<correction raspi3-firmware "Исправление несовпадения имён dtb в z50-raspi-firmware; исправление загрузки Raspberry Pi семейств 1 и 0">
<correction resource-agents "Исправление ошибки <q>ethmonitor не выводит список интерфейсов без назначенных IP-адресов</q>; удаление более не нужной заплаты xen-toolstack; исправление нестандартного использования в ZFS-агенте">
<correction rootskel "Отключение поддержки нескольких консолей, если используется предварительная настройка установки">
<correction ruby-i18n "Исправление создания gemspec">
<correction rubygems-integration "Избегание устаревших предупреждений, если пользователи устанавливают новую версию Rubygems через <q>gem update --system</q>">
<correction schleuder "Улучшение заплаты для обработки ошибок кодирования, появившихся в предыдущей версии; изменение кодировки по умолчанию на UTF-8; обработка почты с вложениями и закодированными ключами с помощью x-add-key; исправление x-attach-listkey с сообщениями, созданными Thunderbird и включающими защищённые заголовки">
<correction scilab "Исправление загрузки библиотеки с помощью OpenJDK 11.0.7">
<correction serverspec-runner "Поддержка Ruby 2.5">
<correction softflowd "Исправление сломанного агрегирования потока, что может приводить к переполнению таблицы потока и использованию 100% ЦП">
<correction speech-dispatcher "Исправление задержки pulseaudio по умолчанию, которая приводит к <q>хриплому</q> выводу звука">
<correction spl-linux "Исправление зависания">
<correction sssd "Исправление зацикливания sssd_be в случае разрыва LDAP-соединения">
<correction systemd "При авторизации через PolicyKit заново разрешать обратные вызовы/пользовательские данные вместо их кэширования [CVE-2020-1712]; установка 60-block.rules в udev-udeb и initramfs-tools">
<correction taglib "Исправление повреждения содержимого при работе с OGG-файлами">
<correction tbsync "Новый выпуск основной ветки разработки, восстанавливающий совместимость с новыми версиями Thunderbird">
<correction timeshift "Исправление использования временного каталога с предсказуемым именем [CVE-2020-10174]">
<correction tinyproxy "Установка PIDDIR только в том случае, если PIDFILE имеет ненулевую длину строки">
<correction tzdata "Новый стабильный выпуск основной ветки разработки">
<correction uim "Отмена регистрации тех модулей, которые не были установлены, что исправляет регрессию, возникшую в предыдущей загрузке пакета">
<correction user-mode-linux "Исправление ошибок сборки при использовании текущих стабильных ядер">
<correction vite "Исправление аварийной остановки, если имеется более 32 элементов">
<correction waagent "Новый выпуск основной ветки разработки; поддержка совместной установки с cloud-init">
<correction websocket-api "Исправление обновлений с выпуска stretch до выпуска buster при использовании Tomcat 8">
<correction wpa "Не пытаться обнаружить несовпадение PSK в ходе замены ключей PTK; проверка поддержки FT при выборе наборов FT; исправление установки случайных MAC-адресов на некоторых картах">
<correction xdg-utils "xdg-open: исправление проверки pcmanfm и обработки каталогов с пробелами в именах; xdg-screensaver: очистка имени окна до отправки его по D-Bus; xdg-mime: создание каталога настроек, если он ещё не создан">
<correction xtrlock "Исправление блокировки (некоторых) multitouch-устройств при блокировке экрана [CVE-2016-10894]">
<correction zfs-linux "Исправление потенциальных проблем, приводящих к зависанию">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2020 4616 qemu>
<dsa 2020 4617 qtbase-opensource-src>
<dsa 2020 4618 libexif>
<dsa 2020 4619 libxmlrpc3-java>
<dsa 2020 4620 firefox-esr>
<dsa 2020 4623 postgresql-11>
<dsa 2020 4624 evince>
<dsa 2020 4625 thunderbird>
<dsa 2020 4627 webkit2gtk>
<dsa 2020 4629 python-django>
<dsa 2020 4630 python-pysaml2>
<dsa 2020 4631 pillow>
<dsa 2020 4632 ppp>
<dsa 2020 4633 curl>
<dsa 2020 4634 opensmtpd>
<dsa 2020 4635 proftpd-dfsg>
<dsa 2020 4636 python-bleach>
<dsa 2020 4637 network-manager-ssh>
<dsa 2020 4638 chromium>
<dsa 2020 4639 firefox-esr>
<dsa 2020 4640 graphicsmagick>
<dsa 2020 4641 webkit2gtk>
<dsa 2020 4642 thunderbird>
<dsa 2020 4643 python-bleach>
<dsa 2020 4644 tor>
<dsa 2020 4645 chromium>
<dsa 2020 4646 icu>
<dsa 2020 4647 bluez>
<dsa 2020 4648 libpam-krb5>
<dsa 2020 4649 haproxy>
<dsa 2020 4650 qbittorrent>
<dsa 2020 4651 mediawiki>
<dsa 2020 4652 gnutls28>
<dsa 2020 4653 firefox-esr>
<dsa 2020 4654 chromium>
<dsa 2020 4655 firefox-esr>
<dsa 2020 4656 thunderbird>
<dsa 2020 4657 git>
<dsa 2020 4658 webkit2gtk>
<dsa 2020 4659 git>
<dsa 2020 4660 awl>
<dsa 2020 4661 openssl>
<dsa 2020 4663 python-reportlab>
<dsa 2020 4664 mailman>
<dsa 2020 4665 qemu>
<dsa 2020 4666 openldap>
<dsa 2020 4667 linux-signed-amd64>
<dsa 2020 4667 linux-signed-arm64>
<dsa 2020 4667 linux-signed-i386>
<dsa 2020 4667 linux>
<dsa 2020 4669 nodejs>
<dsa 2020 4671 vlc>
<dsa 2020 4672 trafficserver>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за обстоятельств, на которые мы не
можем повлиять:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction getlive "Сломан из-за изменений Hotmail">
<correction gplaycli "Сломан из-за изменений Google API">
<correction kerneloops "Служба основной ветки разработки более недоступна">
<correction lambda-align2 "[arm64 armel armhf i386 mips64el ppc64el s390x] Сломан на отличных от amd64 архитектурах">
<correction libmicrodns "Проблемы безопасности">
<correction libperlspeak-perl "Проблемы безопасности; не сопровождается">
<correction quotecolors "Несовместим с новыми версиями Thunderbird">
<correction torbirdy "Несовместим с новыми версиями Thunderbird">
<correction ugene "Несвободен; ошибка сборки">
<correction yahoo2mbox "Сломан на протяжении нескольких лет">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
