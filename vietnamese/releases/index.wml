#use wml::debian::template title="Các bản phát hành Debian"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="2b2b2d98876137a0efdabdfc2abad6088d4c511f" maintainer="Trần Ngọc Quân"

<p>Debian luôn luôn có ít nhất là ba bản phát hành
được bảo trì tích cực: <q>stable</q>, <q>testing</q> và
<q>unstable</q>.</p>

<dl>
<dt><a href="stable/">stable</a></dt>
<dd>
<p>
  Bản phân phối <q>stable</q> chứa bản phân phối đã phát hành chính thức
  mới nhất của Debian.
</p>
<p>
  Đây là bản phát hành sản phẩm của Debian, cái mà chúng tôi
  khuyến nghị sử dụng trước tiên.
</p>
<p>
  Bản phân phối <q>stable</q> hiện tại của Debian là phiên bản
  <:=substr '<current_initial_release>', 0, 2:>, tên mã <em><current_release_name></em>.
<ifeq "<current_initial_release>" "<current_release>"
  "Nó được phát hành lần đầu vào <current_release_date>."
/>
<ifneq "<current_initial_release>" "<current_release>"
  "Nó được phát hành lần đầu là phiên bản <current_initial_release>
  vào <current_initial_release_date> và cập nhật mới nhất
  của nó, phiên bản <current_release>, được phát hành vào <current_release_date>."
/>
</p>
</dd>

<dt><a href="testing/">testing</a></dt>
<dd>
<p>
  Bản phân phối <q>testing</q> chứa các gói mà nó chưa được
  chấp thuận vào bản phát hành <q>stable</q>, nhưng chúng đang trong hàng đợi
  để được đưa vào đó. Thuận lợi chính của việc sử dụng bản phân phối này là nó có
  nhiều hơn các phần mềm với phiên bản mới.
</p>
<p>
  Xem <a href="$(DOC)/manuals/debian-faq/">Debian FAQ</a> để có thêm thông tin về
  <a href="$(DOC)/manuals/debian-faq/ftparchives#testing"><q>testing</q> là gì</a>
  và <a href="$(DOC)/manuals/debian-faq/ftparchives#frozen">nó trở thành
  <q>stable</q> như thế nào</a>.
</p>
<p>
  Bản phân phối hiện tại <q>testing</q> là <em><current_testing_name></em>.
</p>
</dd>

<dt><a href="unstable/">unstable</a></dt>
<dd>
<p>
  Bản phân phối <q>unstable</q> là nơi mà có thể tìm thấy các nhà phát triển
  tích cực của Debian. Đại thể, bản phân phối này được chạy bởi các nhà phát
  triển và những người thích sống mạo hiểm. Khuyến nghị những người chạy
  bản chưa ổn định nên đăng ký vào bó thư debian-devel-announce để
  nhận được các thông báo về các thay đổi lớn, ví dụ
  như các nâng cấp cái mà có thể làm đổ vỡ.
</p>
<p>
  Bản phân phối <q>unstable</q> luôn được gọi là <em>sid</em>.
</p>
</dd>
</dl>

<h2>Vòng đời phát hành</h2>
<p>
  Debian công bố bản phát hành ổn định mới cơ bản là đều đặn. Người dùng
  có thể trông đợi được hỗ trợ đầy đủ trong vòng 3 năm cho mỗi bản phát hành và 2 năm
  hỗ trợ LTS mở rộng.
</p>

<p>
  Xem trang Wiki <a href="https://wiki.debian.org/DebianReleases">Debian Releases</a>
  và trang Wiki <a href="https://wiki.debian.org/LTS">Debian LTS</a>
  để có thêm thông tin chi tiết.
</p>

<h2>Danh sách các bản phát hành</h2>

<ul>

  <li><a href="<current_testing_name>/">Bản phát hành kế tiếp của Debian có tên mã là 
    <q><current_testing_name></q></a>
      &mdash; chưa có lịch phát hành cụ thể
      </li>
      
  <li><a href="bullseye/">Debian 11 (<q>bullseye</q>)</a>
      &mdash; bản phát hành ổn định <q>stable</q> hiện tại
  </li>
      
  <li><a href="buster/">Debian 10 (<q>buster</q>)</a>
      &mdash; bản phát hành ổn định <q>oldstable</q> cũ hiện tại
  </li>

  <li><a href="stretch/">Debian 9 (<q>stretch</q>)</a>
      &mdash; bản phát hành ổn định <q>oldoldstable</q> cũ cũ, dưới <a href="https://wiki.debian.org/LTS">hỗ trợ LTS</a>
  </li>

  <li><a href="jessie/">Debian 8 (<q>jessie</q>)</a>
      &mdash; bản phát hành đã được lưu trữ, dưới <a href="https://wiki.debian.org/LTS/Extended">hỗ trợ LTS mở rộng</a>
  </li>

  <li><a href="wheezy/">Debian 7 (<q>wheezy</q>)</a>
      &mdash; bản phát hành ổn định (stable) lỗi thời
  </li>

  <li><a href="squeeze/">Debian 6.0 (<q>squeeze</q>)</a>
      &mdash; bản phát hành ổn định (stable) lỗi thời
  </li>

  <li><a href="lenny/">Debian GNU/Linux 5.0 (<q>lenny</q>)</a>
      &mdash; bản phát hành ổn định (stable) lỗi thời
  </li>

  <li><a href="etch/">Debian GNU/Linux 4.0 (<q>etch</q>)</a>
      &mdash; bản phát hành ổn định (stable) lỗi thời
  </li>
  <li><a href="sarge/">Debian GNU/Linux 3.1 (<q>sarge</q>)</a>
      &mdash; bản phát hành ổn định (stable) lỗi thời
  </li>
  <li><a href="woody/">Debian GNU/Linux 3.0 (<q>woody</q>)</a>
      &mdash; bản phát hành ổn định (stable) lỗi thời
  </li>
  <li><a href="potato/">Debian GNU/Linux 2.2 (<q>potato</q>)</a>
      &mdash; bản phát hành ổn định (stable) lỗi thời
  </li>
  <li><a href="slink/">Debian GNU/Linux 2.1 (<q>slink</q>)</a> 
      &mdash; bản phát hành ổn định (stable) lỗi thời
  </li>
  <li><a href="hamm/">Debian GNU/Linux 2.0 (<q>hamm</q>)</a>
      &mdash; bản phát hành ổn định (stable) lỗi thời
  </li>
</ul>

<p>Các trang thông tin điện tử cho các bản phát hành Debian cũ được giữ y nguyên, nhưng
các bản phát hành bản thân chúng chỉ có thể tìm thấy ở một
<a href="$(HOME)/distrib/archive">kho chứa</a> riêng.</p>

<p>Xem <a href="$(HOME)/doc/manuals/debian-faq/">Debian FAQ</a> để được giải thích
<a href="$(HOME)/doc/manuals/debian-faq/ftparchives#sourceforcodenames">tất
cả những tên những mã này đến từ đâu</a>.</p>

<h2>Toàn vẹn dữ liệu trong bản phát hành</h2>

<p>Toàn vẹn dữ liệu được đảm bảo bằng tập tin chứa chữ ký điện tử <code>Release</code>.
Để chắc chắn rằng mọi tập tin trong bản phát hành thuộc về nó, tổng kiểm của
tất cả các tập tin <code>Packages</code> được chép vào tập tin
<code>Release</code>.</p>

<p>Các chữ ký điện tử cho tập tin này được lưu trong tập tin
<code>Release.gpg</code>, sử dụng phiên bản hiện tại của khóa ký kho lưu.
Với bản <q>stable</q> và <q>oldstable</q> một chữ ký thêm được tạo
sử dụng một khóa offline được tạo riêng cho phát hành
bởi một thành viên của <a href="$(HOME)/intro/organization#release-team">\
Nhóm phát hành bản ổn định</a>.</p>
