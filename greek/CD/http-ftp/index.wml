#use wml::debian::cdimage title="Downloading Debian CD/DVD images via HTTP/FTP" BARETITLE=true
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="f4fe84f1063f34ac4bfb75eb963e6fa71ba0e642" maintainer="galaxico"

<div class="tip">
<p><strong>Please do not download CD or DVD images with your web browser the way
you download other files!</strong> The reason is that if your download
aborts, most browsers do not allow you to resume from the point where it
failed.</p>
</div>

<p>Instead, please use a tool that supports resuming - typically
described as a <q>download manager</q>. There are many browser plugins
that do this job, or you might want to install a separate
program. Under Linux/Unix, you can use <a
href="http://aria2.sourceforge.net/">aria2</a>, <a
href="http://dfast.sourceforge.net/">wxDownload Fast</a> or (on the
command line) <q><tt>wget&nbsp;-c&nbsp;</tt><em>URL</em></q> or
<q><tt>curl&nbsp;-C&nbsp;-&nbsp;-L&nbsp;-O&nbsp;</tt><em>URL</em></q>. There
are many more options listed in a <a
href="https://en.wikipedia.org/wiki/Comparison_of_download_managers">comparison
of download managers</a>.</p>

<p>The following Debian images are available for
download:</p>

<ul>

  <li><a href="#stable">Official CD/DVD images of the <q>stable</q> release</a></li>

  <li><a href="#firmware"><b>Unofficial</b> CD/DVD images for <q>stable</q> with
  <b>non-free</b> firmware included</a></li>

  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">Official
  CD/DVD images of the <q>testing</q> distribution (<em>regenerated
  weekly</em>)</a></li>

<comment>
  <li>Unofficial CD/DVD images of the <q>testing</q> and <q>unstable</q>
  distributions by fsn://HU &mdash; <a href="#unofficial">see below</a></li>
</comment>

</ul>

<p>See also:</p>
<ul>

  <li>A complete <a href="#mirrors">list of <tt>debian-cd/</tt> mirrors</a></li>

  <li>For <q>network install</q> (150-300&nbsp;MB) images,
  see the <a href="../netinst/">network install</a> page.</li>

  <li>For <q>netinst</q> images of the <q>testing</q>
  release, both daily builds and known working snapshots, see the <a
  href="$(DEVEL)/debian-installer/">Debian-Installer page</a>.</li>

</ul>

<hr />

<h2><a name="stable">Official CD/DVD images of the <q>stable</q> release</a></h2>

<p>To install Debian on a machine without an Internet connection,
it's possible to use CD images (700&nbsp;MB each) or DVD images (4.7&nbsp;GB each).
Download the first CD or DVD image file, write it using a CD/DVD recorder (or a
USB stick on i386 and amd64 ports), and then reboot from that.</p>

<p>The <strong>first</strong> CD/DVD disk contains all the files necessary
to install a standard Debian system.<br />
To avoid needless downloads, please do <strong>not</strong> download
other CD or DVD image files unless you know that you need packages on
them.</p>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>

<p>The following links point to image files which are up to 700&nbsp;MB
in size, making them suitable for writing to normal CD-R(W) media:</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>

<p>The following links point to image files which are up to 4.7&nbsp;GB
in size, making them suitable for writing to normal DVD-R/DVD+R and
similar media:</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Be sure to have a look at the documentation before you install.
<strong>If you read only one document</strong> before installing, read our
<a href="$(HOME)/releases/stable/i386/apa">Installation Howto</a>, a quick
walkthrough of the installation process. Other useful documentation includes:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Installation Guide</a>,
    the detailed installation instructions</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer
    Documentation</a>, including the FAQ with common questions and answers</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Debian-Installer
    Errata</a>, the list of known problems in the installer</li>
</ul>

<hr />

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<h2><a name="firmware">Unofficial CD/DVD images with non-free firmware included</a></h2>

<div id="firmware_nonfree" class="important">
<p>
If any of the hardware in your system <strong>requires non-free firmware to be
loaded</strong> with the device driver, you can use one of the
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
tarballs of common firmware packages</a> or download an <strong>unofficial</strong> image
including these <strong>non-free</strong> firmwares. Instructions how to use the tarballs
and general information about loading firmware during an installation can
be found in the <a href="../../releases/stable/amd64/ch06s04">Installation Guide</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">unofficial
installation images for <q>stable</q> with firmware included</a>
</p>
</div>

<hr />

<h2><a name="mirrors">Registered mirrors of the <q>debian-cd</q> archive</a></h2>

<p>Note that <strong>some mirrors are not up to date</strong> &mdash;
before downloading, check the version number of the images is the
same as the one listed <a href="../#latest">on this site</a>!
Additionally, note that many sites do not mirror the full set of
images (especially the DVD images) due to its size.</p>

<p><strong>If in doubt, use the <a href="https://cdimage.debian.org/debian-cd/">primary
CD image server</a> in Sweden,</strong> or try
<a href="http://debian-cd.debian.net/">the experimental automatic
mirror selector</a> that will automatically redirect you to a nearby
mirror that is known to have the current version.</p>

<p>Are you interested in offering the Debian CD images on your
mirror? If yes, see the <a href="../mirroring/">instructions on
how to set up a CD image mirror</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"
