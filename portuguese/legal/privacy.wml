#use wml::debian::template title="Política de privacidade" NOCOMMENTS="yes"
#use wml::debian::translation-check translation="1b02209a4926cd501ca7c0996d9295f8aa18d354"

## Tradutores(as) podem adicionar uma nota declarando que a tradução
## é somente informativa e que não possui valor legal, e que as pessoas
## devem ler o documento original a esse respeito.
## Alguns idiomas já fizeram essa nota nas traduções de
## /trademark (marca registrada) e /license (licença), por exemplo.

## Nota adicionada pelo time de tradução do português brasileiro
## Note added by the Brazilian Portuguese translation team
<p>
<strong>NOTA</strong>: Esta tradução é somente informativa e não possui valor
jurídico. Para questões legais, por favor, acesse a
<a href="https://www.debian.org/legal/privacy">página em inglês</a> neste mesmo
site web.
</p>

<p>O <a href="https://www.debian.org/">projeto Debian</a> é uma associação
voluntária de pessoas que tem como objetivo comum a criação de um sistema
operacional livre, denominado de Debian.</p>

<p>Não é requisito para ninguém que deseje usar o Debian que forneça ao projeto
qualquer informação pessoal; ele é livremente disponível para baixar sem
registro ou outra forma de identificação, tanto de espelhos oficiais mantidos
pelo projeto, quanto pelas numerosas entidades terceiras.</p>

<p>Muitos outros aspectos de interação com o projeto Debian envolverão, contudo,
a coleta de informação pessoal. Isto ocorre principalmente na forma de nomes e
endereços de e-mail quando o projeto recebe e-mails; todas as listas de
discussão do Debian são arquivadas publicamente, como também o são todas as
interações com o sistema de acompanhamento de bugs. Isto está de acordo com
nosso <a href="https://www.debian.org/social_contract">Contrato Social</a>,
particularmente com nossa declaração de que retribuiremos para a comunidade do
software livre (n.2) e que nós não esconderemos nossos problemas (n.3). Não
realizamos qualquer processo posterior com as informações que armazenamos, mas
há casos em que as informações são automaticamente compartilhadas com
terceiros(as) (como os e-mails para as listas ou interações com o sistema de
acompanhamento de bugs).</p>

<p>A lista abaixo classifica os diversos serviços executados pelo projeto, as
informações usadas por esses serviços e os motivos de sua requisição.</p>

<p>Perguntas sobre esses serviços devem ser dirigidas, primeiramente, para o(a)
dono(a) do serviço. Se não estiver claro quem é esta pessoa, você pode contatar
o time de proteção de dados em
<a href="mailto:data-protection@debian.org">data-protection@debian.org</a>, que
tentará direcionar seu questionamento para o time correto.</p>

<p>Por favor, observe que (a menos que declarado de outra forma nesta página)
servidores e serviços sob o domínio <strong>debian.net</strong> não são parte do
projeto Debian oficial;
eles são executados por pessoas que são associadas com o projeto em vez do
projeto em si mesmo.
Perguntas sobre quais dados exatamente esses serviços mantêm devem ser
direcionadas para os(as) donos(as) do serviço em vez do projeto Debian.</p>

<h2>Contribuidores(as) (<a href="https://contributors.debian.org/">contributors.debian.org</a>)</h2>

<p>O site web Contribuidores(as) Debian fornece um agregado de dados sobre qual
parte do projeto Debian alguém contribuiu, se o fez pelo preenchimento de um
relatório de bug, fazendo um upload para um repositório, postando em uma lista
de discussão ou diversas outras interações com o projeto. Ele recebe as
informações de tais serviços (detalhes de identificadores como o nome de login
e o horário da última contribuição), e fornece um ponto central de referência
para se ver onde o projeto está armazenando informações sobre uma pessoa.</p>

<h2>O repositório (<a href="https://ftp.debian.org/debian/">ftp.debian.org</a>)</h2>

<p>O método primário de distribuição do Debian se dá através de sua rede pública
de repositórios. O repositório consiste de todos os pacotes binários e seu
código-fonte correspondente, o que incluirá informações pessoais na forma de
nomes e endereços de e-mail armazenados como parte de registros de alterações,
informação de copyright e documentação em geral. A maioria dessas informações
é fornecida através do código-fonte distribuído pelos(as) autores(as) de
software (upstream), com o Debian adicionando informações para rastreamento de
autoria e direitos de autor para assegurar que as licenças estão sendo
corretamente documentadas, e que a Definição Debian de Software Livre (DFSG)
tenha sido seguida.</p>

<h2>Sistema de acompanhamento de bugs (<a href="https://bugs.debian.org/">bugs.debian.org</a>)</h2>

<p>O sistema de acompanhamento de bugs (BTS) interage via e-mail e armazena
todos os e-mails recebidos em relação a um bug como parte do histórico desse
bug. Para que o projeto possa efetivamente lidar com problemas encontrados na
distribuição, e para permitir que os(as) usuários(as) vejam esses
problemas e se há correções ou soluções disponíveis, o sistema de acompanhamento
de bugs por inteiro está abertamente acessível. Portanto, qualquer informação
enviada ao BTS, incluindo nomes e endereços de e-mail como parte dos cabeçalhos
de e-mail, estará arquivada e publicamente disponível.</p>

<h2>DebConf (<a href="https://www.debconf.org/">debconf.org</a>)</h2>

<p>A estrutura de inscrição da DebConf armazena detalhes sobre os(as)
participantes da conferência. Estes são requisitados para determinar a
elegibilidade para bolsas, associação com o projeto e para contactar
participantes com detalhes apropriados. As informações também podem ser
compartilhadas com fornecedores(as) da conferência, por exemplo, participantes
que se hospedem nas acomodações fornecidas pela conferência terão seus nomes e
data de participação compartilhados com o(a) fornecedor(a) da hospedagem.</p>

<h2>LDAP dos(as) desenvolvedores(as) (<a href="https://db.debian.org">db.debian.org</a>)</h2>

<p>Contribuidores(as) do projeto (desenvolvedores(as) e outras pessoas com
contas de convidados(as)), que possuam contas de acesso às máquinas dentro da
infraestrutura Debian, têm suas informações armazenadas dentro da infraestrutura
LDAP do projeto. Armazena-se principalmente nome, nome de usuário(a) e
informação de autenticação. Entretanto, possibilita-se meios para que
contribuidores(as) forneçam informações adicionais como gênero, mensageiros
instantâneos (IRC/XMPP), país e endereço ou telefone, e uma mensagem se
estiverem de férias.
</p>

<p>
Nome, nome de usuário(a) e detalhes fornecidos voluntariamente estarão
livremente disponíveis através da interface web ou da busca do LDAP. Detalhes
adicionais são compartilhados somente com outras pessoas que têm conta de acesso
na infraestrutura do Debian, com o objetivo de fornecer um local centralizado
para os(as) membros(as) do projeto trocarem essas informações de contato. São
informações que não são explicitamente coletadas a qualquer momento e sempre
podem ser removidas através do login à interface web do db.debian.org ou
enviando um e-mail assinado para a interface de e-mail. Veja
<a href="https://db.debian.org/">https://db.debian.org/</a> e
<a href="https://db.debian.org/doc-general.html">https://db.debian.org/doc-general.html</a>
para mais detalhes.
</p>

<h2>Gitlab (<a href="https://salsa.debian.org/">salsa.debian.org</a>)</h2>

<p>O salsa.debian.org fornece uma instância do
<a href="https://about.gitlab.com/">GitLab</a>, uma ferramenta DevOps de
administração de ciclo de vida.
É primariamente utilizada pelo projeto para permitir que contribuidores(as) do
projeto hospedem repositórios de software usando o Git e para encorajar a
colaboração entre contribuidores(as). Como resultado, ele requer diversas partes
de informação pessoal para administrar as contas. Para membros(as) do projeto,
isto está atrelado ao sistema LDAP central do Debian, mas convidados(as) também
podem se registrar para ter uma conta e terão que fornecer nome e e-mail para
facilitar a configuração e utilização dessa conta.</p>

<p>O propósito principal do salsa é gerir nosso histórico do git. Veja abaixo.</p>

<h2><a name="git">git</a></h2>

<p>O Debian mantém múltiplos servidores git, contendo código-fonte e materiais
similares, e seus históricos de revisão e contribuição.
Contribuidores(as) individuais do Debian também tem suas informações
administradas no git.</p>

<p>O histórico do git contém o nome e o endereço de e-mail de contribuidores(as)
(incluindo relatores(as) de bug, autores(as), revisores(as) e assim por diante).
O git retém essas informações permanentemente em um histórico somente de adição.
Nós usamos um histórico somente de adição porque ele possui importante
propriedades de integridade, particularmente a significante proteção contra
alterações não rastreadas. Mantemos esse histórico indefinidamente para que
sempre possamos verificar o copyright e outras situações legais das
contribuições, de modo que sempre possamos identificar os(as) criadores(as) por
razões de integridade de software.</p>

<p>A natureza somente de adição do sistema git implica que qualquer modificação
nos detalhes desses commits, uma vez que estejam incorporados ao
repositório, é extremamente disruptivo e, em alguns casos, impossível
(quando, por exemplo, commits assinados estão em uso). Então evitamos isso ao
máximo, a não ser em casos extremos.
Quando apropriado, usamos funcionalidades do git (por exemplo,
<code>mailmap</code>) para ordenar aquele histórico de informações pessoais que,
embora esteja armazenado, pode ser suprimido ou corrigido quando exibido ou
usado.
</p>

<p>A menos que existam motivos excepcionais para que se faça o contrário,
os históricos do git do Debian, incluindo as informações pessoais sobre
contribuidores(as), são completamente públicos.</p>

<h2>Gobby (<a href="https://gobby.debian.org/">gobby.debian.org</a>)</h2>

<p>O gobby é um editor de texto colaborativo e on-line, que rastreia as
contribuições e mudanças de usuários(as) conectados(as). Nenhuma autenticação é
requisitada para se conectar ao sistema, e os(as) usuários(as) podem escolher
qualquer nome de usuário(a) que desejarem. Contudo, enquanto nenhuma ação é
feita pelo serviço para rastrear quem é o(a) dono(a) do nome de usuário(a), deve
ficar claro que pode ser possível mapear os nomes de usuários(as) até pessoas,
baseado no uso comum deste nome de usuário(a) ou do conteúdo postado em um
documento colaborativo dentro do sistema. </p>

<h2>Listas de discussão (<a href="https://lists.debian.org/">lists.debian.org</a>)</h2>

<p>As listas de discussão são o mecanismo primário de comunicação do projeto
Debian. Quase todas as listas de discussão relativas ao projeto são abertas, e
desse modo disponíveis para qualquer pessoa ler e/ou postar. Todas as listas
também são arquivadas; para listas públicas isto significa que são acessíveis
pela web. Isto materializa o compromisso com a transparência e ajuda nossos(as)
usuários(as) e desenvolvedores(as) a entenderem o que está acontecendo no
projeto, bem como possibilita compreenderem as razões históricas de certos
aspectos do projeto. Devido à natureza do e-mail, esses arquivos possivelmente
manterão informações pessoais como nomes e endereços de e-mail.</p>

<h2>Listas Alioth
(<a href="https://alioth-lists.debian.net/">alioth-lists.debian.net</a>)</h2>

<p>As listas Alioth fornecem listas de discussão adicionais e específicas
sobre certos assuntos para o projeto Debian. A maior parte das listas de
discussão neste sistema são abertas, e assim disponíveis para qualquer pessoa
ler e/ou postar. Muitas listas também são arquivadas; para listas públicas isto
significa que são acessíveis pela web. Isto materializa o compromisso do projeto
com a transparência e ajuda nossos(as) usuários(as) e desenvolvedores(as) a
entenderem o que está acontecendo no projeto, bem como possibilita
compreenderem as razões históricas de certos aspectos do projeto. Devido
à natureza do e-mail, esses arquivos possivelmente manterão informações
pessoais como nomes e endereços de e-mail.</p>

<p>O serviço de listas Alioth é também conhecido como <tt>lists.alioth.debian.org</tt>.</p>

<h2>Site de novos(as) membros(as) (<a href="https://nm.debian.org/">nm.debian.org</a>)</h2>

<p>Contribuidores(as) do projeto Debian que desejam formalizar seu envolvimento
podem solicitar aplicar para o processo de novos(as) membros(as). Isso permite
que eles(as) obtenham a capacidade de fazer upload de seus próprios pacotes
(tornando-se mantenedores(as) Debian) ou que tornem-se membros(as) plenos(as)
votantes do projeto com direito a conta (desenvolvedores(as) Debian, nas suas
variações uploading e non-uploading). Como parte deste processo, diversos
detalhes pessoais são coletados, começando com nome, endereço de e-mail e
detalhes da chave de encriptação/assinatura. As solicitações completas do
projeto também demandam que o(a) candidato(a) se envolve com um "Application
Manager" que irá iniciar uma conversa por e-mail para garantir que o(a) novo(a)
membro(a) compreenda os princípios por trás do Debian e possua as habilidades
apropriadas para interagir com a infraestrutura do projeto.
Esta conversa por e-mail é arquivada e fica disponível para o(a) candidato(a) e
os "Application Managers" através da interface nm.debian.org. Adicionalmente,
detalhes pendentes dos(as) candidatos(as) ficam publicamente visíveis no site,
permitindo que qualquer pessoa veja o estado do processo de novo(a) membro(a)
no projeto, de modo a assegurar um nível apropriado de transparência.</p>

<h2>Concurso de popularidade (<a href="https://popcon.debian.org/">popcon.debian.org</a>)</h2>

<p>O "popcon" rastreia quais pacotes estão instalados em um sistema Debian para
habilitar a coleta de estatísticas a respeito de quais pacotes são amplamente
usados e quais não estão mais em uso. Ele usa o pacote opcional
"popularity-contest" para reunir essas informações e requer consentimento
explícito (opt-in) para fazê-lo. Isto fornece uma orientação útil sobre onde
dedicar os esforços dos(as) desenvolvedores(as); por exemplo, quando da migração
para novas versões de bibliotecas, o esforço gasto na portabilidade de
aplicações mais antigas. Cada instância do popcon gera uma identificação única e
aleatória de 128 bits que é usada para rastrear as submissões da mesma máquina.
Nenhuma tentativa é feita para mapear esta identificação à pessoa. As submissões
são realizadas via e-mail ou HTTP e, nesse sentido, é possível que informações
sejam vazadas na forma de endereços IP usados para acesso ou por cabeçalhos de
e-mail. Esta informação somente fica disponível para administradores(as) de
sistema do Debian e administradores(as) popcon; todos os metadados são removidos
antes que as submissões fiquem acessíveis ao projeto como um todo. Entretanto,
usuários(as) devem atentar para o fato de que assinaturas únicas de pacotes
(como pacotes criados localmente ou pacotes com contagens baixas de instalação)
podem fazer com que uma pessoa em particular possa ser deduzida a partir de uma
máquina.</p>

<p>Submissões brutas ficam armazenadas por 24 horas, para permitir repetição no
caso de problemas com os mecanismos de processamento. Submissões anonimizadas
são mantidas por no máximo 20 dias. Relatórios consolidados, que não contêm
informação pessoal identificável, são mantidos indefinidamente.</p>

<h2>snapshot (<a href="http://snapshot.debian.org/">snapshot.debian.org</a>)</h2>

<p>O repositório snapshot fornece uma visão histórica do repositório Debian
(o ftp.debian.org explicado acima), permitindo o acesso a pacotes antigos
baseado em datas e números de versão. Ele não carrega informações adicionais do
repositório principal (e pode conter informação pessoal na forma de nomes +
endereço de e-mail dentro de registros de alterações, declarações de copyright e
outras documentações), mas pode conter pacotes que não fazem mais parte da
versão do Debian que está sendo disponibilizada. Isto fornece um recurso útil
para desenvolvedores(as) e usuários(as) quando rastreiam regressões nos pacotes
de software, ou quando fornecem um ambiente específico para executar uma
determinada aplicação.
</p>

<h2>Votos (<a href="https://vote.debian.org/">vote.debian.org</a>)</h2>

<p>O sistema de acompanhamento de votos (devotee) acompanha o estado das
resoluções gerais em andamento e os resultados das votações anteriores. Na
maioria dos casos, isto significa que após o período de votação terminar,
detalhes de quem votou (nome de usuário(a) + mapeamento de nomes) e como votaram
tornam-se publicamente visíveis. Somente membros(as) do projeto são votantes
válidos(as) para os propósitos do devotee, e somente votantes válidos(as) são
rastreados pelo sistema.</p>

<h2>Wiki (<a href="https://wiki.debian.org/">wiki.debian.org</a>)</h2>

<p>A wiki do Debian fornece recursos de suporte e documentação para o projeto e
é editável por qualquer pessoa. Como parte disso, as contribuições são
rastreadas periodicamente e associada a contas de usuário(a) da wiki; cada
modificação em uma página é rastreada para permitir que edições erradas sejam
revertidas e informações atualizadas sejam facilmente examinadas. Este
rastreamento fornece detalhes sobre o(a) usuário(a) responsável pela alteração,
que podem ser usados para prevenir abusos de edição ao bloquear usuários(as) ou
endereços IP. Contas de usuário(a) também permitem que usuários(as) inscrevam-se
às páginas que querem acompanhar quanto a mudanças, ou vejam detalhes das
alterações através de todo a wiki desde da última vez que olharam. No geral,
contas de usuário(a) são nomeadas seguindo o nome do(a) usuário(a), mas nenhuma
validação é feita nas contas; o(a) usuário(a) pode escolher qualquer nome de
conta que esteja livre. Um endereço de e-mail é requisitado para que seja
possível fornecer um mecanismo de redefinição de senha de conta, e para
notificar o(a) usuário(a) sobre as mudanças nas páginas que foram
subscritas.</p>

<h2>Servidor git dgit (<a href="https://browse.dgit.debian.org/">*.dgit.debian.org</a>)</h2>

<p>O servidor git dgit é o repositório do git (controle de revisão) do histórico
de pacotes enviados para o Debian via <code>dgit push</code>, usado por
<code>dgit clone</code>.
Ele contém informação similar para o repositório Debian e da instância gitlab
Salsa: cópias das (e snapshots passados das) mesmas informações - no
formato git.
Exceto por pacotes que estejam pela primeira vez sendo examinados pelos(as)
ftpmasters para inclusão no Debian: toda essa informação git é sempre pública,
e armazenada para sempre.
Veja <a href="#git">git</a>, acima.</p>

<p>Para propósitos de controle de acesso, existe uma lista mantida a mão
das chaves públicas de ssh dos(as) mantenedores(as) Debian que pediram para
serem adicionados(as). Esta lista administrada manualmente não é pública. Nós
esperamos aposentá-la e, em vez dela, usar dados completos mantidos por
outro serviço Debian.</p>

<h2>Echelon</h2>

<p>O Echelon é um sistema usado pelo projeto para rastrear as atividades dos(as)
membros(as); em particular, ele acompanha as listas de discussão e as
infraestruturas de repositório, procurando por mensagens e uploads, para
registrar que um(a) membro(a) Debian está ativo. Somente a atividade mais
recente é armazenada no registro da pessoa no LDAP. É, portanto, limitado para
somente rastrear detalhes de pessoas que têm contas dentro da infraestrutura
Debian. Esta informação é usada para determinar se um(a) membro(a) do projeto
está inativo(a) ou ausente, e assim pode ocorrer uma requisição operacional para
bloquear a conta dele(a) ou, ao contrário, reduzir as permissões de acesso para
garantir que os sistemas Debian permaneçam seguros.</p>

<h2>Registro (logging) relativo a serviços</h2>

<p>Além dos serviços explicitamente listados acima, os logs da infraestrutura do
Debian detalham sobre acessos ao sistema com o propósito de garantir a
disponibilidade e a confiança do serviço, e para habilitar a depuração
e o diagnóstico de problemas quando eles aparecem. Este registro inclui detalhes
de e-mails enviados/recebidos através da infraestrutura do Debian, requisições
de acesso às páginas web enviadas para a infraestrutura do Debian, e informação
de login para os sistemas Debian (como logins via SSH nas máquinas do projeto).
Nenhuma dessas informações é usada para qualquer outro propósito que não
requerimentos operacionais, e somente são armazenadas por 15 dias no caso de
logs de servidores web, 10 dias para logs de e-mail e 4 semanas para logs de
autenticação/ssh.</p>
