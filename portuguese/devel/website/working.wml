#use wml::debian::template title="Como trabalhar nas páginas web do Debian" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="b5c35a479efa1910b978df5114fd87e4142e3892"

<toc-display/>

<toc-add-entry name="general">Informações gerais</toc-add-entry>

<h3>Recursos necessários</h3>

<p>Se você quer trabalhar com nosso site web, por favor esteja preparado(a) para
armazenar ao menos 740MB de dados no seu disco. Isto reflete o tamanho atual do
repositório-fonte. Se você (acidentalmente) reconstruir todas as páginas, você
precisará de ao menos três vezes esse espaço.</p>

<h3><q>O que são essas linhas começando com "#"?</q></h3>

<p>No WML, uma linha começando com "#" é um comentário. Isto é preferível aos
comentários normais em HTML, já que eles não são mostrados na página final.</p>

<p>Por favor, leia a página <a href="using_wml">usando WML</a> para mais
informações sobre WML.</p>

<toc-add-entry name="etiquette">Etiqueta para editores(as)</toc-add-entry>

<h3><q>Eu posso modificar essa página?</q></h3>

<p>Isso depende. Se você ver um pequeno engano, como um erro ortográfico,
simplesmente corrija-o.</p>

<p>Se você notar que alguma pequena informação está faltando, sinta-se livre
para consertá-la também.</p>

<p>Se sentir que alguma coisa está horrível e precisa ser reescrita, leve-a
para a lista de discussão debian-www para que possa ser discutida. Nós
provavelmente concordaremos com você.</p>

<p>Se você observar que existe um problema em um modelo (ou seja, em um arquivo
no diretório webwml/english/template/debian), por favor, pense sobre a
modificação antes de enviar seu commit, porque as alterações em modelos
geralmente fazem com que grandes porções do site sejam reconstruídas.</p>

<h3>Quando adicionar novos diretórios, adicione também o Makefile!</h3>

<p>Cuidados devem ser tomados quando se adiciona um novo diretório ao git. Se
o diretório atual está listado em ../Makefile, então você <b>deverá</b> criar
um Makefile nele &mdash; do contrário, o <tt>make</tt> resultará em uma
mensagem de erro.</p>

<h3>Use inglês claro e simples</h3>

<p>Já que as páginas do Debian são lidas por falantes não nativos do idioma
inglês e são traduzidas em outras línguas, é melhor escrever em inglês
claro e simples, e evitar o uso de gírias, emoticons e expressões obscuras.
</p>

<p>Se você usar alguma dessas coisas, adicione um comentário no arquivo
explicando seu significado.</p>

<p>
Em caso de dúvida, ou para uma revisão da sua proposta, por favor contate o <a
href="mailto:debian-l10n-english@lists.debian.org">time de localização em língua inglesa</a>.
</p>

<h3>Procure pelos READMEs</h3>

<p>Alguns diretórios contêm um README para lhe ajudar a entender
como aquele diretório está organizado. Esses arquivos devem fornecer
quaisquer informações especiais necessárias quando se trabalha naquela área.</p>

<h3>Separe as alterações de conteúdo das alterações de formatação</h3>

<p>Sempre faça correções ou commits separados para alterações de conteúdo e para
alterações de formatação. Quando elas estão combinadas, é muito difícil para
os(as) tradutores(as) encontrarem as diferenças. Se você executar
<kbd>git diff -u</kbd> com tais alterações misturadas, você mesmo(a) poderá
ver a bagunça.</p>

<p>Em geral, evite alterações aleatórias de formatação. Fazer partes de
páginas mais antigas compatíveis com XHTML/XML não deveria estar no mesmo
commit que outras alterações.
(Coisas novas podem e devem ser feitas adequadamente de início, claro).</p>

<h3>Atualize as traduções também, se possível</h3>

<p>Algumas modificações são independentes do idioma usado no arquivo WML, como
alterações em URLs ou código Perl embutido. A correção de erros ortográficos cai
na mesma categoria, porque os(as) tradutores(as) geralmente os ignoram quando
traduzem.
Com tais alterações independentes de idioma, você pode fazer a mesma
modificação em todos os arquivos traduzidos sem realmente saber uma outra
língua; e seguramente também pode aumentar a versão nos cabeçalhos
translation-check.</p>

<p>Não é terrivelmente difícil que os(as) tradutores(as) façam este trabalho
eles(as) mesmos(as), e pode ser inconveniente para editores(as) anglófonos(as)
ter quer fazer uma verificação completa. Contudo, nós ainda assim encorajamos as
pessoas a fazer isso de modo a evitar aborrecimentos a uma dúzia de pessoas
devido a algo que pode ser feito rapidamente por uma única pessoa.</p>

<p>Além disso, para que essas modificações sejam mais fáceis de aplicar, você
pode usar o script
<a href="using_git#translation-smart-change"><code>smart_change.pl</code></a>
do diretório mais alto na hierarquia no módulo git webwml.</p>

<toc-add-entry name="links">Links</toc-add-entry>

<h3><q>Este link não parece correto. Posso alterá-lo?</q></h3>

<p>Devido ao modo como os servidores web são configurados (usando
<a href="content_negotiation">negociação de conteúdo</a>),
você não deveria ter que modificar qualquer um dos links internos.
Na verdade, nós sugerimos que você não o faça. Antes de alterá-lo,
escreva para a lista de discussão debian-www se sentir que um link
está incorreto.</p>

<h3>Corrigindo links</h3>

<p>Se você perceber que um link para um site web externo resulta em
redirecionamento (301, 302, um &lt;meta&gt; redirecionamento ou uma página
dizendo <q>Esta página mudou.</q>), por favor conte à debian-www sobre isso.</p>

<p>Se você encontrar um link quebrado (404, 403 ou uma página que não é o que
o link dizia que era), por favor corrija-o e diga à debian-www sobre isso,
de modo que os(as) tradutores(as) fiquem cientes. Melhor ainda, conserte o link
em todas as outras traduções e atualize os cabeçalhos translation-check se
possível.</p>

<toc-add-entry name="special">Separando texto de dados</toc-add-entry>

<h3><q>O que são esses arquivos foo.def e foo.data?</q></h3>

<p>Para facilitar a manutenção de traduções atualizadas, nós separamos as
partes genéricas (dados) das partes textuais (texto) de algumas páginas.
Os(As) tradutores(as) somente precisam copiar e traduzir as partes textuais,
as partes genéricas serão adicionadas automaticamente.</p>

<p>Um exemplo pode ajudar no entendimento. São necessários vários arquivos para
gerar a página com a lista de fornecedores(as) em <code>CD/vendors</code>:</p>

<dl>
  <dt><code>index.wml</code>:</dt>
      <dd>O texto no topo da página de fornecedores(as) está neste arquivo.
      Uma cópia traduzida desse texto deve ser colocada em cada diretório de
      idioma.</dd>
  <dt><code>vendors.CD.def</code>:</dt>
      <dd>Ele contém todas as partes de texto que são necessárias para cada
      entrada de fornecedor(a). Traduções são adicionadas via
      <code>&lt;<var>idioma</var>&gt;/po/vendors.<var>xy</var>.po</code>.</dd>
  <dt><code>vendors.CD</code>:</dt>
      <dd>Este arquivo contém as entradas de fornecedores(as) que são
      independentes do idioma, então um(a) tradutor(a) não precisa mexer neste
      arquivo.</dd>
</dl>

<p>Quando uma das pessoas que está por trás de <email "cdvendors@debian.org">
adiciona um(a) novo(a) fornecedor(a), ele(a) adiciona no
<code>debiancd.db</code>, convertendo-o para o formato WML como
<code>vendors.CD</code> (usando <code>getvendors.pl</code>) e então
deixa que o WML e os makefiles façam suas mágicas. Todas as traduções são
reconstruídas usando o texto traduzido existente, mas com os dados do(a)
novo(a) fornecedor(a)(uma tradução atualizada de graça!).</p>

<toc-add-entry name="newpage">Adicionando uma nova página</toc-add-entry>

<p>Adicionar novas páginas ao Debian é muito fácil. Todo o trabalho de obter
corretamente o cabeçalho e o rodapé são feitos usando WML. Tudo o que você
precisa fazer é incluir uma linha no topo do novo arquivo, como segue:</p>

<pre><protect>
#use wml::debian::template title="TÍTULO DA PÁGINA"
</protect></pre>

<p>seguido do corpo do texto. Todas as páginas devem usar o arquivo de modelo
<code>wml::debian::template</code> a menos que elas estejam usando um modelo
especial criado justamente para aquela seção, por exemplo, item de notícias
ou de segurança.</p>

<p>Os modelos que temos permitem que você defina certas variáveis que afetarão
as páginas criadas. Isto deve evitar a criação de modelos diferentes para cada
situação e permitem que melhorias sejam de mais fácil implementação. As
variáveis atualmente disponíveis e seus propósitos são:</p>

<dl>
<dt>BARETITLE="true"</dt>
	<dd>Remove a parte "Debian --" que geralmente é acrescentada no início
	de todas as tags &lt;title&gt;.</dd>
<dt>NOHEADER="true"</dt>
	<dd>Remove o cabeçalho inicial da página. Um cabeçalho personalizado
	pode, é claro, ser incluído no corpo do texto.</dd>
<dt>NOMIRRORS="true"</dt>
	<dd>Remove a lista suspensa de espelhos da página. Geralmente não se
	recomenda que seja usada, exceto em algumas poucas páginas.</dd>
<dt>NOHOMELINK="true"</dt>
	<dd>Remove o link para a página principal do Debian, que é normalmente
	adicionado no fim da página.</dd>
<dt>NOLANGUAGES="true"</dt>
	<dd>Remove os links para as versões em outros idiomas, que são
	normalmente adicionados no fim da página.</dd>
<dt>GEN_TIME="true"</dt>
	<dd>Define a data dos arquivos resultantes com a data e hora dos
        arquivos gerados, em vez da data e hora do arquivo-fonte.</dd>
<dt>NOCOPYRIGHT="true"</dt>
	<dd>Remove a notificação de copyright no fim da página.</dd>
</dl>

<p>Note que você pode usar qualquer string como valor dessas variáveis,
<q>true</q>, <q>yes</q>, <q>foo</q>, não importa.</p>

<p>Um exemplo de uso está nas páginas de portes, que têm
seus próprios cabeçalhos. A página <code>ports/arm/index.wml</code> usa:</p>

<pre><protect>
#use wml::debian::template title="ARM Port" NOHEADER="yes"
</protect></pre>

<p>Se você quer fazer alguma coisa que não pode ser feita usando os
modelos existentes, primeiro considere estender um deles. Se não é possível
estendê-lo de modo compatível com versões anteriores, tente fazer do
novo modelo um super conjunto de um modelo existente, de modo que as páginas
possam ser convertidas para ele na próxima grande atualização (esperamos que
não nunca seja mais do que 6 meses).</p>

<p>Se você está criando uma página que é gerada por um script ou que
tem pouco texto, considere usar as tags &lt;gettext&gt; para facilitar
a tarefa de manter as traduções atualizadas.</p>

# pense em um bom exemplo para <gettext> em novas páginas

<toc-add-entry name="inclusion">Incluindo outros arquivos</toc-add-entry>

<p>Se você quiser separar algumas partes da sua página em um arquivo distinto
   (que é então incluído pelo seu arquivo principal), use
   a extensão <code>.src</code> se seu arquivo contém conteúdo que deverá
   ser traduzido, porque desse modo seu arquivo incluído será rastreado por
   alterações como qualquer arquivo <code>.wml</code> comum. Se você usar
   qualquer outra extensão, como <code>.inc</code>, os(as) tradutores(as) não
   vão notar suas atualizações e páginas de diferentes idiomas poderão ser
   enviadas com diferentes conteúdos.</p>

<toc-add-entry name="newdir">Adicionando um novo diretório</toc-add-entry>

<p>Nota: <strong>não</strong> crie um diretório com o nome
<code>install</code>. Isto confunde o make e as páginas naquele diretório
não serão atualizadas automaticamente.</p>

<p>Abaixo segue um exemplo anotado de adição de um novo diretório para o site
web.
</p>
<pre>
   mkdir foo
   git add foo
   cd foo
   cp ../intro/Makefile .
   git add Makefile
</pre>

<p>Edite o Makefile no diretório pai e adicione o diretório que você
acabou de criar na variável <code>SUBS</code>. Isto vai adicionar o
diretório na construção quando o make for executado.</p>

<p>Finalmente, faça o commit de todas as alterações realizadas para o
repositório com
</p>
<pre>
  git commit Makefile foo
</pre>
