#use wml::debian::translation-check translation="7dcdd3e690d3084ffd92cb099c4321e35edccfc1"
<define-tag pagetitle>Eigendomsrecht op het domein <q>debian.community</q></define-tag>
<define-tag release_date>2022-08-07</define-tag>
#use wml::debian::news

<p>
De Wereldorganisatie voor Intellectuele Eigendom (World Intellectual Property Organization - WIPO) heeft onder haar uniform beleid voor het oplossen van geschillen over domeinnamen (Uniform Domain-Name Dispute-Resolution Policy - UDRP) beslist dat het eigendomsrecht op het domein <q><a href="https://debian.community">debian.community</ a></q> moet worden <a href="https://www.wipo.int/amc/en/domains/search/case.jsp?case=D2022-1524">overgedragen aan het Debian-project</a>.
</p>

<p>
De aangestelde jury oordeelde dat <q>de betwiste domeinnaam identiek is aan een merk waarop Klager rechten heeft.</q>
</p>

<p>
In zijn besluit merkte de jury op dat:
<p>

<blockquote>
[...] de betwiste domeinnaam identiek is aan het merk DEBIAN, hetgeen een groot risico op impliciete associatie met Klager inhoudt.
[...] aangezien Klager Debian op zijn homepagina duidelijk omschrijft als een 'gemeenschap' en niet alleen als een besturingssysteem, versterkt het achtervoegsel [.community] in feite het vermoeden dat de betwiste domeinnaam zal verwijzen naar een site die wordt beheerd of onderschreven door [Debian]. De betwiste domeinnaam bevat geen cruciale of andere termen om deze onjuiste indruk te ontkrachten of te karakteriseren.
</blockquote>

<p>
De jury merkte verder op dat:
</p>

<blockquote>
uit het door Klager overgelegde bewijsmateriaal blijkt dat in sommige berichten het merk DEBIAN wordt weergegeven samen met informatie over een beruchte sekscultus, beruchte seksuele delinquenten en slavernij van vrouwen, en één bericht foto's toont van fysieke brandmerken die op de genitale huid van de slachtoffers zouden zijn aangebracht.
De overgangen van informatie over Klager naar dit soort informatie zijn gekunsteld en de omvang van deze informatie is niet louter incidenteel op de website. Volgens de jury zijn deze berichten opzettelijk bedoeld om een valse associatie tussen het merk DEBIAN en beledigende verschijnselen te creëren en zo het merk aan te tasten.
</blockquote>

<p>
en concludeerde voorts dat:
</p>

<blockquote>
niets in het sociaal contract van Debian of elders erop wijst dat Klager ooit heeft ingestemd met het soort valse associaties met zijn merk dat door Verweerder op zijn website is gepubliceerd.
Verweerder wijst erop dat het merk DEBIAN alleen is geregistreerd voor software. Hoewel de relevante berichten leden van Klager aanvallen die DEBIAN-software beschikbaar stellen, in plaats van de software zelf, gebruiken deze berichten het merk in combinatie met de betwiste domeinnaam op een manier die opzettelijk valse associaties met het merk zelf probeert te creëren.
</blockquote>

<p>
Debian zet zich in het kader van zijn <a href="$(HOME)/trademark">handelsmerkbeleid</a> in voor het juiste gebruik van zijn handelsmerken en zal handhavend blijven optreden wanneer dat beleid wordt geschonden.
</p>

<p>
De inhoud van <q>debian.community</q> is nu vervangen door een <a href="$(HOME)/legal/debian-community-site">pagina</a> die de situatie uitlegt en verdere vragen beantwoordt.
</p>

<p>
De volledige tekst van het besluit van WIPO is online beschikbaar:
<a href="https://www.wipo.int/amc/en/domains/search/case.jsp?case_id=58273">https://www.wipo.int/amc/en/domains/search/case.jsp?case_id=58273</a>

<h2>Over Debian</h2>

<p>
Het Debian-project is een vereniging van ontwikkelaars van vrije software
die vrijwillig tijd en moeite steken in het produceren van het volledig vrije
besturingssysteem Debian.
</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a> of stuur een e-mail naar
&lt;press@debian.org&gt;.</p>
