#use wml::debian::cdimage title="Live installatie-images"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="18c4b33fe6dcee4c5cb83603609a9b4bd481da0c"

<p>Een <q>live installatie</q>-image bevat een Debian-systeem dat kan
opstarten zonder een bestand op de harde schijf te wijzigen. Ook maakt de
inhoud van het image de installatie van Debian mogelijk.
</p>

<p><a name="choose_live"><strong>Is een live-image geschikt voor mij?</strong></a>
Hierna volgen een aantal in aanmerking te nemen zaken die u kunnen helpen een beslissing te nemen.
<ul>
<li><b>Varianten:</b> De live-images bestaan in verschillende "varianten"
die een keuze bieden uit grafische werkomgevingen (GNOME, KDE, LXDE, Xfce,
Cinnamon en MATE). Veel gebruikers zullen de initiële selectie pakketten
geschikt vinden en nadien eventuele bijkomende pakketten welke ze nodig hebben,
via het netwerk installeren.
<li><b>Architectuur:</b> Momenteel worden enkel images voor de
populairste architectuur, 64-bits pc (amd64),
aangeboden.
<li><b>Installatieprogramma:</b> Vanaf Debian 10 Buster bevatten de live-images
het gebruikersvriendelijke <a href="https://calamares.io">Calamares
installatieprogramma</a>, een distributie-onafhankelijk installatieprogramma,
als een alternatief voor ons welbekend
<a href="$(HOME)/devel/debian-installer">Debian-installatiesysteem</a>.
<li><b>Grootte:</b> Elk image is veel kleiner dan de volledige collectie op
dvd-images, maar groter dan het netwerkinstallatiemedium.
<li><b>Taal:</b> De images bevatten niet de volledige verzameling pakketten
voor taalondersteuning. Als u invoermethoden, lettertypes of bijkomende
pakketten nodig heeft voor uw taal, moet u deze nadien installeren.
</ul>

<p>U kunt de volgende live installatie-images downloaden:</p>

<ul>

  <li>Officiële <q>live installatie</q>-images voor de <q>stabiele</q> release &mdash; <a
  href="#live-install-stable">zie hieronder</a></li>

</ul>


<h2 id="live-install-stable">Officiële live installatie-images voor de <q>stabiele</q> release</h2>

<p>Aangeboden in verschillende varianten, die, zoals hierboven aangegeven,
onderling in grootte verschillen. Deze images zijn geschikt om een
Debian-systeem met een geselecteerde collectie standaardpakketten uit te
proberen en ze met behulp van diezelfde media te installeren.</p>

<div class="line">
<div class="item col50">
<p><strong>dvd/USB (via <a
href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<p><q>Hybride</q> ISO imagebestanden die op een dvd-r(w) gebrand kunnen
worden en ook naar een voldoende grote USB-stick gekopieerd kunnen worden.
Indien u dat kunt, gebruik dan BitTorrent, omdat dit de belasting van
onze servers beperkt.</p>
	  <stable-live-install-bt-cd-images />
</div>

<div class="item col50 lastcol"> <p><strong>dvd/USB</strong></p>
<p><q>Hybride</q> ISO imagebestanden die op een dvd-r(w) gebrand kunnen
worden en ook naar een voldoende grote USB-stick gekopieerd kunnen worden.</p>
       <stable-live-install-iso-cd-images />
</div> </div>

<p>Raadpleeg de <a href="../faq/">FAQ</a> voor informatie over wat deze
bestanden zijn en hoe u ze kunt gebruiken.</p>

<p>Indien u van plan bent Debian te installeren met het gedownloade live-image,
moet u zeker de <a href="$(HOME)/releases/stable/installmanual">uitgebreide
informatie over het installatieproces</a> bekijken.</p>

<p>Raadpleeg <a href="$(HOME)/devel/debian-live"> de projectpagina van Debian Live</a> voor
extra informatie over de Debian Live-systemen die op deze images aangeboden worden.</p>

