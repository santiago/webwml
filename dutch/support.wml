#use wml::debian::template title="Gebruikersondersteuning" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="c7a64739b495b3bd084b972d820ef78fc30a3f8a"

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">


<ul class="toc">
  <li><a href="#irc">IRC (directe ondersteuning)</a></li>
  <li><a href="#mail_lists">Mailinglijsten</a></li>
  <li><a href="#usenet">Usenet-nieuwsgroepen</a></li>
  <li><a href="#forums">Debian gebruikersfora</a></li>
  <li><a href="#maintainers">Contact opnemen met pakketbeheerders</a></li>
  <li><a href="#bts">Bugvolgsysteem</a></li>
  <li><a href="#release">Bekende problemen</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Ondersteuning voor Debian wordt geboden door een groep vrijwilligers. Als deze door de gemeenschap gedragen ondersteuning niet aan uw behoeften beantwoordt en u geen antwoord kunt vinden in onze <a href="doc/">documentatie</a>, kunt u een <a href="consultants/">consultant</a> inhuren om uw vragen te beantwoorden of om uw Debian-systeem te onderhouden of er extra functionaliteit aan toe te voegen.</p>
</aside>

<h2><a id="irc">IRC (directe ondersteuning)</a></h2>

<p>
<a href="http://www.irchelp.org/">IRC</a> (Internet Relay Chat) is een geweldige manier om rechtstreeks te chatten met mensen van over de hele wereld. Het is een op tekst gebaseerd chatsysteem voor instant messaging. Op IRC kunt u chatrooms (zogenaamde kanalen) binnengaan of u kunt rechtstreeks chatten met individuele personen via privéberichten.
</p>

<p>
Aan Debian gewijde IRC-kanalen zijn te vinden op <a href="https://www.oftc.net/">OFTC</a>. Voor een volledige lijst van Debian-kanalen verwijzen wij u naar onze <a href="https://wiki.debian.org/IRC">Wiki</a>. U kunt ook een <a href="https://netsplit.de/channels/index.en.php?net=oftc&chat=debian">zoekmachine</a> gebruiken om te zoeken naar Debian-gerelateerde kanalen.
</p>

<h3>IRC-clients</h3>

<p>
Om verbinding te maken met het IRC-netwerk, kunt u OFTC's <a href="https://www.oftc.net/WebChat/">WebChat</a> in uw favoriete webbrowser gebruiken of een client op uw computer installeren. Er zijn veel verschillende clients, sommige met een grafische interface, sommige voor de console. Sommige populaire IRC-clients zijn verpakt voor Debian, bijvoorbeeld:
</p>

<ul>
  <li><a href="https://packages.debian.org/stable/net/irssi">irssi</a> (tekstmodus)</li>
  <li><a href="https://packages.debian.org/stable/net/weechat-curses">WeeChat</a> (tekstmodus)</li>
  <li><a href="https://packages.debian.org/stable/net/hexchat">HexChat (GTK)</a></li>
  <li><a href="https://packages.debian.org/stable/net/konversation">Konversation</a> (KDE)</li>
</ul>

<p>
De Debian Wiki biedt een meer uitgebreide <a href="https://wiki.debian.org/IrcClients">lijst van IRC-clients</a> die beschikbaar zijn als Debian-pakket.
</p>

<h3>Verbinding maken met het netwerk</h3>

<p>
Nadat u de client hebt geïnstalleerd, moet u hem opdragen om verbinding te maken met de server. In de meeste clients kunt u dat doen door het volgende te typen:
</p>

<pre>
/server irc.debian.org
</pre>

<p>De computernaam irc.debian.org is een alias voor irc.oftc.net. Bij sommige clients (zoals irssi) zult u in de plaats daarvan echter het volgende moeten typen:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<h3>Een kanaal binnengaan</h3>

<p>Eens u verbonden bent, kunt u voor ondersteuning in het Engels het kanaal <code>#debian</code> binnengaan door het volgende commando te typen:</p>

<pre>
/join #debian
</pre>

<p>Voor ondersteuning in het Nederlands kunt u het kanaal <code>#debian-nl</code> binnengaan door het volgende te typen:</p>

<pre>
/join #debian-nl
</pre>

<p>Noot: grafische clients zoals HexChat of Konversation hebben vaak een knop of een menu-item om verbinding te maken met servers en kanalen binnen te gaan.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://wiki.debian.org/DebianIRC">Onze IRC-FAQ lezen</a></button></p>

<h2><a id="mail_lists">Mailinglijsten</a></h2>

<p>
Meer dan duizend actieve <a href="intro/people.en.html#devcont">ontwikkelaars</a> verspreid over de hele wereld werken aan Debian in hun vrije tijd — en in hun eigen tijdzone. Daarom communiceren wij voornamelijk via e-mail. Op dezelfde manier vinden de meeste conversaties tussen Debian-ontwikkelaars en gebruikers plaats op verschillende <a href="MailingLists/">mailinglijsten</a>:
</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.

<ul>
  <li>Voor gebruikersondersteuning in het Engels moet u contact opnemen met de mailinglijst <a href="https://lists.debian.org/debian-user/">debian-user</a>.</li>
  <li>Voor gebruikersondersteuning in het Nederlands neemt u best contact op
met de mailinglijst <a href="https://lists.debian.org/debian-user-dutch/">debian-user-dutch</a>.</li>
  <li>Voor gebruikersondersteuning in andere talen kunt u de <a href="https://lists.debian.org/users.html">index</a> van andere gebruikersmailinglijsten raadplegen.</li>
</ul>

<p>
U kunt onze <a href="https://lists.debian.org/">mailinglijstarchieven</a> doorbladeren of <a href="https://lists.debian.org/search.html">zoeken</a> in de archieven zonder dat u er moet op intekenen.
</p>

<p>
Er zijn uiteraard vele andere mailinglijsten, gewijd aan een bepaald aspect van het Linux ecosysteem, die niet specifiek op Debian zijn gericht. Gebruik uw favoriete zoekmachine om de lijst te vinden die best aansluit bij uw behoefte.
</p>

<h2><a id="usenet">Usenet-nieuwsgroepen</a></h2>

<p>
Een groot aantal van onze <a href="#mail_lists">mailinglijsten</a>
kan in de vorm van nieuwsgroepen worden gelezen, in de hiërarchie
<kbd>linux.debian.*</kbd>.
</p>

<h2><a id="forums">Debian gebruikersfora</a></h2>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="https://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p>
<a href="https://forums.debian.net">Debian User Forums</a> is een webportaal waarop duizenden andere gebruikers met Debian verband houdende zaken bespreken, vragen stellen en elkaar helpen door ze te beantwoorden. U kunt alle fora lezen zonder dat u zich hoeft te registreren. Als u wilt deelnemen aan de discussie en uw eigen berichten wilt publiceren, moet u zich registreren en inloggen.
</p>


<h2><a id="maintainers">Pakketbeheerders contacteren</a></h2>

<p>
In principe zijn er twee gebruikelijke manieren om in contact te komen met een beheerder van een Debian-pakket:
</p>

<ul>
  <li>Als u een bug wilt melden, dient u eenvoudig een <a href="Bugs/Reporting">bugrapport</a> in; de beheerder ontvangt automatisch een kopie van uw bugrapport.</li>
  <li>Als u gewoon een e-mail wilt sturen naar de beheerder, gebruik dan de speciale mailaliassen die voor elk pakket zijn ingesteld:<br>
      &lt;<em>pakketnaam</em>&gt;@packages.debian.org</li>
</ul>


<h2><a id="bts">Bugvolgsysteem</a></h2>


<p>
De Debian-distributie heeft zijn eigen <a href="Bugs/">bugvolger</a> met bugs die gerapporteerd werden door gebruikers en ontwikkelaars. Elke bug heeft een uniek nummer en blijft actief totdat deze als opgelost wordt gemarkeerd. Er zijn twee verschillende manieren om een bug te melden:
</p>

<ul>
  <li>De aanbevolen manier is om het Debian pakket <em>reportbug</em> te gebruiken.</li>
  <li>U kunt ook een e-mail sturen zoals beschreven wordt op deze <a href="Bugs/Reporting">pagina</a>.</li>
</ul>

<h2><a id="release">Bekende problemen</a></h2>

<p>Beperkingen en ernstige problemen van de huidige stabiele distributie (als die er al zijn) worden beschreven op <a href="releases/stable/">de releasepagina</a>.</p>

<p>Let vooral op de <a href="releases/stable/releasenotes">notities bij
de release</a> en de <a href="releases/stable/errata">errata</a>.</p>

