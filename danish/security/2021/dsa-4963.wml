#use wml::debian::translation-check translation="6485968c0f180fd4720e21f7f0bdc48b3c3bf4f0" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Adskillige sårbarheder er opdaget i OpenSSL, et Secure Sockets 
Layer-værktøjssæt.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3711">CVE-2021-3711</a>

    <p>John Ouyang rapporterede om en bufferoverløbssårbarhed i 
    SM2-dekryptering.  En angriber i stand til at præsentere SM2-indhold til 
    dekryptering til en applikation, kunne drage nytte af fejlen to at ændre 
    applikationsvirkemåde eller forårsage at applikationen gik ned 
    (lammelsesangreb).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3712">CVE-2021-3712</a>

    <p>Ingo Schwarze rapporterede en bufferoverløbsfejl ved behandling af 
    ASN.1-strenge i funktionen X509_aux_print(), hvilket kunne medføre 
    lammelsesangreb.</p></li>

</ul>

<p>Yderligere oplysninger findes i opstrøms bulletin:
<a href="https://www.openssl.org/news/secadv/20210824.txt">\
https://www.openssl.org/news/secadv/20210824.txt</a></p>

<p>I den gamle stabile distribution (buster), er disse problemer rettet
i version 1.1.1d-0+deb10u7.</p>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 1.1.1k-1+deb11u1.</p>

<p>Vi anbefaler at du opgraderer dine openssl-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende openssl, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4963.data"
