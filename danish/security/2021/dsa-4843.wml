#use wml::debian::translation-check translation="7d4e416de9b0d5870d3b56d250bdbed4f5cdde8b" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationslækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27815">CVE-2020-27815</a>

    <p>En fejl blev rapporteret i JFS-filsystemskoden, som tillod at en lokal 
    angriber med mulighed for at opsætte udvidede attributter, kunne forårsage 
    et lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27825">CVE-2020-27825</a>

    <p>Adam <q>pi3</q> Zabrocki rapporterede om en fejl i forbindelse med 
    anvendelse efter frigivelse i logikken til ændring af størrelsen på ftraces 
    ringbuffer, på grund af en kapløbstilstand, hvilken kunne medføre 
    lammelsesangreb eller informationslækage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27830">CVE-2020-27830</a>

    <p>Shisong Qin rapporterede om en NULL-pointerdereferencefejl i 
    skærmlæsercoredriveren Speakup.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28374">CVE-2020-28374</a>

    <p>David Disseldorp opdagede at LIO SCSI-målimplementeringen udførte 
    utilstrækkelig kontrol af visse XCOPY-forespørgsler.  En angriber med 
    adgang til en LUN samt viden om Unit Serial Number-tildelinger, kunne drage 
    nytte af fejlen til at læse og skrive til enhver LIO-backstore, uafhængigt 
    af SCSI-transportindstillingerne.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29568">CVE-2020-29568 (XSA-349)</a>

    <p>Michael Kurth og Pawel Wieczorkiewicz rapporterede at frontends kunne 
    udløse en OOM i backends, ved at opdatere en overvåget sti.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29569">CVE-2020-29569 (XSA-350)</a>

    <p>Olivier Benjamin og Pawel Wieczorkiewicz rapporterede om en fejl i 
    forbindelse med anvendelse efter frigivelse, hvilken kunne udløses af en 
    blockfrontend i Linux' blkback.  En gæst der ikke opfører sig korrekt, kunne 
    udløse et dom0-nedbrud ved uafbrudt at tilslutte og fjerne en 
    blockfrontend.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29660">CVE-2020-29660</a>

    <p>Jann Horn rapporterede om et inkonsistent låsningsproblem i 
    tty-undersystemet, hvilket kunne gøre det muligt for en lokal angriber at 
    iværksætte et læsning efter frigivelse-angreb mod TIOCGSID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29661">CVE-2020-29661</a>

    <p>Jann Horn rapporterede om et låsningsproblem i tty-undersystemet, hvilket 
    kunne medføre en anvendelse efter frigivelse.  En lokal angriber kunne drage 
    nytte af fejlen til hukommelseskorruption eller rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36158">CVE-2020-36158</a>

    <p>En bufferoverløbsfejl blev opdaget i mwifiex-WiFi-driveren, hvilken kunne 
    medføre lammelsesangreb eller udførelse af vilkårlig kode gennem en lang 
    SSID-værdi.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3347">CVE-2021-3347</a>

    <p>Man opdagede at PI-futexe'er havde anvendelse efter frigivelse i 
    kernestakken under fejlhåndtering.  En upriviligeret bruger kunne udnytte 
    fejlen til at få kerne til at gå ned (medførende lammelsesangreb) eller til 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20177">CVE-2021-20177</a>

    <p>En fejl blev opdaget i Linux-implementeringen af stringmatching i en 
    pakke.  En priviligeret bruger (med root eller CAP_NET_ADMIN) kunne drage 
    nytte af fejlen til at forårsage en kernepanik, når der blev indsat 
    iptables-regler.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 4.19.171-2.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4843.data"
