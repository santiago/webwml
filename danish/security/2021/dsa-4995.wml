#use wml::debian::translation-check translation="75b99797a7e35c030fc904a603d0d4186d4c5ac4" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Følgende sårbarheder er opdaget i webmotoren webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30846">CVE-2021-30846</a>

    <p>Sergei Glazunov opdagede at behandling af ondsindet fremstillet 
    webindhold kunne føre udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30851">CVE-2021-30851</a>

    <p>Samuel Gross opdagde at behandling af ondsindet fremstillet webindhold, 
    kunne føre til udførelse af kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42762">CVE-2021-42762</a>

    <p>En anonym rapportør opdagede en begrænset omgåelse af 
    Bubblewrap-sandkassen, hvilket gjorde det muligt for en proces i en 
    sandkasse, at narre værtsprocesser til at tro, at sandkasseprocessen i er 
    indespærret.</p></li>

</ul>

<p>I den gamle stabile distribution (buster), er disse problemer rettet
i version 2.34.1-1~deb10u1.</p>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 2.34.1-1~deb11u1.</p>

<p>Vi anbefaler at du opgraderer dine webkit2gtk-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende webkit2gtk, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4995.data"
