#use wml::debian::translation-check translation="2592e40c5d7143a6f575ff96f6127ba4fb3f18d5" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er fundet i HTTPD-serveren Apache.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1927">CVE-2020-1927</a>

    <p>Fabrice Perez rapporterede at visse mod_rewrite-opsætninger var sårbare 
    over for en åben viderestilling.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1934">CVE-2020-1934</a>

    <p>Chamal De Silva opdagede at modulet mod_proxy_ftp anvendte uinitialiseret 
    hukommelse, når det var en proxy for en ondsindet FTP-backend.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9490">CVE-2020-9490</a>

    <p>Felix Wilhelm opdagede at en særligt fremstillet værdi for headeren 
    <q>Cache-Digest</q> i en HTTP/2-forespørgsel, kunne forårsage et nedbrud når 
    serveren efterfølgende faktisk prøvede at HTTP/2-PUSH'e en 
    ressource.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11984">CVE-2020-11984</a>

    <p>Felix Wilhelm rapporterede om en bufferoverløbsfejl i modulet 
    mod_proxy_uwsgi, hvilken kunne medføre informationsafsløring eller 
    potentielt fjernudførelse af kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11993">CVE-2020-11993</a>

    <p>Felix Wilhelm rapporterede at når trace/debug var aktiveret for 
    HTTP/2-modulet, kunne visse traffic edge-mønstre forårsage 
    logningsstatements i den forkerte forbindelse, medførende samtidig 
    anvendelse af hukommelsespools.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 2.4.38-3+deb10u4.</p>

<p>Vi anbefaler at du opgraderer dine apache2-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende apache2, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/apache2">\
https://security-tracker.debian.org/tracker/apache2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4757.data"
