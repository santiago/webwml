#use wml::debian::translation-check translation="d65a53d18834a941829aaf85abcf1935333ebf52" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationslækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4197">CVE-2021-4197</a>

    <p>Eric Biederman rapporterede at ukorrekte rettighedskontroller i 
    implementeringen af cgroup-procesmigreringen, kunne gøre det muligt for en 
    lokal angriber at forsøge rettigheder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0168">CVE-2022-0168</a>

    <p>En NULL-pointerdereferencefejl blev fundet i implementeringen af 
    CIFS-klienten, kunne gøre det muligt for en lokal angriber med 
    CAP_SYS_ADMIN-rettigheder for at få systemet til at gå ned.  
    Sikkerhedspåvirkningen er ubetydelig, da CAP_SYS_ADMIN i sagens natur giver 
    mulighed for at afvise service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1016">CVE-2022-1016</a>

    <p>David Bouman opdagede en fejl i undersystemet netfilter, hvor funktionen 
    nft_do_chain ikke initialiserede registerdata, som nf_tables-udtryk kunne 
    læse fra og skrive til.  En lokal angriber kunne drage nytte af dette til at 
    læse følsomme oplysninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1048">CVE-2022-1048</a>

    <p>Hu Jiahui opdagede en kapløbstilstand i undersystemet sound, som kunne 
    medføre en anvendelse efter frigivelse.  En lokal bruger, med rettigheder 
    til at tilgå en PCM-lydenhed, kunne drage nytte af fejlen til at få systemet 
    til at gå ned eller potentielt til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1158">CVE-2022-1158</a>

    <p>Qiuhao Li, Gaoning Pan og Yongkang Jia opdagede en fejl i 
    KVM-implementeringen til x86-processorer.  En lokal bruger med adgang til 
    /dev/kvm, kunne få MMU-emulatoren til at opdatere sidetabelforekomstflag på 
    den forkerte adresse.  Det kunne udnyttes til at forårsage et 
    lammelsesangreb (hukommelseskorruption eller nedbrud) eller muligvis til 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1195">CVE-2022-1195</a>

    <p>Lin Ma opdagede kapløbstilstande i amatørradiodriverne 6pack og mkiss, 
    hvilke kunne føre til en anvendelse efter frigivelse.  En lokal bruger kunne 
    udnytte disse til at forårsage et lammelsesangreb (hukommelseskorruption 
    eller nedbrud) eller muligvis til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1198">CVE-2022-1198</a>

    <p>Duoming Zhou opdagede en kapløbstilstand i amatørradiodriveren 6pack, 
    hvilke kunne føre til anvendelse efter frigivelse.  En lokal bruger kunne 
    udnytte dette til at forårsage et lammelsesangreb (hukommelseskorruption 
    eller nedbrud) eller muligvis til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1199">CVE-2022-1199</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-1204">CVE-2022-1204</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-1205">CVE-2022-1205</a>

    <p>Duoming Zhou opdagede kapløbsstande i amatørradioprotokollen AX.25, 
    hvilke kunne føre til anvendelse efter frigivelse eller 
    NULL-pointerdereference.  En lokal bruger kunne udnytte dette til at 
    forårsage et lammelsesangreb (hukommelseskorruption eller nedbrud) eller 
    muligvis til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1353">CVE-2022-1353</a>

    <p>Værktøjet TCS Robot, fandt en informationslækage i undersystemet PF_KEY. 
    En lokal bruger kunne modtage en netlink-meddelelse, når en IPsec-dæmon 
    registrerer sig hos kernen, og dette kunne indeholde følsomme 
    oplysninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1516">CVE-2022-1516</a>

    <p>En NULL-pointerdereferencefejl i implementeringen af X.25-sættet af 
    standardiserede netværksprotokoller, hvilken kunne medføre 
    lammelsesangreb.</p>

    <p>Denne driver er ikke aktiveret i Debians officielle 
    kerneopsætninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26490">CVE-2022-26490</a>

    <p>Bufferoverløb i STMicroelectronics coredriver ST21NFCA, kunne medføre 
    lammelsesangreb eller rettighedsforøgelse.</p>

    <p>Denne driver er ikke aktiveret i Debians officielle 
    kerneopsætninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-27666">CVE-2022-27666</a>

    <p><q>valis</q> rapporterede om et muligt bufferoverløb i IPsecs 
    ESP-transformationskode.  En lokal bruger kunne drage nytte af fejlen til at 
    forårsage et lammelsesangreb eller til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28356">CVE-2022-28356</a>

    <p>Beraphin opdagede at ANSI/IEEE 802.2 LLC type 2-driveren ikke på korrekt 
    vis udføre referenceoptælling i nogle fejlstier.  En lokal angriber kunne 
    drage nytte af fejlen til at forårsage et lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28388">CVE-2022-28388</a>

    <p>En dobbelt frigivelse-sårbarhed blev opdaget i 8 devices' 
    USB2CAN-græsefladedriver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28389">CVE-2022-28389</a>

    <p>En dobbelt frigivelse-sårbarhed blev opdaget i Microchip CAN BUS 
    Analyzers grænsefladedriver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28390">CVE-2022-28390</a>

    <p>En dobbelt frigivelse-sårbarhed blev opdaget i EMS CPC-USB/ARM7 CAN/USB's 
    græsefladedriver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29582">CVE-2022-29582</a>

    <p>Jayden Rivers og David Bouman opdagede en sårbarhed i forbindelse med 
    anvendelse efter frigivelse i undersystemet io_uring, på grund af en 
    kapløbstilstand i io_uring-timeouts.  En lokal upriviligeret bruger kunne 
    drage nytte af fejlen til rettighedsforøgelse.</p></li>

</ul>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 5.10.113-1.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5127.data"
