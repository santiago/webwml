# translation of doc.po to Arabic
# Ossama M. Khayat <okhayat@yahoo.com>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-05-25 11:00+0000\n"
"Last-Translator: Med <medeb@protonmail.com>\n"
"Language-Team: Arabic <debian-l10n-arabic@lists.debian.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/doc/books.data:35
msgid ""
"\n"
"  Debian 9 is the must-have handbook for learning Linux. Start on the\n"
"  beginners level and learn how to deploy the system with graphical\n"
"  interface and terminal.\n"
"  This book provides the basic knowledge to grow and become a 'junior'\n"
"  systems administrator. Start off with exploring the GNOME desktop\n"
"  interface and adjust it to your personal needs. Overcome your fear of\n"
"  using the Linux terminal and learn the most essential commands in\n"
"  administering Debian. Expand your knowledge of system services (systemd)\n"
"  and learn how to adapt them. Get more out of the software in Debian and\n"
"  outside of Debian. Manage your home-network with network-manager, etc.\n"
"  10 percent of the profits on this book will be donated to the Debian\n"
"  Project."
msgstr ""

#: ../../english/doc/books.data:64 ../../english/doc/books.data:174
#: ../../english/doc/books.data:229
msgid ""
"Written by two Debian developers, this free book\n"
"  started as a translation of their French best-seller known as Cahier de\n"
"  l'admin Debian (published by Eyrolles). Accessible to all, this book\n"
"  teaches the essentials to anyone who wants to become an effective and\n"
"  independent Debian GNU/Linux administrator.\n"
"  It covers all the topics that a competent Linux administrator should\n"
"  master, from the installation and the update of the system, up to the\n"
"  creation of packages and the compilation of the kernel, but also\n"
"  monitoring, backup and migration, without forgetting advanced topics\n"
"  like SELinux setup to secure services, automated installations, or\n"
"  virtualization with Xen, KVM or LXC."
msgstr ""

#: ../../english/doc/books.data:86
msgid ""
"The aim of this freely available book is to get you up to\n"
"  speed with Debian. It is comprehensive with basic support\n"
"  for the user who installs and maintains the system themselves (whether\n"
"  in the home, office, club, or school). Some Debian specific information "
"are very old.\n"
"  "
msgstr ""

#: ../../english/doc/books.data:108
msgid ""
"The first French book about Debian is already in its fifth edition. It\n"
"  covers all aspects of the administration of Debian from the installation\n"
"  to the configuration of network services.\n"
"  Written by two Debian developers, this book can be of interest to many\n"
"  people: the beginner wishing to discover Debian, the advanced user "
"looking\n"
"  for tips to enhance his mastership of the Debian tools and the\n"
"  administrator who wants to build a reliable network with Debian."
msgstr ""

#: ../../english/doc/books.data:128
msgid ""
"The book covers topics ranging from concepts of package\n"
"  management over the available tools and how they're used to concrete "
"problems\n"
"  which may occur in real life and how they can be solved. The book is "
"written\n"
"  in German, an English translation is planned. Format: e-book (Online, "
"HTML,\n"
"  PDF, ePub, Mobi), printed book planned.\n"
msgstr ""

#: ../../english/doc/books.data:149
msgid ""
"This book teaches you how to install and configure the system and also how "
"to use Debian in a professional environment.  It shows the full potential of "
"the distribution (in its current version 8) and provides a practical manual "
"for all users who want to learn more about Debian and its range of services."
msgstr ""

#: ../../english/doc/books.data:203
msgid ""
"Written by two penetration researcher - Annihilator, Firstblood.\n"
"  This book teaches you how to build and configure Debian 8.x server system\n"
"  security hardening using Kali Linux and Debian simultaneously.\n"
"  DNS, FTP, SAMBA, DHCP, Apache2 webserver, etc.\n"
"  From the perspective that 'the origin of Kali Linux is Debian', it "
"explains\n"
"  how to enhance Debian's security by applying the method of penetration\n"
"  testing.\n"
"  This book covers various security issues like SSL cerificates, UFW "
"firewall,\n"
"  MySQL Vulnerability, commercial Symantec antivirus, including Snort\n"
"  intrusion detection system."
msgstr ""

#: ../../english/doc/books.def:38
msgid "Author:"
msgstr "المؤلف :"

#: ../../english/doc/books.def:41
msgid "Debian Release:"
msgstr "إصدار دبيان:"

#: ../../english/doc/books.def:44
msgid "email:"
msgstr "البريد الإلكتروني:"

#: ../../english/doc/books.def:48
msgid "Available at:"
msgstr "متوفر على:"

#: ../../english/doc/books.def:51
msgid "CD Included:"
msgstr "متضمن قرص مدمج:"

#: ../../english/doc/books.def:54
msgid "Publisher:"
msgstr "الناشر:"

#: ../../english/doc/manuals.defs:28
msgid "Authors:"
msgstr "المؤلفون:"

#: ../../english/doc/manuals.defs:35
msgid "Editors:"
msgstr "المحررين:"

#: ../../english/doc/manuals.defs:42
msgid "Maintainer:"
msgstr "المصين:"

#: ../../english/doc/manuals.defs:49
msgid "Status:"
msgstr "الحالة:"

#: ../../english/doc/manuals.defs:56
msgid "Availability:"
msgstr "التوفر:"

#: ../../english/doc/manuals.defs:85
msgid "Latest version:"
msgstr "أحدث نسخة:"

#: ../../english/doc/manuals.defs:101
msgid "(version <get-var version />)"
msgstr "(الإصدار <get-var version />)"

#: ../../english/doc/manuals.defs:137 ../../english/releases/arches.data:39
msgid "plain text"
msgstr "نص صِرف"

#: ../../english/doc/manuals.defs:140
msgid "HTML (single page)"
msgstr ""

#: ../../english/doc/manuals.defs:156 ../../english/doc/manuals.defs:166
#: ../../english/doc/manuals.defs:174
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/git\">Git</a> repository."
msgstr ""

#: ../../english/doc/manuals.defs:158 ../../english/doc/manuals.defs:168
#: ../../english/doc/manuals.defs:176
msgid "Web interface: "
msgstr "واجهة الوِب:"

#: ../../english/doc/manuals.defs:159 ../../english/doc/manuals.defs:169
#: ../../english/doc/manuals.defs:177
msgid "VCS interface: "
msgstr "واجهة VCS:"

#: ../../english/doc/manuals.defs:184 ../../english/doc/manuals.defs:188
msgid "Debian package"
msgstr "حزمة دبيان"

#: ../../english/doc/manuals.defs:193 ../../english/doc/manuals.defs:197
msgid "Debian package (archived)"
msgstr "حزمة دبيان (مؤرشفة)"

#: ../../english/releases/arches.data:37
msgid "HTML"
msgstr "HTML"

#: ../../english/releases/arches.data:38
msgid "PDF"
msgstr "PDF"

#~ msgid ""
#~ "CVS sources working copy: set <code>CVSROOT</code>\n"
#~ "  to <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>,\n"
#~ "  and check out the <kbd>boot-floppies/documentation</kbd> module."
#~ msgstr ""
#~ "نسخة مصادر CVS للعمل: يجب تعيين <code>CVSROOT</code>\n"
#~ "  القيمة <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</"
#~ "kbd>,\n"
#~ "  ومراجعة وحدة <kbd>أقراص الإقلاع المرنة/التوثيق</kbd>."

#~ msgid "CVS via web"
#~ msgstr "CVS عبر الوِب"

#~ msgid ""
#~ "Use <a href=\"cvs\">SVN</a> to download the SGML source text for <get-var "
#~ "ddp_pkg_loc />."
#~ msgstr ""
#~ "استخدم <a href=\"cvs\">CVS</a> لتنزيل مصدر نص الـ SGML لـ <get-var "
#~ "ddp_pkg_loc />."
