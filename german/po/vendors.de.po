# German translation of the Debian webwml modules
# Copyright (c) 2004 Software in the Public Interest, Inc.
# Florian Ernst <florian@uni-hd.de>, 2004.
# Dr. Tobias Quathamer <toddy@debian.org>, 2006, 2007, 2016.
# Helge Kreutzmann <debian@helgefjell.de>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml vendors\n"
"PO-Revision-Date: 2016-01-14 17:05+0100\n"
"Last-Translator: Dr. Tobias Quathamer <toddy@debian.org>\n"
"Language-Team: Debian l10n German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.4\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Händler"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Erlaubt Spenden"

#: ../../english/CD/vendors/vendors.CD.def:16
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD/BD/USB"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Architekturen"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Versand ins Ausland"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Kontakt"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Name des Händlers"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "Seite"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "E-Mail"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "innerhalb Europas"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "in bestimmten Gebieten"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "Quellcode"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "und"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Erlaubt eine Spende an Debian:"

#~ msgid "Architectures:"
#~ msgstr "Architekturen:"

#, fuzzy
#~| msgid "CD Type:"
#~ msgid "BD Type:"
#~ msgstr "Art der CD:"

#~ msgid "CD Type:"
#~ msgstr "Art der CD:"

#~ msgid "Country:"
#~ msgstr "Land:"

#~ msgid "DVD Type:"
#~ msgstr "DVD-Typ:"

#~ msgid "Ship International:"
#~ msgstr "Versand ins Ausland:"

#~ msgid "URL for Debian Page:"
#~ msgstr "URL der Debian-Seite:"

#, fuzzy
#~| msgid "CD Type:"
#~ msgid "USB Type:"
#~ msgstr "Art der CD:"

#~ msgid "Vendor:"
#~ msgstr "Distributor:"

#~ msgid "email:"
#~ msgstr "E-Mail:"
