#use wml::debian::translation-check translation="99e7b2c2dbf5350e343013f1a92208e1d6e0084b" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Déclaration à propos de Daniel Pocock</define-tag>
<define-tag release_date>2021-11-17</define-tag>
#use wml::debian::news
# $Id$
# $Rev$

<p>Debian a connaissance d'un certain nombre de messages publics concernant
Debian et de membres de sa communauté dans une série de sites internet par
M. Daniel Pocock qui prétend être un développeur Debian.</p>


<p>M. Pocock n'est pas lié à Debian. Il n'est ni développeur Debian, ni un
membre de la communauté Debian. Il a été auparavant un développeur Debian,
mais a été exclu du projet il y a quelques années pour avoir adopté un
comportement nuisible pour la réputation de Debian et pour la communauté
elle-même. Il n'est plus membre du projet Debian depuis 2018. Il lui est aussi
interdit de participer à la communauté sous quelque forme que ce soit, y
compris par des contributions techniques, la participation à des espaces en
ligne ou la participation à des conférences ou des événements. Il n'a aucun
droit ou statut pour représenter Debian en quelque qualité que ce soit, ou
pour se présenter lui-même comme développeur Debian ou membre de la communauté
Debian.</p>

<p>Depuis qu'il a été exclu du projet, M. Pocock s'est engagé dans une
campagne permanente et généralisée de harcèlement en représailles en envoyant
de nombreux messages en ligne incendiaires et diffamatoires, en particulier sur
un site internet qui prétend être un site Debian. Le contenu de ces messages
implique non seulement Debian, mais aussi un certain nombre de ses développeurs
et bénévoles. Il a aussi continué à se présenter frauduleusement comme un
membre de la communauté Debian dans beaucoup de ses communications et
présentations publiques.

Veuillez consulter <a href="https://bits.debian.org/2020/03/official-communication-channels.html">cet article</a>
pour prendre connaissance de la liste des canaux de communication officiels de
Debian.

Une action légale est envisagée pour, entre autres choses, diffamation,
mensonges malveillants et harcèlement.</p>

<p>Debian est unie en tant que communauté et contre le harcèlement. Nous avons
un code de conduite qui nous guide pour répondre aux comportements nuisibles
dans notre communauté, et nous continuerons à agir pour protéger notre
communauté et nos bénévoles. N'hésitez pas à contacter l'équipe en charge de la
communauté si vous avez des soucis ou si vous avez besoin de soutien. En
attendant, tous les droits de Debian et de ses bénévoles sont réservés.</p>

<h2>À propos de Debian</h2>

<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt;.
