#use wml::debian::translation-check translation="8167b93a196b74047bf587b084389409c062778d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans Curl, une bibliothèque côté
client de transfert par URL d'utilisation facile, qui pouvaient avoir pour
conséquences un déni de service ou la divulgation d'informations.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 7.74.0-1.3+deb11u5. Cette mise à jour révise la correction
du <a href="https://security-tracker.debian.org/tracker/CVE-2022-27774">\
CVE-2022-27774</a> publié dans la DSA-5197-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de curl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5330.data"
# $Id: $
