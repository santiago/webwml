#use wml::debian::translation-check translation="eda8e74e2c1c2bf3112d3452306d23853a3c0454" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Qualys Research Labs a signalé un contournement non autorisé
(<a href="https://security-tracker.debian.org/tracker/CVE-2022-41974">CVE-2022-41974</a>)
et une attaque par liens symboliques
(<a href="https://security-tracker.debian.org/tracker/CVE-2022-41973">CVE-2022-41973</a>)
dans multipath-tools, un ensemble d'outils pour gérer des mappages de
périphériques disques multichemins, qui peuvent avoir pour conséquence une
élévation locale de privilèges.</p>

<p>Veuillez consulter /usr/share/doc/multipath-tools/NEWS.Debian.gz pour
connaître les modifications non rétrocompatibles dans cette mise à jour.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 0.8.5-2+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets multipath-tools.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de multipath-tools,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/multipath-tools">\
https://security-tracker.debian.org/tracker/multipath-tools</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5366.data"
# $Id: $
