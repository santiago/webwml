#use wml::debian::translation-check translation="febe4f74db24ecb7f868f3dc12ec713f8f748a3f" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans c-ares, une bibliothèque de
résolution de noms asynchrone :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31130">CVE-2023-31130</a>

<p>Il a été découvert qu'ares_inet_net_pton() était vulnérable à un
dépassement de tampon par le bas avec certaines adresses ipv6, et en
particulier que <q>0::00:00:00/2</q> provoquait un problème. c-ares utilise
seulement cette fonction en interne à des fins de configuration, cependant
une utilisation externe avec d'autres objectifs peut provoquer des
problèmes plus graves.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32067">CVE-2023-32067</a>

<p>Le résolveur cible peut interpréter à tort un paquet UDP mal formé de
longueur <q>0</q> comme un arrêt progressif de la connexion, ce qui peut
provoquer un déni de service.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 1.17.1-1+deb11u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets c-ares.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de c-ares, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/c-ares">\
https://security-tracker.debian.org/tracker/c-ares</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5419.data"
# $Id: $
