#use wml::debian::template title="Information pour les conseillers Debian"
#use wml::debian::translation-check translation="5f70081cb38037d61262d8cb159fb510c9315981" maintainer="David Prévot"

# Translators:
# Nicolas Bertolissio, 2007.
# Guillaume Delacour, 2009.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#policy">Charte des pages des conseillers Debian</a></li>
  <li><a href="#change">Ajouts, modifications et suppressions d'entrées</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Les conseillers peuvent communiquer entre eux en utilisant la <a href="https://lists.debian.org/debian-consultants/">liste de diffusion debian-consultants</a>. La liste n'est pas modérée ; tout le monde peut envoyer des messages. La liste de diffusion possède également une archive publique.</p>
</aside>

<h2><a id="policy">Charte des pages des conseillers Debian</a></h2>

<p>
Si vous souhaitez être listé en tant que conseiller sur le site de Debian,
veuillez noter les règles suivantes&nbsp;:
</p>

<ul>
  <li><strong>Informations de contact obligatoires</strong><br />
    Vous devez fournir une adresse électronique valide et répondre aux
    courriels en quatre semaines maximum. Afin d'éviter les abus, toutes les
    requêtes (ajouts, suppressions ou modifications) doivent être envoyées
    depuis la même adresse. Si vous voulez éviter les courriels
    indésirables, vous pouvez demander à ce que votre adresse électronique
    soit brouillée (par exemple <em>debian -point- conseil -chez- exemple
    -point- com</em>). Veuillez le mentionner explicitement dans votre
    candidature. Autrement, vous pouvez également demander à ce que votre
    adresse n'apparaisse pas du tout sur le site web (mais nous avons besoin
    d'une adresse fonctionnelle pour la maintenance de la liste). Si vous
    souhaitez que votre adresse soit absente de la liste, vous pouvez fournir
    à la place l'URL d'un formulaire de contact sur votre site web vers lequel
    nous pouvons faire un lien. <em>Contact</em> est prévu pour cela&nbsp;;
  </li>
  <li><strong>Votre site web :</strong><br />
    Si vous fournissez un lien vers un site web, ce site doit faire mention de
    vos services de conseil Debian. Il est préférable de fournir le lien direct
    plutôt que la page d'accueil, mais ce n'est pas obligatoire si
    l'information est facilement accessible.
  </li>
  <li><strong>Villes, régions ou pays multiples</strong><br />
    Vous devez choisir le pays (un seul) dans lequel vous souhaitez apparaître.
    Les villes, régions ou pays supplémentaires doivent être indiqués soit dans
    les informations complémentaires, soit sur votre propre site&nbsp;;
  </li>
  <li><strong>Règles pour les développeurs Debian</strong><br />
    Vous n'êtes pas autorisés à utiliser votre adresse courriel officielle
    <em>@debian.org</em> pour la liste des conseillers. C'est la même chose
    pour votre site web : vous ne pouvez pas utiliser de domaine officiel
    <em>*.debian.org</em> comme cela est indiqué dans les
    <a href="$(HOME)/devel/dmup">règles d'usage des machines de Debian</a>.
  </li>
</ul>

<p>
Si les critères ci-dessus ne sont plus remplis à un moment donné, le conseiller
reçoit un message d'avertissement lui indiquant qu'il est sur le point d'être
supprimé de la liste, à moins qu'il ne remplisse à nouveau tous les critères.
Il y a une période de grâce de quatre semaines.
</p>

<p>
Certaines (ou toutes les) informations des conseillers peuvent être supprimées
s'ils ne se conforment plus à cette charte ou à la discrétion des responsables
de la liste.
</p>


<h2><a id="change">Ajouts, modifications et suppressions d'entrées</a></h2>

<p>
Si vous souhaitez être ajouté à la liste des conseillers, veuillez envoyer un
courriel en anglais à <a 
href="mailto:consultants@debian.org">consultants@debian.org</a> contenant parmi
les informations suivantes toutes celles que vous souhaitez voir apparaître
dans la liste (l'adresse électronique est obligatoire, tout le reste est à votre
discrétion)&nbsp;:
</p>

<ul>
  <li><i>Country</i> (pays dans lequel vous voulez être référencé)</li>
  <li><i>Name</i> (nom)</li>
  <li><i>Company</i> (entreprise)</li>
  <li><i>Address</i> (adresse)</li>
  <li><i>Phone</i> (téléphone)</li>
  <li><i>Fax</i> (télécopie)</li>
  <li><i>Contact</i></li>
  <li><i>Email</i> (adresse électronique)</li>
  <li><i>URL</i> (adresse du site)</li>
  <li><i>Rates</i> (tarifs)</li>
  <li><i>Additional information</i> (informations complémentaires)</li>
</ul>

<p>
Les demandes de mises à jour des informations des conseillers doivent être
envoyées par courriel à
<a href="mailto:consultants@debian.org">consultants@debian.org</a>,
de préférence à partir de l'adresse indiquée sur la <a
href="https://www.debian.org/consultants/">page des conseillers</a>.</p>

# translators can, but don't have to, translate the rest - yet
# BEGIN future version
# Veuillez compléter le formulaire suivant&nbsp;:
# </p>
# 
# <form method=post action="https://cgi.debian.org/cgi-bin/submit_consultant.pl">
# 
# <p>
# <input type="radio" name="submissiontype" value="new" checked>
# Demande d'ajout à la liste des conseillers
# <br />
# <input type="radio" name="submissiontype" value="update">
# Mise à jour de la liste des conseillers existants
# <br />
# <input type="radio" name="submissiontype" value="remove">
# Suppression de la liste des conseillers existants
# </p>
# 
# <p>Nom&nbsp;:<br />
# <input type="text" name="name" size="50"></p>
# 
# <p>Entreprise&nbsp;:<br />
# <input type="text" name="company" size="50"></p>
# 
# <p>Adresse&snbp;:<br />
# <textarea name="comment" cols=35 rows=4></textarea></p>
# 
# <p>Téléphone&nbsp;:<br />
# <input type="text" name="phone" size="50"></p>
# 
# <p>Télécopie&nbsp;:<br />
# <input type="text" name="fax" size="50"></p>
# 
# <p>Adresse électronique&nbsp;:<br />
# <input type="text" name="email" size="50"></p>
# 
# <p>Adresse du site&nbsp;:<br />
# <input type="text" name="url" size="50"></p>
# 
# <p>Tarifs&nbsp;:<br />
# <textarea name="comment" cols=35 rows=4></textarea></p>
# 
# <p>Informations complémentaires, si nécessaire (<em>en anglais</em>)&nbsp;:<br />
# <textarea name="comment" cols=40 rows=7></textarea></p>
# 
# </form>
# 
# <p>
# Si vous n'arrivez pas à soumettre le formulaire ci-dessus pour quelque
# raison que ce soit, veuillez envoyer un courriel <em>en anglais</em> à
# END future version
