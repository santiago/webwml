#use wml::debian::translation-check translation="190f3d6514cde3c5d986336e54d61d03e1702b8f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans Werkzeug, une collection
d’utilitaires pour les applications WSGI (web). Un attaquant pouvait injecter
des cookies dans des situations particulières et causer un déni de service
(DoS).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23934">CVE-2023-23934</a>

<p>Werkzeug analysait le cookie <q>=__Host-test=bad</q> comme
<q>__Host-test=bad</q>. Si une application Werkzeug fonctionnait près d’un
sous-domaine vulnérable ou malveillant qui autorisait un tel cookie en utilisant
un navigateur vulnérable, l’application Werkzeug voyait la mauvaise valeur de
cookie mais aussi la clé valable de cookie. Les navigateurs pouvaient autoriser
des cookies<q>sans nom</q> qui ressemblaient à <q>=value</q> au lieu de
<q>key=value</q>. Un navigateur vulnérable pouvait permettre à une application
corrompue dans un sous-domaine adjacent d’exploiter cela pour définir un cookie
tel que <q>=__Host-test=bad</q> pour un autre sous-domaine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-25577">CVE-2023-25577</a>

<p>Un analyseur de données de formulaire multipartie de Werkzeug analysait un
nombre illimité de parties, dont des parties de fichier. Celles-ci pouvaient
être un petit nombre d’octets, mais chacune requérait du temps de CPU pour
l’analyse et pouvait utiliser plus de mémoire que les données de Python. Si une
requête pouvait être faite sur un terminal pouvant accéder à <q>request.data</q>,
<q>request.form</q>, <q>request.files</q> ou
<q>request.get_data(parse_form_data=False)</q>, elle pouvait provoquer contre
toute attente une utilisation élevée de ressources. Cela permettait à un
attaquant de provoquer un déni de service en envoyant des données multiparties
contrefaites à un terminal qui les analysait.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.14.1+dfsg1-4+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-werkzeug.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-werkzeug,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-werkzeug">\
https://security-tracker.debian.org/tracker/python-werkzeug</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3346.data"
# $Id: $
