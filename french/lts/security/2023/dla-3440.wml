#use wml::debian::translation-check translation="d5c65b03f64abbf68af9477688bbd1a6e1dc9ab3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un problème a été découvert dans cups, le système commun d'impression pour
UNIX. À cause d’une vulnérabilité de dépassement de tampon dans la fonction
format_log_line(), un attaquant distant pouvait provoquer un déni de service
(DoS). La vulnérabilité pouvait être déclenchée quand le fichier de
configuration, cupsd.conf, réglait la valeur de <q>loglevel</q> à <q>DEBUG</q>.</p>


<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.2.10-6+deb10u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cups.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cups,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cups">\
https://security-tracker.debian.org/tracker/cups</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3440.data"
# $Id: $
