#use wml::debian::translation-check translation="3cb65f4d79aabec4232bdf81fbcbb507fa840e1d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans snort, un
système de détection d’intrusion. Elles pouvaient permettre à un attaquant non
authentifié et distant de provoquer une condition de déni de service (DoS) ou
de contournement de la technologie de filtrage d’un périphérique affecté et
dérober des données de l’hôte compromis.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.9.20-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets snort.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de snort,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/snort">\
https://security-tracker.debian.org/tracker/snort</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3317.data"
# $Id: $
