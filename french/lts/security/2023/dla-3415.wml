#use wml::debian::translation-check translation="caf14100a5c14ac0032f9067beb1c520db3b4161" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un contournement potentiel de validation
dans <a href="https://djangoproject.com">Django</a>, un cadriciel populaire de
développement web.</p>

<p>Le téléchargement de plusieurs fichiers en utilisant un champ de formulaire
n’a jamais été pris en charge par <code>forms.FileField</code> ou
<code>forms.ImageField</code>, car seul le dernier fichier téléchargé était
validé. Malheureusement, le sujet sur le téléchargement de plusieurs fichiers
dans la documentation laissait penser autrement.</p>

<p>Pour éviter cette vulnérabilité, les composants graphiques
<code>ClearableFileInput</code> et <code>FileInput</code> du formulaire soulève
<code>ValueError</code> quand plusieurs attributs HTML y sont déclarés. Pour
empêcher cette exception et conserver l’ancien comportement,
<code>allow_multiple_selected</code> doit être réglé à <code>True</code>.</p>

<p>Pour plus de détails sur l’utilisation du nouvel attribut et le traitement
de plusieurs fichiers en utilisant un seul champ, veuillez consulter le
<a href="https://www.djangoproject.com/weblog/2023/may/03/security-releases/">site de l’amont</a>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31047">CVE-2023-31047</a>

<p>Contournement potentiel de validation lors du téléchargement de plusieurs
fichiers en utilisant un seul champ de formulaire.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:1.11.29-1+deb10u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3415.data"
# $Id: $
