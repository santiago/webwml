#use wml::debian::translation-check translation="df4ff68bc319cbda2a210310589cec754312f83a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Des vulnérabilités de dépassement de tampon ont été trouvées dans libraw,
une bibliothèque de décodage d’image brute, qui pouvaient conduire à un
plantage d'application ou à une élévation des privilèges.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32142">CVE-2021-32142</a>

<p>Une vulnérabilité de dépassement de tampon a été découverte dans
<code>LibRaw_buffer_datastream::gets(char*, int)</code>, qui pouvait conduire
à une élévation des privilèges ou à un plantage d'application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1729">CVE-2023-1729</a>

<p>Un dépassement de tampon de tas a été découvert dans
<code>LibRaw::raw2image_ex(int)</code>, qui pouvait conduire à un plantage
d'application à l’aide de fichiers d’entrée contrefaits de manière
malveillante.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.19.2-2+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libraw.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libraw,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libraw">\
https://security-tracker.debian.org/tracker/libraw</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3433.data"
# $Id: $
