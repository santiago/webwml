#use wml::debian::translation-check translation="23f5b67d4354be2619d99621e7acc75821a38d38" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>David Marchand a découvert que Open vSwitch, un commutateur virtuel Ethernet
multicouche et logiciel, était vulnérable à des paquets IP contrefaits avec
<q>ip proto</q> réglé à 0, provoquant éventuellement un déni de service.</p>

<p>Le déclenchement de cette vulnérabilité nécessitait qu’un attaquant envoie
un paquet IP contrefait avec le champ de protocole réglé à <code>0</code> et
que les règles de flux contiennent les actions <code>set</code> dans les autres
champs de l’en-tête du protocole IP. Les flux résultants omettaient alors les
actions requises et échouaient à masquer le champ du protocole IP, aboutissant
à un large panier capturant tous les paquets IP.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.10.7+ds1-0+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openvswitch.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openvswitch,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openvswitch">\
https://security-tracker.debian.org/tracker/openvswitch</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3410.data"
# $Id: $
