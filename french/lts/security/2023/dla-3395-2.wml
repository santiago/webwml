#use wml::debian::translation-check translation="679ce95438e8c931d856a9d895194bd4fe17d161" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de régression pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour précédente de golang-1.11 publiée dans la DLA-3395-1 échouait
à construire de manière fiable sur l’architecture armhf, à cause de cas
erratiques de tests peu fiables dans la suite de tests qui est réalisée sur un
paquet Debian pour un gage de qualité. Cette mise à jour désactive quelques-uns
de ces tests, et n’a jusqu’à nouvel ordre aucun impact sur les paquets
golang-1.11 publiés.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.11.6-1+deb10u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets golang-1.11.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de golang-1.11,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/golang-1.11">\
https://security-tracker.debian.org/tracker/golang-1.11</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3395-2.data"
# $Id: $
