#use wml::debian::translation-check translation="eaf7e53dfd89887495d459252a3e7e41275998b3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une erreur de logique a été découverte dans l'implémentation de l'option
<q>SafeSocks</q> de Tor, un système de communication anonyme à faible latence,
basé sur les connexions, qui avait pour conséquence de laisser passer un trafic
SOCKS4 non sécurisé.</p>


<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 0.3.5.16-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tor.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tor,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tor">\
https://security-tracker.debian.org/tracker/tor</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3286.data"
# $Id: $
