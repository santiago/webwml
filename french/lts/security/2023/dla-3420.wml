#use wml::debian::translation-check translation="4469ab531ce7a9d67b22336fb0d353fd5b799c1f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de dépassement d’entier existait dans golang-websocket,
un paquet de Go mettant en œuvre une connexion utilisant le protocole WebSocket.
Un attaquant pouvait utiliser ce défaut pour provoquer une attaque par déni de
service sur un serveur HTTP autorisant les connexions websocket.</p>

<p>Les dépendances arrières suivantes ont été reconstruites avec la nouvelle
version de golang-websocket : hugo et gitlab-workhorse.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.4.0-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets golang-websocket.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de golang-websocket,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/golang-websocket">\
https://security-tracker.debian.org/tracker/golang-websocket</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3420.data"
# $Id: $
