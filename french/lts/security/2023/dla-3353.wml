#use wml::debian::translation-check translation="da223faadfaa5bf8b212107dd2e9d82ec15bfe37" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème de sécurité a été découvert dans xfig, un outil de dessin pour la
création interactive de figures sous X11.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40241">CVE-2021-40241</a>:

<p>Un dépassement potentiel de tampon existait dans le fichier src/w_help.c à la
ligne 55. Plus précisément, la longueur de la chaîne renvoyée par getenv("LANG")
pouvait devenir très importante et provoquer un dépassement de tampon lors de
l’exécution de la fonction sprintf(). Cette vulnérabilité pouvait éventuellement
permettre à un attaquant d’exécuter du code arbitraire ou de créer une
possibilité de déni de service.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:3.2.7a-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xfig.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de xfig,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/xfig">\
https://security-tracker.debian.org/tracker/xfig</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3353.data"
# $Id: $
