#use wml::debian::translation-check translation="cccd467920bb8ee8cbe290b5b443c4ac2dcb7a87" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans tiff, une bibliothèque et des
outils prenant en charge le format TIFF (Tag Image File Format), conduisant à
un déni de service (DoS) et, éventuellement, à une exécution de code locale.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1354">CVE-2022-1354</a>

<p>Un défaut de dépassement de tampon de tas a été découvert dans tiffinfo.c
de Libtiffs dans la fonction TIFFReadRawDataStriped(). Ce défaut permettait à
un attaquant de passer un fichier TIFF contrefait à l’outil tiffinfo, provoquant
un dépassement de tampon de tas et un plantage aboutissant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1355">CVE-2022-1355</a>

<p>Un défaut de dépassement de tampon de pile a été découvert dans tiffcp.c de
Libtiffs dans la fonction main(). Ce défaut permettait à un attaquant de passer
un fichier TIFF contrefait à l’outil tiffcp, déclenchant un dépassement de
tampon de pile et, éventuellement, corrompant la mémoire, provoquant un plantage
aboutissant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2056">CVE-2022-2056</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-2057">CVE-2022-2057</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-2058">CVE-2022-2058</a>

<p>Une erreur de division par zéro dans tiffcrop permettait à des attaquants de
provoquer un déni de service à l'aide d'un fichier tiff contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2867">CVE-2022-2867</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-2868">CVE-2022-2868</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-2869">CVE-2022-2869</a>

<p>L’utilitaire tiffcrop de libtiff avait un dépassement de capacité par le bas
et un défaut de validation d’entrée qui pouvait conduire à une lecture ou
écriture hors limites. Un attaquant fournissant un fichier contrefait à tiffcrop
(vraisemblablement en dupant un utilisateur pour exécuter tiffcrop sur lui avec
certains paramètres) pouvait causer un plantage ou, dans certains cas, une
exploitation future.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3570">CVE-2022-3570</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-3598">CVE-2022-3598</a>

<p>Plusieurs dépassements de tampon de tas dans l’utilitaire tiffcrop.c dans
libtiff permettaient de déclencher un accès en mémoire non sûr ou hors limites
à l’aide d’un fichier image TIFF contrefait, pouvant aboutir à un plantage
d'application, une divulgation potentielle d'informations ou d’autres impacts
selon le contexte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3597">CVE-2022-3597</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-3626">CVE-2022-3626</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-3627">CVE-2022-3627</a>

<p>Une écriture hors limites permettait à des attaquants de provoquer un déni de
service à l'aide d'un fichier tiff contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3599">CVE-2022-3599</a>

<p>Une lecture hors limites dans writeSingleSection dans tools/tiffcrop.c
permettait à des attaquants de provoquer un déni de service à l'aide d'un
fichier tiff contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3970">CVE-2022-3970</a>

<p>Cette vulnérabilité touchait la fonction TIFFReadRGBATileExt du fichier
libtiff/tif_getimage.c. La manipulation conduisait à un dépassement d'entier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34526">CVE-2022-34526</a>

<p>Un débordement de pile a été découvert dans la fonction _TIFFVGetField de
Tiffsplit. Cette vulnérabilité permettait à des attaquants de provoquer un déni
de service (DoS) à l'aide d'un fichier TIFF contrefait analysé par les
utilitaires <q>tiffsplit</q> ou <q>tiffcrop</q>.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 4.1.0+git191117-2~deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tiff,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tiff">\
https://security-tracker.debian.org/tracker/tiff</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3278.data"
# $Id: $
