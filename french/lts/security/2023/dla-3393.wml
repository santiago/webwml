#use wml::debian::translation-check translation="71670bc97219c86e67e2534a0429f4d9b5c0e15f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour corrige un déréférencement de pointeur NULL et
deux conditions de déni de service dans protobuf.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22569">CVE-2021-22569</a>

<p>Un problème dans protobuf-java permettait l’entrelacement de champs
<code>com.google.protobuf.UnknownFieldSet</code> de telle façon qu’ils étaient
traités dans le désordre . Une petite charge utile malveillante pouvait occuper
l’analyseur pendant plusieurs minutes en créant un grand nombre d’objets de
courte durée de vie qui provoquaient des pauses fréquentes et répétées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22570">CVE-2021-22570</a>

<p>Déréférencement de nullptr quand un caractère NULL est présent dans un
symbole proto. Celui-ci est analysé incorrectement, conduisant à un appel non
vérifié dans le nom de fichier proto lors de la génération du message d’erreur
résultant. Puisque le symbole est mal analysé, le fichier est nullptr.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1941">CVE-2022-1941</a>

<p>Une vulnérabilité d’analyse pour le type <code>MessageSet</code>
ProtocolBuffers peut conduire à des échecs de mémoire. Un message contrefait
pour l'occasion avec plusieurs <q>clé-valeur</q> par éléments créait des erreurs
d’analyse et pouvait conduire à un déni de service à l’encontre de services
recevant une entrée non nettoyée.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 3.6.1.3-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets protobuf.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de protobuf,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/protobuf">\
https://security-tracker.debian.org/tracker/protobuf</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3393.data"
# $Id: $
