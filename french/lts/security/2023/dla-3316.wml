#use wml::debian::translation-check translation="e216ff82dd2bd263c1d40db845a1bbb6fbd1848c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Jacob Champion a découvert que libpq pouvait divulguer le contenu de la
mémoire après que l’initialisation du chiffrement du transport GSSAPI ait
échoué.</p>

<p>Un serveur modifié, ou un homme du milieu non authentifié, pouvait envoyer
un message d’erreur de fin non terminée par zéro lors de la mise en place du
chiffrement de transport GSSAPI (Kerberos). libpq copiait alors cette chaine,
ainsi que les octets suivants, dans la mémoire de l’application jusqu’au
prochain octet zéro pour son rapport d’erreur. Selon ce que l’application faisait
avec le rapport d’erreur, cela pouvait aboutir à une divulgation du contenu de
la mémoire. Il existait aussi une faible probabilité de plantage à cause d’une
lecture après la fin de la mémoire. Corrigez le problème en terminant par zéro
le message du serveur.
(<a href="https://security-tracker.debian.org/tracker/CVE-2022-41862">CVE-2022-41862</a>)</p>


<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 11.19-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets postgresql-11.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de postgresql-11,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/postgresql-11">\
https://security-tracker.debian.org/tracker/postgresql-11</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3316.data"
# $Id: $
