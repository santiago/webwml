#use wml::debian::translation-check translation="5d3c177a7a8181b7c9e47472519a548ec852f14b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une élévation potentielle locale des
privilèges dans GruntJS, un outil générique d’exécution de tâches et un système
de construction.</p>

<p>Les opérations <code>file.copy</code> dans GruntJS étaient vulnérables à une
situation de compétition TOCTOU (<q>time-of-check vs. time-of-use</q>) qui
pouvait amener à des écritures arbitraires de fichiers dans des dépôts de
GitHub. Cela pouvait conduire à une élévation locale des privilèges si un
utilisateur moins privilégié avait un accès en écriture à la fois aux
répertoires source et destination, car celui-ci pouvait avoir créé un lien
symbolique vers le fichier de configuration <code>~/.bashrc</code> de
l’utilisateur de GruntJS (etc.).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1537">CVE-2022-1537</a>

<p>Les opérations <code>file.copy</code> dans GruntJS étaient vulnérables à une
situation de compétition TOCTOU qui pouvait amener à des écritures arbitraires
de fichiers dans des dépôts de GitHub gruntjs/grunt avant la version 1.5.3.
Cette vulnérabilité permettait des écritures arbitraires de fichiers pouvant
amener à une élévation locale des privilèges de l’utilisateur de GruntJS si un
utilisateur moins privilégié avait un accès en écriture à la fois aux
répertoires source et destination, car celui-ci pouvait avoir créé un lien
symbolique vers le fichier de configuration <code>~/.bashrc</code> de
l’utilisateur de GruntJS ou remplacé le fichier /etc/shadow si l’utilisateur
de GruntJS était le superutilisateur.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.0.1-8+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets grunt.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3383.data"
# $Id: $
