#use wml::debian::translation-check translation="1065d2ca418a628dd985939933435259f8c76cbe" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité potentielle de
dépassement de tampon dans ghostscript, un interpréteur populaire pour le
langage PostScript utilisé, par exemple, pour créer des fichiers PDF.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28879">CVE-2023-28879</a>

<p>Dans Ghostscript d’Artifex jusqu’à la version 10.01.0, un dépassement de
tampon existe, conduisant à une corruption potentielle de données, interne à
l’interpréteur PostScript, dans base/sbcp.c. Cela affecte BCPEncode, BCPDecode,
TBCPEncode et TBCPDecode. Si le tampon écrit est rempli à un octet de moins
que le total possible et qu’une écriture d’un caractère d’échappement est
tentée, alors deux octets sont écrits.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 9.27~dfsg-2+deb10u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ghostscript.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3381.data"
# $Id: $
