#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Grégoire Scano"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de sécurité et un bogue de perte de données ont été découverts dans
postgresql-common, des outils de gestion de grappe de base de données PostgreSQL de
Debian.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1255">CVE-2016-1255</a>

<p>Dawid Golunski a découvert qu'un lien symbolique dans /var/log/postgresql/
pourrait être utilisé par l'utilisateur système <q>postgres</q> pour écrire dans un fichier arbitraire sur le système de fichiers la prochaine fois que PostgreSQL est démarré par le superutilisateur.</p></li>

<li>n° 614374

<p>Rafał Kupka a découvert que pg_upgradecluster n'effectuait pas correctement
la mise à jour de bases de données détenues par un rôle (ou groupe) sans connexion.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 134wheezy5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets postgresql-common.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-774.data"
# $Id: $
