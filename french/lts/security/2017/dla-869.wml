#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>L’équipe de sécurité de cPanel a découvert plusieurs vulnérabilités de
sécurité dans cgiemail, un programme CGI utilisé pour créer des formulaires HTML
pour l’envoi de courriels.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5613">CVE-2017-5613</a>

<p>Une vulnérabilité d’injection de chaîne de formatage permettait de fournir
des chaînes de formatage arbitraires à cgiemail et cgiecho. Un attaquant local
ayant la permission de fournir un modèle cgiemail pourrait utiliser cette
vulnérabilité pour exécuter du code en tant qu’utilisateur du serveur web. Les
chaînes de formatage dans les patrons cgiemail sont maintenant limitées à des
séquences simples, %s, %U et %H.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5614">CVE-2017-5614</a>

<p>Une vulnérabilité de redirection ouverte dans les binaires cgiemail et
cgiecho pourrait être exploitée par un attaquant local pour forcer la
redirection vers une URL arbitraire. Ces redirections sont maintenant limitées
au domaine gérant la requête.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5615">CVE-2017-5615</a>

<p>Une vulnérabilité dans les binaires cgiemail et cgiecho permettait une
injection d’en-têtes HTTP supplémentaires. Les caractères de nouvelle ligne sont
maintenant retirés de l’emplacement de redirection pour une protection contre
cela.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5616">CVE-2017-5616</a>

<p>Une protection manquante du paramètre addendum conduit à une vulnérabilité de
script intersite réfléchi (XSS) dans les binaires cgiemail et cgiecho. La sortie
est protégée contre l’HTML.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.6-37+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cgiemail.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-869.data"
# $Id: $
