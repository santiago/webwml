#use wml::debian::translation-check translation="970cc4501d4669f175b3cc84435d7a21e11ce641" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été découverts dans dovecot, un serveur
de courrier électronique IMAP et POP3.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33515">CVE-2021-33515</a>

<p>Le service submission dans Dovecot avant la version 2.3.15 permet une
injection de commande STARTTLS dans lib-smtp. Des informations sensibles
peuvent être redirigées vers une adresse contrôlée par l'attaquant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30550">CVE-2022-30550</a>

<p>Quand deux entrées de configuration passdb existent avec les mêmes
configurations de pilote et de paramètres, les réglages incorrectement
appliqués peuvent conduire à une configuration de sécurité non prévue et
permettre une élévation de privilèges dans certaines configurations.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
1:2.3.4.1-5+deb10u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets dovecot.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de dovecot, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/dovecot">\
https://security-tracker.debian.org/tracker/dovecot</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3122.data"
# $Id: $
