#use wml::debian::translation-check translation="74e9bfcc5c7a4ee9da9dc7df82bf3218112f8e51" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes ont été découverts dans GnuTLS, une bibliothèque
implémentant les protocoles TLS et SSL. Un attaquant distant pouvait tirer
avantage de ces défauts pour provoquer le plantage d'une application
utilisant la bibliothèque GnuTLS (déni de service), ou éventuellement, pour
exécuter du code arbitraire.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans la
version 3.6.7-4+deb10u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gnutls28.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gnutls28, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/gnutls28">\
https://security-tracker.debian.org/tracker/gnutls28</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3070.data"
# $Id: $
