#use wml::debian::translation-check translation="33e2a388eeeeebef8c7fa168b1c354365406051b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Jhead, un outil pour manipuler les données EXIF imbriquées dans les images
JPEG, permettait à des attaquants d’exécuter des commandes arbitraires de
système d’exploitation en les plaçant dans un nom de fichier JPEG, et en
utilisant l’option de régénération « -rgt50 », « -autorot » ou « -ce ». De plus,
une erreur de dépassement de tampon dans exif.c a été corrigée, qui pouvait
conduire à un déni de service (plantage d'application).</p>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 1:3.00-8+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jhead.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de jhead,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/jhead">\
https://security-tracker.debian.org/tracker/jhead</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3219.data"
# $Id: $
