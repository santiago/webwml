#use wml::debian::translation-check translation="be354f6329774156579ac1978ed0c5b911b59401" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans pngcheck, un outil
pour vérifier l'intégrité de fichiers PNG, JNG et MNG, qui pouvaient
éventuellement avoir pour conséquence l'exécution de code arbitraire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35511">CVE-2020-35511</a>

<p>Un dépassement global de tampon a été découvert dans la fonction pngcheck
dans pngcheck-2.4.0 à l'aide d'un fichier png contrefait (5 correctifs
appliqués).</p>


<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 3.0.3-1~deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pngcheck.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de pngcheck,
veuillez consulter sa page de suivi de sécurité à l'adresse : <a href="https://security-tracker.debian.org/tracker/pngcheck">\
https://security-tracker.debian.org/tracker/pngcheck</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3238.data"
# $Id: $
