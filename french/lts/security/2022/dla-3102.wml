#use wml::debian::translation-check translation="54556417b1e12a0c5f972aff2457f7522837c3f9" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Linux 5.10 a été empaqueté pour Debian 10 en tant que linux-5.10. Cela
fournit un chemin de mise à niveau pris en charge pour les systèmes qui
utilisent les paquets du noyau de la suite <q>buster-backports</q>.</p>

<p>Il n'est pas nécessaire de mettre à niveau les systèmes utilisant
Linux 4.19, dans la mesure où cette version du noyau continuera à être
prise en charge durant toute la période de prise en charge à long terme.</p>

<p>La commande <q>apt full-upgrade</q> <em>n'installera pas</em>
automatiquement les paquets du noyau mis à jour. Vous devrez d'abord
installer explicitement le métapaquet adapté à votre système parmi les
suivant :

<ul>
<li>linux-image-5.10-686</li>
<li>linux-image-5.10-686-pae</li>
<li>linux-image-5.10-amd64</li>
<li>linux-image-5.10-arm64</li>
<li>linux-image-5.10-armmp</li>
<li>linux-image-5.10-armmp-lpae</li>
<li>linux-image-5.10-cloud-amd64</li>
<li>linux-image-5.10-cloud-arm64</li>
<li>linux-image-5.10-rt-686-pae</li>
<li>linux-image-5.10-rt-amd64</li>
<li>linux-image-5.10-rt-arm64</li>
<li>linux-image-5.10-rt-armmp</li>
</ul>

<p>Par exemple, si la commande <q>uname -r</q> affiche actuellement
<q>5.10.0-0.deb10.16-amd64</q>, vous devez installer
linux-image-5.10-amd64.</p>

<p>Ce rétroportage ne comprend pas les paquets binaires suivants :</p>

<blockquote><div>bpftool hyperv-daemons libcpupower-dev libcpupower1
linux-compiler-gcc-8-arm linux-compiler-gcc-8-x86 linux-cpupower
linux-libc-dev usbip</div></blockquote>

<p>Les versions les plus anciennes de la plupart de ces paquets sont
construites à partir du paquet source linux dans Debian 10.</p>

<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou à
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2585">CVE-2022-2585</a>

<p>Un défaut d'utilisation de la mémoire après libération dans
l'implémentation des temporisations CPU POSIX peut avoir pour conséquences
un déni de service ou une élévation locale de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2586">CVE-2022-2586</a>

<p>Une utilisation de mémoire après libération dans le sous-système
Netfilter peut avoir pour conséquence une élévation locale de privilèges
pour un utilisateur doté de la capacité CAP_NET_ADMIN dans n'importe quel
espace de noms utilisateur ou réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2588">CVE-2022-2588</a>

<p>Zhenpeng Lin a découvert un défaut d'utilisation de mémoire après
libération dans l'implémentation du filtre cls_route qui peut avoir pour
conséquence une élévation locale de privilèges pour un utilisateur doté de
la capacité CAP_NET_ADMIN dans n'importe quel espace de noms utilisateur ou
réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26373">CVE-2022-26373</a>

<p>Sur certains processeurs dotés des capacités <q>Enhanced Indirect Branch
Restricted Speculation</q> (eIBRS) d'Intel, il y a des exceptions aux
propriétés documentées dans certaines situations qui peuvent avoir pour
conséquence la divulgation d'informations.</p>

<p>L'explication du problème par Intel peut être trouvée sur la page
<a href="https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/post-barrier-return-stack-buffer-predictions.html">\
https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/post-barrier-return-stack-buffer-predictions.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29900">CVE-2022-29900</a>

<p>Johannes Wikner et Kaveh Razavi ont signalé que pour les processeurs
AMD ou Hygon des prédictions de branchement avec un mauvais apprentissage
pour des instructions <q>return</q> peuvent permettre l'exécution de code
spéculatif arbitraire dans certaines conditions dépendant de la
micro-architecture.</p>

<p>Une liste des types de processeurs AMD affectés se trouve sur la page
<a href="https://www.amd.com/en/corporate/product-security/bulletin/amd-sb-1037">\
https://www.amd.com/en/corporate/product-security/bulletin/amd-sb-1037</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29901">CVE-2022-29901</a>

<p>Johannes Wikner et Kaveh Razavi ont signalé que pour les processeurs
Intel (Intel Core génération 6, 7 et 8), les protections contre les
attaques par injection de cible de branchement spéculatif étaient
insuffisantes dans certaines circonstances, ce qui peut permettre
l'exécution de code spéculatif arbitraire dans certaines conditions
dépendant de la micro-architecture.</p>

<p>D'avantage d'informations peuvent être trouvées sur la page
<a href="https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/return-stack-buffer-underflow.html">\
https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/return-stack-buffer-underflow.html</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36879">CVE-2022-36879</a>

<p>Un défaut a été découvert dans xfrm_expand_policies dans le sous-système
xfrm qui peut faire qu'un compte de références soit supprimé deux fois.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36946">CVE-2022-36946</a>

<p>Domingo Dirutigliano et Nicola Guerrera ont signalé un défaut de
corruption de mémoire dans le sous-système Netfilter qui peut avoir pour
conséquence un déni de service.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
5.10.136-1~deb10u3. En complément, cette mise à jour comprend beaucoup plus
de corrections de bogues issues des mises à jour des versions stables de
5.10.128 à 5.10.136 inclus.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-5.10.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux-5.10, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux-5.10">\
https://security-tracker.debian.org/tracker/linux-5.10</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3102.data"
# $Id: $
