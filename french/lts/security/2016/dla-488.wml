#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Markus Krell a découvert que xymon (anciennement connu sous le nom de
Hobbit), un système de surveillance de réseaux et d'applications, était
vulnérable aux problèmes de sécurité suivants :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2054">CVE-2016-2054</a>

<p>Le traitement incorrect des entrées fournies par l'utilisateur dans la
commande <q>config</q> peut déclencher un dépassement de pile, menant à un
déni de service (au moyen du plantage de l'application) ou à l'exécution de
code distant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2055">CVE-2016-2055</a>

<p>Le traitement incorrect des entrées fournies par l'utilisateur dans la
commande <q>config</q> peut conduire à une fuite d'informations en servant
des fichiers de configuration sensibles à un utilisateur distant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2056">CVE-2016-2056</a>

<p>Les commandes traitant la gestion des mots de passe ne valident pas
correctement les entrées fournies par l'utilisateur et sont ainsi
vulnérables à l'injection de commande shell par un utilisateur distant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2058">CVE-2016-2058</a>

<p>L'échappement incorrect des entrées fournies par l'utilisateur dans les
pages web d'état peut être utilisé pour déclencher des attaques
réfléchies par script intersite.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 4.3.0~beta2.dfsg-9.1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xymon.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-488.data"
# $Id: $
