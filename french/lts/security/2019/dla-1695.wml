#use wml::debian::translation-check translation="fef912fdfc278f3eb829dd42319c335f17e8f4ad" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans SoX (Sound eXchange),
un programme de traitement de sons.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15370">CVE-2017-15370</a>

<p>The fonction ImaAdpcmReadBlock (src/wav.c) est sujette à un dépassement de
tampon basé sur le tas. Cette vulnérabilité peut être exploitée par des
attaquants distants utilisant un fichier WAV contrefait pour provoquer un déni
de service (plantage d'application).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15372">CVE-2017-15372</a>

<p>La fonction lsx_ms_adpcm_block_expand_i (adpcm.c) est sujette à un
dépassement de pile. Cette vulnérabilité peut être exploitée par des attaquants
distants utilisant un fichier audio contrefait pour provoquer un déni de service
(plantage d'application).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15642">CVE-2017-15642</a>

<p>La fonction lsx_aiffstartread (aiff.c) est sujette à une vulnérabilité
d’utilisation de mémoire après libération. Ce défaut peut être exploité par des
attaquants distants utilisant un fichier AIFF contrefait pour provoquer un déni
de service (plantage d'application).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18189">CVE-2017-18189</a>

<p>La fonction startread (xa.c) est sujette à une vulnérabilité de
déréférencement de pointeur NULL. Ce défaut peut être exploité par des
attaquants distants utilisant un un fichier audio Maxis XA contrefait pour
provoquer un déni de service (plantage d'application).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 14.4.1-5+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sox.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1695.data"
# $Id: $
