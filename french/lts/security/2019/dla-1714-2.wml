#use wml::debian::translation-check translation="28d1cad20ee56b498daf616b7b98c31beee55d55" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour de libsdl2 publiée dans la DLA 1714-1 aboutissait à plusieurs
régressions, comme signalées par Avital Ostromich. Celles-ci sont provoquées par
des correctifs pour libsdl1.2 pour
<a href="https://security-tracker.debian.org/tracker/CVE-2019-7637">CVE-2019-7637</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-7635">CVE-2019-7635</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-7638">CVE-2019-7638</a>
et <a href="https://security-tracker.debian.org/tracker/CVE-2019-7636">CVE-2019-7636</a>
appliqués à libsdl2 sans adaptation.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 2.0.2+dfsg1-6+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libsdl2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1714-2.data"
# $Id: $
