#use wml::debian::translation-check translation="38529fd25d5a417468faf6eec715b30e3cd064e4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité de dépassement de tampon
dans <a href="http://slirp.sourceforge.net/">slirp</a>, un émulateur SLIP/PPP
pour utiliser un compte d’interpréteur d’accès par ligne commutée. Cela était
provoqué par une utilisation incorrecte des valeurs de renvoi
de <tt>snprintf(3)</tt>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8608">CVE-2020-8608</a>

<p>Dans libslirp 4.1.0, tel qu’utilisé dans QEMU 4.2.0, tcp_subr.c utilise
improprement les valeurs de renvoi, conduisant à un dépassement de tampon dans
le code ultérieur.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:1.0.17-7+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets slirp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2142.data"
# $Id: $
