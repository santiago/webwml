#use wml::debian::translation-check translation="a5e4e9174786c758beaf054ccc6c89b05a203dc4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité de déni de service a été découverte dans ntp, le client/serveur
du protocole de synchronisation horaire.</p>

<p>ntp permettait à un attaquant « hors chemin » (off-path) de bloquer une
synchronisation non authentifiée à l'aide d'un mode serveur avec une adresse
IP source usurpée car les transmissions étaient reprogrammées même si un paquet
n’avait pas d’estampille temporelle valable (origin timestamp).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11868">CVE-2020-11868</a>

<p>ntpd dans ntp avant la version 4.2.8p14 et les versions 4.3.x avant la
version 4.3.100 permettait à un attaquant « hors chemin » de bloquer une
synchronisation non authentifiée à l'aide d'un mode serveur avec une adresse
IP source usurpée car les transmissions étaient reprogrammées même si un paquet
n’avait pas d’estampille temporelle valable.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:4.2.6.p5+dfsg-7+deb8u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ntp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2201.data"
# $Id: $
