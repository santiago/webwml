#use wml::debian::translation-check translation="03216a9851a5b5d8a84b98d3895478b2619b7a06" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Andres Freund a trouvé un problème dans le système de base de données
PostgreSQL où un chemin de recherche non contrôlé pouvait permettre aux
utilisateurs d’exécuter des fonctions SQL arbitraires avec des privilèges élevés
quand un superutilisateur exécutait certaines instructions « CREATE EXTENSION ».</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 9.6.19-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets postgresql-9.6.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de postgresql-9.6, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/postgresql-9.6">https://security-tracker.debian.org/tracker/postgresql-9.6</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2331.data"
# $Id: $
