#use wml::debian::translation-check translation="e5db2f4fb19495cb97535a208f169cf7f44d7387" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>ZeroMQ, une bibliothèque centrale légère de messagerie ne gère pas
correctement la connexion à des pairs avant que la poignée de main ne soit
achevée. Un client distant non authentifié se connectant à une application qui
utilise la bibliothèque libzmq, exécutée avec l'écoute d'un socket ayant le
chiffrage et l'authentification CURVE activés, peut tirer avantage de ce défaut
pour provoquer un déni de service affectant des clients authentifiés et chiffrés.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 4.2.1-4+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zeromq3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de zeromq3, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/zeromq3">https://security-tracker.debian.org/tracker/zeromq3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2443.data"
# $Id: $
