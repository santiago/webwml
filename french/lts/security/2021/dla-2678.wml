#use wml::debian::translation-check translation="1c64c6e5ff0bc4efc7c8f9d9fc5c9b88dfbf6d23" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité d’entité externe XML a été découverte dans Nokogiri, un
gem de Ruby fournissant des analyseurs HTML, XML, SAX et Reader avec prise en
charge de XPath et de sélection CSS.</p>

<p>Les schémas XML analysés par Nokogiri::XML::Schema étaient considérés comme
fiables par défaut, permettant l’accès à des ressources externes à travers le
réseau, autorisant éventuellement des attaques XXE ou SSRF. Le nouveau
comportement par défaut est de traiter toutes les entrées comme non fiables.
L’annonce de l’amont fournit d’autres informations pour mitiger le problème ou
pour restaurer l’ancien comportement.</p>

<p><a rel="nofollow" href="https://github.com/sparklemotion/nokogiri/security/advisories/GHSA-vr8q-g5c7-m54m">https://github.com/sparklemotion/nokogiri/security/advisories/GHSA-vr8q-g5c7-m54m</a>.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.6.8.1-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-nokogiri.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-nokogiri, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ruby-nokogiri">\
https://security-tracker.debian.org/tracker/ruby-nokogiri</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2678.data"
# $Id: $
