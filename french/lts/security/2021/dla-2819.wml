#use wml::debian::translation-check translation="c8bec78c8bd715b6b5debabee816cc982707088d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans NTFS-3G, un pilote de
lecture et écriture NTFS pour FUSE. Un utilisateur local peut tirer
avantage de ces défauts pour une élévation locale de privilèges
administrateur.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1:2016.2.22AR.1+dfsg-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ntfs-3g.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ntfs-3g, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ntfs-3g">\
https://security-tracker.debian.org/tracker/ntfs-3g</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2819.data"
# $Id: $
