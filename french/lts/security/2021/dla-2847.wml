#use wml::debian::translation-check translation="59d3769a2263283ac2d0576cae0644fc8433693e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème de sécurité a été découvert dans MediaWiki, un moteur de
site web pour travail collaboratif : l'absence de validation dans l'action
de mcrundo peut permettre à un attaquant de divulguer des contenus de pages
issus de wikis privés ou de contourner des restrictions d'édition.</p>

<p>Pour davantage d'informations, veuillez consulter
<a href="https://www.mediawiki.org/wiki/2021-12_security_release/FAQ">\
https://www.mediawiki.org/wiki/2021-12_security_release/FAQ</a>.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1:1.27.7-1+deb9u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mediawiki.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mediawiki, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mediawiki">\
https://security-tracker.debian.org/tracker/mediawiki</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2847.data"
# $Id: $
