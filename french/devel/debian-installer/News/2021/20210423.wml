#use wml::debian::translation-check translation="58f09ddfae9948ac0ceca3e1ddd5baadc0d529a6" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Bullseye RC 1</define-tag>
<define-tag release_date>2021-04-23</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la première version candidate pour Debian 11 <q>Bullseye</q>.
</p>


<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>base-installer :
    <ul>
      <li>réintroduction de la fonction is_ports_architecture() supprimée – elle
	est utilisée par d'autres paquets et library.sh semble être le bon
	endroit central pour elle (<a href="https://bugs.debian.org/979193">nº 979193</a>).</li>
    </ul>
  </li>
  <li>brltty :
    <ul>
      <li>mise à jour des règles d'udev ;</li>
      <li>utilisation assurée des attributs de renforcement dans la construction
	d'udeb.</li>
    </ul>
  </li>
  <li>choose-mirror :
    <ul>
      <li>mise à jour de Mirrors.masterlist.</li>
    </ul>
  </li>
  <li>debian-archive-keyring :
    <ul>
      <li>ajout des clés de Bullseye, retrait des clés de Jessie.</li>
    </ul>
  </li>
  <li>debian-cd :
    <ul>
      <li>abréviation de « ppc64el » en « p64el » dans l'identifiant du volume ;</li>
      <li>inclusion du paquet deb eatmydata pour que l'installateur l'utilise
	hors ligne (<a href="https://bugs.debian.org/986772">nº 986772</a>).</li>
    </ul>
  </li>
  <li>debian-installer :
    <ul>
      <li>intégration de libinput à la place du pilote evdev pour l'installateur
	graphique. Cela devrait améliorer la prise en charge des touchpads en
	particulier ;</li>
      <li>passage de l'ABI du noyau Linux à la version 5.10.0-6 ;</li>
      <li>ajout de wireless-regdb-udeb à toutes les images d'architecture Linux
	(<a href="https://bugs.debian.org/979104">nº 979104</a>) ;</li>
      <li>correction du mot clé pour les arguments du chargeur d'amorçage dans
	le fichier d'exemple netboot/xen/debian.cfg (<a href="https://bugs.debian.org/904131">nº 904131</a>) ;</li>
      <li>mise à jour du manuel debian-internals vers l'état de développement
	actuel ;</li>
      <li>mise à jour des tailles minimales d'espace disque et de mémoire.</li>
    </ul>
  </li>
  <li>debian-installer-utils :
    <ul>
      <li>list-devices-linux : prise en charge des partitions sur les
	périphériques USB UAS.</li>
    </ul>
  </li>
  <li>espeakup :
    <ul>
      <li>report des niveaux du mixeur Alsa dans le système installé.</li>
    </ul>
  </li>
  <li>grub-installer :
    <ul>
      <li>assurance que le module efivarfs est chargé et que le pseudo-système
	de fichiers efivars est monté si nécessaire.</li>
    </ul>
  </li>
  <li>grub2 :
    <ul>
      <li>ajout de la prise en charge de SBAT ;</li>
      <li>grub-install : correction du test inversé sur l'activation de NLS
	lors de la copie des paramètres linguistiques régionaux (<a href="https://bugs.debian.org/979754">nº 979754</a>).</li>
    </ul>
  </li>
  <li>installation-report :
    <ul>
      <li>bugscript : pas d'inclusion de modèle dans la sortie du script (<a href="https://bugs.debian.org/980929">nº 980929</a>).</li>
    </ul>
  </li>
  <li>libdebian-installer :
    <ul>
      <li>suppression de la limitation arbitraire de longueur maximale de ligne
	dans les fichiers de paquets et de sources (<a href="https://bugs.debian.org/971946">nº 971946</a>).</li>
    </ul>
  </li>
  <li>libinih :
    <ul>
      <li>ajout de libinih1-udeb à l'installateur Debian (<a href="https://bugs.debian.org/981864">nº 981864</a>).</li>
    </ul>
  </li>
  <li>libinput :
    <ul>
      <li>suppression de la prise en charge de libwacom du paquet udeb.</li>
    </ul>
  </li>
  <li>libmd :
    <ul>
      <li>ajout d'un paquet udeb nécessaire à libbsd.</li>
    </ul>
  </li>
  <li>libwacom :
    <ul>
      <li>suppression du paquet udeb dont libinput n'a plus besoin.</li>
    </ul>
  </li>
  <li>linux :
    <ul>
      <li>ajout du pilote bonding au paquet udeb nic-modules udeb ;</li>
      <li>retrait d'efivars du paquet udeb efi-modules ;</li>
      <li>arm64 : correction du nom du module i2c-mv64xxx.</li>
    </ul>
  </li>
  <li>localechooser :
    <ul>
      <li>désactivation du kabyle pour l'installateur en mode texte, car le
	changement de clavier échoue ici (<a href="https://bugs.debian.org/973455">nº 973455</a>) ;</li>
      <li>correction de la définition des paramètres linguistiques régionaux
	kabyles dans la liste des langues par son appellation exacte.</li>
    </ul>
  </li>
  <li>lowmem :
    <ul>
      <li>suppression des fichiers relatifs au terminal graphique quand la
	quantité de mémoire est faible (<a href="https://bugs.debian.org/977490">nº 977490</a>) ;</li>
      <li>5lowmem : renommé en 05lowmem pour que le mode rescue l'écrase
	réellement (<a href="https://bugs.debian.org/870574">nº 870574</a>) ;</li>
      <li>S15lowmem : mise à jour des tailles minimales de mémoire.</li>
    </ul>
  </li>
  <li>nano :
    <ul>
      <li>compilation du paquet udeb avec --without-included-regex pour rendre
	les binaires nettement plus petits.</li>
    </ul>
  </li>
  <li>netcfg :
    <ul>
      <li>mise à jour des cas d'essai pour qu'ils fonctionnent avec l'API Check
	actuelle (<a href="https://bugs.debian.org/980607">nº 980607</a>) ;</li>
      <li>exigences excessives de GCC satisfaites pour les appels strncpy().</li>
    </ul>
  </li>
  <li>os-prober :
    <ul>
      <li>découverte du système d'exploitation Microsoft sur arm64.</li>
    </ul>
  </li>
  <li>partman-btrfs :
    <ul>
      <li>ajout de la prise en charge minimale des sous-volumes pour / (<a href="https://bugs.debian.org/964818">nº 964818</a>).</li>
    </ul>
  </li>
  <li>partman-efi :
    <ul>
      <li>assurance que le module efivarfs est chargé et que le pseudo-système
	de fichiers efivars est monté si nécessaire.</li>
    </ul>
  </li>
  <li>rootskel :
    <ul>
      <li>utilisation de /dev/tty0 comme console même s'il n'est pas dans
	/proc/consoles ;</li>
      <li>pas de retrait du fichier de fontes si bterm n'est pas démarré (<a href="https://bugs.debian.org/977466">nº 977466</a>).</li>
    </ul>
  </li>
  <li>rootskel-gtk :
    <ul>
      <li>passage au thème Homeworld pour Debian 11, créé par Juliette Taka.</li>
    </ul>
  </li>
  <li>user-setup :
    <ul>
      <li>tiret bas permis dans le nom d'utilisateur du premier compte (<a href="https://bugs.debian.org/977214">nº 977214</a>).</li>
    </ul>
  </li>
</ul>


<h2>Modifications de la prise en charge matérielle</h2>

<ul>
  <li>debian-installer :
    <ul>
      <li>arm64 : ajout de la prise en charge de puma-rk3399 ;</li>
      <li>arm64 : mise à jour pour l'utilisation de u-boot-install-sunxi ;</li>
      <li>armhf : les images sur carte SD pour média disque dur et pour amorçage
	réseau démarrent à la position 32768, pour une compatibilité avec les
	plateformes rockchip ;</li>
      <li>arm64, armhf : mise à jour de la taille des images sur carte SD pour
	amorçage réseau, netboot-gtk et média disque dur.</li>
    </ul>
  </li>
  <li>flash-kernel :
    <ul>
      <li>ajout de la prise en charge de Orange Pi One Plus (<a href="https://bugs.debian.org/981328">nº 981328</a>) ;</li>
      <li>ajout de la prise en charge de ROCK Pi 4 (A,B,C) ;</li>
      <li>ajout de la prise en charge de Banana Pi BPI-M2-Ultra (<a href="https://bugs.debian.org/982089">nº 982089</a>) ;</li>
      <li>ajout de la prise en charge de Banana Pi BPI-M3 (<a href="https://bugs.debian.org/981561">nº 981561</a>) ;</li>
      <li>corrections des variantes ESPRESSObin manquantes (<a href="https://bugs.debian.org/969518">nº 969518</a>) ; </li>
      <li>correction de plusieurs entrées DTB-Id qui référençaient de manière
	erronée .dts à la place du fichier .dtb.</li>
    </ul>
  </li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>78 langues sont prises en charge dans cette version.</li>
</ul>


<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres supports d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>
