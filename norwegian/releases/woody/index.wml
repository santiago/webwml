#use wml::debian::template title="Debian GNU/Linux 3.0 &ldquo;woody&rdquo;-utgivelsesinformasjon" BARETITLE="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/woody/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="0a158377b74b807d40736c5de7ac54c071d55714" maintainer="Hans F. Nordhaug"

<h2><a name="general">Generell informasjon</a></h2>

<p>Debian GNU/Linux 3.0 (med kodenavn <em>woody</em>) 
  ble først utgitt <:=spokendate('2002-07-19'):>.
  Utgivelsen inkluderte mange store endringer beskrevet i vår
  <a href="$(HOME)/News/2002/20020719">pressemelding</a> og i
  <a href="releasenotes">utgivelsesmerknadene</a>.
</p>

<p><strong>
Debian GNU/Linux 3.0 er erstattet av 
<a href="../etch/">Debian GNU/Linux 3.1 (<q>sarge</q>)</a>.
Sikkerhetsoppdateringer opphørte ved utgangen av juni 2006.
</strong></p>

<p>Følgende datamaskinarkitekturer var støttet i denne utgaven:</p>

<ul>
<:
foreach $arch (@arches) {
  if ($arch eq "mipsel") { # both MIPS ports have the same web page
    print "<li><a href=\"$(HOME)/ports/mips/\">$arches{$arch}</a>\n";
  } else {
    print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
  }
}
:>
</ul>

<p>
  I motsetning til våre ønsker kan det være problemer i denne utgave selvom
  den er erklært <em>stabil</em>. Vi har lagd <a href="errata">en list med de
  viktigste kjente problemene</a>, og du kan alltid 
  <a href="reportingbugs">rapportere andre problem</a> til oss.
</p>

<p>
  Sist, men ikke minst, har vi en liste med <a href="credits">folk som har
  sørget for at denne utgaven ble utgitt</a>.
</p>
