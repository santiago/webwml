#use wml::debian::template title="Debian 11 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="bf24b081dbd7cfda9ffff0ba955eac66877be675"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Kända problem</toc-add-entry>
<toc-add-entry name="security">Säkerhetsproblem</toc-add-entry>

<p>Debians säkerhetsgrupp ger ut uppdateringar till paket i den stabila utgåvan
där de har identifierat problem relaterade till säkerhet. Vänligen se
<a href="$(HOME)/security/">säkerhetssidorna</a> för information om
potentiella säkerhetproblem som har identifierats i <q>Bullseye</q>.</p>

<p>Om du använder APT, lägg till följande rad i <tt>/etc/apt/sources.list</tt>
för att få åtkomst till de senaste säkerhetsuppdateringarna:</p>

<pre>
  deb http://security.debian.org/debian-security bullseye-security main contrib non-free
</pre>

<p>Kör <kbd>apt update</kbd> efter detta, följt av
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Punktutgåvor</toc-add-entry>

<p>Ibland, när det gäller kritiska problem eller säkerhetsuppdateringar,
uppdateras utgåvedistributionen. Generellt indikeras detta som punktutgåvor.
</p>

<ul>
  <li>Den första punktutgåvan, 11.1 släpptes
      <a href="$(HOME)/News/2021/20211009">den 9 oktober, 2021</a>.</li>
  <li>Den andra punktutgåvan, 11.2, släpptes
      <a href="$(HOME)/News/2021/20211218">den 18 december, 2021</a>.</li>
  <li>Den tredje punktutgåvan, 11.3, släpptes
      <a href="$(HOME)/News/2022/20220326">den 26 mars, 2022</a>.</li>
  <li>Den fjärde punktutgåvan 11.4, släpptes
      <a href="$(HOME)/News/2022/20220709">den 9 juli, 2022</a>.</li>
  <li>Den femte punktutgåvan 11.5 , släpptes
      <a href="$(HOME)/News/2022/2022091002">den 10 september, 2022</a>.</li>
<li>Den sjätte punktutgåvan 11.6, släpptes
      <a href="$(HOME)/News/2022/20221217">den 17 december, 2022</a>.</li>
<li>Den sjunde punktutgåvan 11.7, släpptes
      <a href="$(HOME)/News/2023/20230429">den 29 april 2023</a>.</li>
</ul>

<ifeq <current_release_bullseye> 11.0 "

<p>Det finns inga punktutgåvor för Debian 11 ännu.</p>" "

<p>Se <a
href="http://http.us.debian.org/debian/dists/bullseye/ChangeLog">\
förändringsloggen</a>
för detaljer om förändringar mellan 11 och <current_release_bullseye/>.</p>"/>

<p>Rättningar till den utgivna stabila utgåvan går ofta genom en
utökad testningsperiod innan de accepteras i arkivet. Dock, så finns dessa
rättningar tillgängliga i mappen
<a href="http://ftp.debian.org/debian/dists/bullseye-proposed-updates/">\
dists/bullseye-proposed-updates</a> i alla Debianarkivspeglingar.</p>

<p>Om du använder APT för att uppdatera dina paket kan du installera de
föreslagna uppdateringarna genom att lägga till följande rad i
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# föreslagna tillägg för en punktutgåva till Debian 11
  deb http://deb.debian.org/debian bullseye-proposed-updates main contrib non-free
</pre>

<p>Kör <kbd>apt update</kbd> efter detta, följt av
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Installationssystem</toc-add-entry>

<p>
För mer information om kända problem och uppdateringar till
installationssystemet, se
<a href="debian-installer/">debian-installer</a>-sidan.
</p>
