#use wml::debian::template title="Versionsfakta för Debian &ldquo;Buster&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/buster/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="318de675f94f01b3b99f51dd88802dda75065f7d"

<p>Debian <current_release_buster> släpptes
<a href="$(HOME)/News/<current_release_newsurl_buster/>"><current_release_date_buster></a>.
<ifneq "10.0" "<current_release>"
  "Debian 10.0 släpptes ursprungligen <:=spokendate('2019-07-06'):>."
/>
Utgåvan inkluderade många stora
förändringar, vilka beskrivs i
vårt <a href="$(HOME)/News/2019/20190706">pressmeddelande</a>
samt <a href="releasenotes">versionsfakta</a>.</p>

<p><strong>Debian 10 har efterträtts av
<a href="../bullseye/">Debian 11 (<q>Bullseye</q>)</a>.
#Security updates have been discontinued as of <:=spokendate('xxxx-xx-xx'):>.
</strong></p>

<p><strong>Buster drar dock nytta av långtidsstöd (Long Term Support - LTS) fram
till slutet på juni 2024. LTS begränsas till i386, amd64, armhf and arm64.
Ingen av de andra arkitekturerna har stöd i Buster.
För ytterligare information, vänligen se <a
href="https://wiki.debian.org/LTS">sektionen för LTS-stöd i Debianwikin.</a>.
</strong></p>

<p>För att få tag på och installera Debian, se vår sida med
<a href="debian-installer/">installationsinformation</a> samt
<a href="installmanual">installationsguiden</a>. För att uppgradera från en
tidigare Debianutgåva, se informationen i
<a href="releasenotes">versionsfakta</a>.</p>

### Activate the following when LTS period starts.
#<p>Arkitekturer som stöds under perioden för långtidsstöd:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Följande datorarkitekturer stöds i denna utgåva:</p>
# <p>Computer architectures supported at initial release of buster:</p> ### Use this line when LTS starts, instead of the one above.

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Tvärt emot våra önskemål finns det en del problem i denna utgåva, även om den
kallas för <em>stabil</em>. Vi har sammanställt
<a href="errata">en lista över de största kända problemen</a>, och du kan alltid
<a href="reportingbugs">rapportera andra problem</a> till oss.</p>

<p>Sist, men inte minst, har vi en lista över <a href="credits">folk som skall
ha tack</a> för att ha möjliggjort denna version.</p>
