#use wml::debian::template title="Introduktion till Debian" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
      <h2>Debian är en gemenskap</h2>
      <p>Tusentals frivilliga runt hela världen arbetar tillsammans på
      operativsystemet Debian, och prioriterar fri mjukvara och mjukvara med
      öppen källkod. Möt Debianprojektet.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="people">Folk:</a>
          Vilka vi är, vad vi gör
        </li>
        <li>
          <a href="philosophy">Filosofi:</a>
          Varför vi gör det och hur vi gör det
        </li>
        <li>
          <a href="../devel/join/">Engagera dig:</a>
          Du kan bli en del av detta!
        </li>
        <li>
          <a href="help">Hur kan du hjälpa Debian?</a>
        </li>
        <li>
          <a href="../social_contract">Socialt Kontrakt:</a>
          Vår moraliska agenda
        </li>
        <li>
          <a href="diversity">Mångfaldserkännande</a>
        </li>
        <li>
          <a href="../code_of_conduct">Uppförandekod</a>
        </li>
        <li>
          <a href="../partners/">Partners:</a>
          Företag och organisationer som tillhandahåller pågående assistans
          till Debianprojektet
        </li>
        <li>
          <a href="../donations">Donationer</a>
        </li>
        <li>
          <a href="../legal/">Rättsliga frågor</a>
        </li>
        <li>
          <a href="../legal/privacy">Dataintegritet</a>
        </li>
        <li>
          <a href="../contact">Kontakta oss</a>
        </li>
      </ul>
   </div>
</div>


  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="software"></a>
      <h2>Debian är ett operativsystem</h2>
      <p>Debian är ett fritt operativsystem, utvecklat och underhållet av
      Debianprojektet. En fri linuxdistribution med tusentals program för att
      möta våra användares behov.</p>
    </div>

    <div style="text-align: left">
   <ul>
     <li>
       <a href="../distrib">Hämta:</a>
       Flera varianter av Debianavbildningar
     </li>
     <li>
     <a href="why_debian">Varför Debian</a>
     </li>
     <li>
       <a href="../support">Support:</a>
       Få hjälp
     </li>
     <li>
       <a href="../security">Säkerhet:</a>
       Senaste uppdatering <br>
       <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
           @MYLIST = split(/\n/, $MYLIST);
           $MYLIST[0] =~ s#security#../security#;
           print $MYLIST[0]; }:>
     </li>
     <li>
       <a href="../distrib/packages"> Mjukvarupaket:</a>
       Sök och bläddra i den långa listan på vår mjukvara
     </li>
     <li>
       <a href="../doc"> Dokumentation</a>
     </li>
     <li>
       <a href="https://wiki.debian.org"> Debianwikin</a>
     </li>
     <li>
       <a href="../Bugs"> Felrapporter</a>
     </li>
     <li>
       <a href="https://lists.debian.org/">
       Sändlistor</a>
     </li>
     <li>
       <a href="../blends"> Pure Blends:</a>
       Metapaket för speciella behov
     </li>
     <li>
       <a href="../devel"> Utvecklarhörnet:</a>
       Information primärt av intresse för Debianutvecklare
     </li>
     <li>
       <a href="../ports"> Anpassningar/Arkitekturer:</a>
       CPU-arkitekturer som vi stödjer
     </li>
     <li>
       <a href="search">Information om hur du använder Debians sökmotor</a>.
     </li>
     <li>
       <a href="cn">Information om sidor som finns tillgängliga på flera språk</a>.
     </li>
   </ul>
 </div>
  </div>

</div>
