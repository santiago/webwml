#
# Damyan Ivanov <dmn@debian.org>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: unnamed project\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-07-15 16:08+0300\n"
"Last-Translator: Damyan Ivanov <dmn@debian.org>\n"
"Language-Team: Bulgarian <dict@ludost.net>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.1\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "Универсалната операционна система"

#: ../../english/index.def:12
msgid "DebConf is underway!"
msgstr "Годишна конференция DebConf"

#: ../../english/index.def:15
msgid "DebConf Logo"
msgstr "Групова снимка от годишната конференция"

#: ../../english/index.def:19
#, fuzzy
#| msgid "DC19 Group Photo"
msgid "DC22 Group Photo"
msgstr "Групова снимка от DebConf19"

#: ../../english/index.def:22
#, fuzzy
#| msgid "DebConf19 Group Photo"
msgid "DebConf22 Group Photo"
msgstr "Групова снимка от годишната конференция през 2019"

#: ../../english/index.def:26
msgid "Debian Reunion Hamburg 2023"
msgstr ""

#: ../../english/index.def:29
#, fuzzy
#| msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgid "Group photo of the Debian Reunion Hamburg 2023"
msgstr "Групова снимка от мини конференцията в Регенсбург през 2021"

#: ../../english/index.def:33
msgid "Mini DebConf Regensburg 2021"
msgstr "Мини-конференция в Регенсбург 2018"

#: ../../english/index.def:36
msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgstr "Групова снимка от мини конференцията в Регенсбург през 2021"

#: ../../english/index.def:40
msgid "Screenshot Calamares Installer"
msgstr "Снимка от инсталатора Calamares"

#: ../../english/index.def:43
msgid "Screenshot from the Calamares installer"
msgstr "Снимка на екрана на инсталатора Calamares"

#: ../../english/index.def:47 ../../english/index.def:50
msgid "Debian is like a Swiss Army Knife"
msgstr "Дебиан е като швейцарско ножче"

#: ../../english/index.def:54
msgid "People have fun with Debian"
msgstr "Хора се забавляват"

#: ../../english/index.def:57
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr "Посетители на конференцията в Шинчу (Тайван) наистина се забавляват"

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr ""

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr ""

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr ""

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr ""

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr ""

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr ""
